﻿using FRIWebAPI.App_Start;
using FRIWebAPI.ClassFiles;
using FRIWebAPI.Database;
using FRIWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using static FRIWebAPI.ClassFiles.Common;

namespace FRIWebAPI.Controllers
{
    public class EventsController : ApiController
    {
        FRIEntities dbContext = null;

        public EventsController()
        {
            dbContext = new FRIEntities();
        }

        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        [HttpPost]
        [Route("api/Events/AddEvent")]
        public IHttpActionResult AddEvent(Master_Events oEvent)
        {
            ReturnResponse oResponse = new ReturnResponse();


            try
            {

                oEvent.CreatedDate = DateTime.Now;
                oEvent.Creator = GetUserName(oEvent.FK_CreatedBy);


                dbContext.Master_Events.Add(oEvent);


                dbContext.SaveChanges();

                oResponse.status = "200";
                oResponse.status = "Success";
                oResponse.result = oEvent.PK_EventID;

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;


            }




            return Ok(oResponse);
        }


        [HttpPost]
        [Route("api/Events/UpdateEvent")]
        public IHttpActionResult UpdateEvent(Master_Events oEvent)
        {
            ReturnResponse oResponse = new ReturnResponse();


            try
            {

                var MasterEvent = (from Event in dbContext.Master_Events
                                   where Event.PK_EventID == oEvent.PK_EventID
                                   select Event).FirstOrDefault();

                if (MasterEvent != null)
                {

                    MasterEvent.Banner = oEvent.Banner;
                    MasterEvent.City = oEvent.City;
                    MasterEvent.CreatedDate = DateTime.Now;
                    MasterEvent.Date = oEvent.Date;
                    MasterEvent.Description = oEvent.Description;
                    MasterEvent.EventTitle = oEvent.EventTitle;
                    MasterEvent.EventType = oEvent.EventType;
                    MasterEvent.FK_Country = oEvent.FK_Country;
                    MasterEvent.PostalOrZipCode = oEvent.PostalOrZipCode;
                    MasterEvent.Street = oEvent.Street;
                    MasterEvent.ModifiedDate = DateTime.Now;
                    MasterEvent.FK_ModifiedBy = oEvent.FK_ModifiedBy;

                    dbContext.SaveChanges();

                    oResponse.status = "200";
                    oResponse.status = "Success";
                    oResponse.result = oEvent.PK_EventID;
                }
                else
                {
                    oResponse.errors = "No data found";
                    oResponse.status = "Error";
                }

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;


            }




            return Ok(oResponse);
        }


        [HttpGet]
        [Route("api/Events/GetEventDetails/{EventId}")]
        public IHttpActionResult GetEventDetails(int EventId)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try

            {
                List<UserEvents> oEvent = new List<UserEvents>();
                oEvent = (from Event in dbContext.Master_Events
                          where Event.DelFlag == false 
                          && Event.PK_EventID== EventId
                          select new UserEvents
                          {
                              PK_EventID = Event.PK_EventID,
                              Creator = Event.Creator,
                              Location = Event.Location,
                              Banner = Event.Banner,
                              City = Event.City,
                              Description = Event.Description,
                              Date = Event.Date,
                              EventTitle = Event.EventTitle,
                              EventType = Event.EventType,
                              FK_Country = Event.FK_Country,
                              FK_ProvinceOrState = Event.FK_ProvinceOrState,
                              PostalOrZipCode = Event.PostalOrZipCode,
                              Street = Event.Street,
                              Country = Event.Lookup_Country.Description,
                              State = Event.Lookup_StateOrProvince.Description,
                              FK_CreatedBy = Event.FK_CreatedBy,
                          }).OrderByDescending(p => p.PK_EventID).ToList();

                foreach (UserEvents Event in oEvent)
                {
                    Event.DateString = FormatDateTime(Event.Date);

                    Event.LocationDetails = Event.Street;
                    if (Event.LocationDetails != null && Event.LocationDetails != "")
                        Event.LocationDetails += ", ";
                    Event.LocationDetails = Event.LocationDetails + Event.State + ", " + Event.Country;
                }

                if (oEvent.Count >= 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = oEvent;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }

            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }
        [HttpPost]
        [Route("api/Events/DeleteEvent")]
        public IHttpActionResult DeleteEvent(Master_Events oEvent)
        {
            ReturnResponse oResponse = new ReturnResponse();


            try
            {


                var MasterEvent = (from Event in dbContext.Master_Events
                                   where Event.PK_EventID == oEvent.PK_EventID
                                   select Event).FirstOrDefault();

                if (MasterEvent != null)
                {
                    MasterEvent.DelFlag = true;


                    dbContext.SaveChanges();
                }


                oResponse.status = "200";
                oResponse.status = "Success";
                oResponse.result = MasterEvent.PK_EventID;

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;


            }



            return Ok(oResponse);
        }


        private string GetUserName(int UserID)
        {
            var User = (from user in dbContext.Master_User
                        where user.PK_UserID == UserID && user.DelFlag == false
                        select new { user.LegalFirstName, user.LegalMiddleName, user.LastName }).FirstOrDefault();

            if (User == null)
                return null;

            if (User.LegalMiddleName == null || User.LegalMiddleName == "" || User.LegalMiddleName == " ")
                return User.LegalFirstName + " " + User.LastName;
            else
                return User.LegalFirstName + " " + User.LegalMiddleName + " " + User.LastName;


        }
        private string FormatDateTime(DateTime Date)
        {
            if (Date == null)
                return string.Empty;
            else
            {
                string time = string.Format("{0:hh:mm tt}", Date);
                string DateString = Date.ToString("dd MMMM yyyy");
                return DateString;
            }

        }

        [HttpGet]
        [Route("api/Events/GetUserEvents")]
        public IHttpActionResult GetUserEvents()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
         
            {
                DateTime dtNow = DateTime.UtcNow.AddDays(-1);
                List<UserEvents> oEvent = new List<UserEvents>();
                    oEvent = (from Event in dbContext.Master_Events
                                  //where Event.DelFlag == false&& Event.Date > DateTime.Now
                              where Event.DelFlag == false && Event.Date > dtNow
                              select new UserEvents
                              {
                                  PK_EventID = Event.PK_EventID,
                                  Creator = Event.Creator,
                                  Location = Event.Location,
                                  Banner = Event.Banner,
                                  City = Event.City,
                                  Description = Event.Description,
                                  Date = Event.Date,
                                  EventTitle = Event.EventTitle,
                                  EventType = Event.EventType,
                                  FK_Country = Event.FK_Country,
                                  FK_ProvinceOrState = Event.FK_ProvinceOrState,
                                  PostalOrZipCode = Event.PostalOrZipCode,
                                  Street = Event.Street,
                                  Country = Event.Lookup_Country.Description,
                                  State = Event.Lookup_StateOrProvince.Description,
                                  FK_CreatedBy = Event.FK_CreatedBy,
                              }).OrderByDescending(p => p.PK_EventID).ToList();
                
                foreach (UserEvents Event in oEvent)
                {
                    Event.DateString = FormatDateTime(Event.Date);

                    Event.LocationDetails = Event.Street;
                    if (Event.LocationDetails != null && Event.LocationDetails != "")
                        Event.LocationDetails += ", ";
                    Event.LocationDetails = Event.LocationDetails + Event.State + ", " + Event.Country;
                }

                if (oEvent.Count >= 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = oEvent;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
        
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }


        [HttpGet]
        [Route("api/Events/GetUpcommingEvents")]
        public IHttpActionResult GetUpcommingEvents()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                List<UserEvents> oEvent = new List<UserEvents>();
                DateTime dtNow = DateTime.UtcNow.AddDays(-1);
                oEvent = (from Event in dbContext.Master_Events
                              //where Event.DelFlag == false && Event.Date >= DateTime.Now
                          where Event.DelFlag == false && Event.Date >= dtNow
                          select new UserEvents
                          {
                              PK_EventID = Event.PK_EventID,
                              Creator = Event.Creator,
                              Location = Event.Location,
                              Banner = Event.Banner,
                              City = Event.City,
                              Description = Event.Description,
                              Date = Event.Date,
                              EventTitle = Event.EventTitle,
                              EventType = Event.EventType,
                              FK_Country = Event.FK_Country,
                              FK_ProvinceOrState = Event.FK_ProvinceOrState,
                              PostalOrZipCode = Event.PostalOrZipCode,
                              Street = Event.Street,
                              Country = Event.Lookup_Country.Description,
                              State = Event.Lookup_StateOrProvince.Description

                          }).OrderByDescending(p => p.PK_EventID).ToList();


                foreach (UserEvents Event in oEvent)
                {
                    Event.DateString = FormatDateTime(Event.Date);

                    Event.LocationDetails = Event.Street;
                    if (Event.LocationDetails != null && Event.LocationDetails != "")
                        Event.LocationDetails += ", ";
                    Event.LocationDetails = Event.LocationDetails + Event.State + ", " + Event.Country;
                }

                if (oEvent.Count >= 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = oEvent;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }

        [HttpPost]
        [Route("api/Events/UploadEventImage")]
        public ReturnResponse UploadEventImage()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count > 0)
                {

                    var postedFile = httpRequest.Files[0];
                    string ext = System.IO.Path.GetExtension(postedFile.FileName);
                    string csNewFileName = Guid.NewGuid().ToString() + ext;
                    var filePath = HttpContext.Current.Server.MapPath("~/Uploads/Event/" + csNewFileName);
                    postedFile.SaveAs(filePath);

                    dbContext.SaveChanges();
                    oResponse.status = "200";
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    string Imageurl = WebConfigurationManager.AppSettings["Imageurl"];
                    Imageurl = Imageurl + "Event/";
                    oResponse.result = Imageurl + csNewFileName;

                }

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }
            return oResponse;
        }


    }
}