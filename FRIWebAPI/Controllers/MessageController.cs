﻿using FRIWebAPI.App_Start;
using FRIWebAPI.Database;
using FRIWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using static FRIWebAPI.ClassFiles.Common;

namespace FRIWebAPI.Controllers
{
    public class MessageController : ApiController
    {
        FRIEntities dbContext = null;

        public MessageController()
        {
            dbContext = new FRIEntities();
        }

        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        [HttpPost]
        [Route("api/Message/AddMessage")]
        public IHttpActionResult AddMessage(UserMessage oUserMessage)
        {
            ReturnResponse oResponse = new ReturnResponse();

            using (var transaction = dbContext.Database.BeginTransaction())
            {
                try
                {

                    Master_Messages oMessage = new Master_Messages();


                    oMessage.CreatedDate = DateTime.Today;
                    oMessage.FK_CreatedBy = oUserMessage.FK_SenderID;
                    oMessage.FK_MessageTypeID = oUserMessage.FK_MessageTypeID;
                    oMessage.FK_RecipientID = oUserMessage.FK_RecipientID;
                    oMessage.FK_SenderID = oUserMessage.FK_SenderID;
                    oMessage.Message = "<strong>" + "Message: " + "</strong>" + oUserMessage.Message;
                    oMessage.SenderName = GetUserName(oMessage.FK_SenderID);
                    oMessage.RecipientName = GetUserName(oMessage.FK_RecipientID);
                    oMessage.Subject = "<strong>" + oMessage.SenderName + "</strong>" + " sent you a private message on " + FormatDate(oMessage.CreatedDate);

                    oUserMessage.IconPath = "../assets/images/envelope.svg";

                    if (oUserMessage.ParentMessageID != null)
                    {
                        oMessage.ParentMessageID = oUserMessage.ParentMessageID;

                        string MessageStatus = null;

                        if (oUserMessage.FK_MessageTypeID == 11 /*Accepted Spar*/  || oUserMessage.FK_MessageTypeID == 15 /*Accepted Challenge*/  )
                            MessageStatus = "ACCEPTED";

                        else if (oUserMessage.FK_MessageTypeID == 12 /*Rejected Spar*/  || oUserMessage.FK_MessageTypeID == 16 /*Rejected Challenge*/  )
                            MessageStatus = "REJECTED";

                        else if (oUserMessage.FK_MessageTypeID == 14 /*Spar Reminder*/ )
                            MessageStatus = "SENT_REMINDER";

                        else if (oUserMessage.FK_MessageTypeID == 10 /*Sponsor Reply */ )
                            MessageStatus = "SPONSOR_REPLYED";


                        else if (oUserMessage.FK_MessageTypeID == 13 /*Sponsor Thanks */ )
                            MessageStatus = "SPONSOR_THANKS";

                        else if (oUserMessage.FK_MessageTypeID == 20 /*Cheer Thanks */ )
                            MessageStatus = "CHEER_THANKS";

                        dbContext.Master_Messages
                               .Where(p => p.PK_MessageID == oMessage.ParentMessageID)
                               .FirstOrDefault().ReadFlag = true;

                        if (MessageStatus != null)
                            dbContext.Master_Messages
                              .Where(p => p.PK_MessageID == oMessage.ParentMessageID)
                              .FirstOrDefault().MessageStatus = MessageStatus;


                        dbContext.SaveChanges();
                    }



                    switch (oUserMessage.FK_MessageTypeID)
                    {
                        case 1://Spar Request
                        case 11://Accepted Spar
                        case 12://Rejected Spar
                        case 14://Spar Reminder
                            oUserMessage.IconPath = "../assets/images/two-boxing-gloves.svg";
                            if (oUserMessage.FK_MessageTypeID == 1)
                            {
                                //oMessage.Message = "<strong>" + "Message: " + "</strong>" + oMessage.Message;
                                //oMessage.Subject = "<strong>" + oMessage.SenderName + "</strong>" + " is requesting to be your sparing partner. Select <strong>Accept</strong> to accept the request.";

                                oMessage.Subject = "<strong>" + oMessage.SenderName + "</strong>" + " has sent you a sparring request.";
                                oMessage.Message = "<strong>" + "Message: " + "</strong>" + " You can message this person directly through their profile." +
                                 "<br/> <br />" + "Sparring with a variety of fighters is a great way to advance your skills and to increase your network of training partners! Sparring should be kept to a ‘training’ level that is appropriate for both you and for your sparring partner.";
                                AddMsgToTrackedUser(oUserMessage, Constants.SparReceived);
                            }
                            else if (oUserMessage.FK_MessageTypeID == 11)//Accepted Spar
                            {
                                oUserMessage.IconPath = "../assets/images/accept.svg";
                                oMessage.Subject = "<strong>" + oMessage.SenderName + "</strong>" + " Accepted your sparing request.";
                                oMessage.Message = "<strong>" + "Message: " + "</strong>" + " Thanks for the invitation to spar. I am interested in training and sparringwith you. Please contact me through my profile and message me your contact info.";
                                AddMsgToTrackedUser(oUserMessage, Constants.SparAccepted);
                            }
                            else if (oUserMessage.FK_MessageTypeID == 12)//Rejected Spar
                            {


                                oUserMessage.IconPath = "../assets/images/reject.svg";
                                oMessage.Subject = "<strong>" + oMessage.SenderName + "</strong>" + " Rejected your sparing request.";
                                oMessage.Message = "<strong>" + "Message: " + "</strong>" + " Thanks for the invitation to spar. At this time,I will decline your request.";
                                AddMsgToTrackedUser(oUserMessage, Constants.SparRejected);
                            }
                            else if (oUserMessage.FK_MessageTypeID == 14)//Spar Reminder
                            {
                                oMessage.Message = "<strong>" + "Message: " + "</strong>" + "Please accept my Spar Request";
                                oMessage.Subject = "<strong>" + oMessage.SenderName + "</strong>" + " has sent a sparing request to you on ." + FormatDate(oMessage.CreatedDate);
                            }
                            break;

                        case 2: //Request Documentation
                            oMessage.Message = "<strong>" + "Message: " + "</strong>" + "Please update your profile";
                            oUserMessage.IconPath = "../assets/images/documentation.svg";
                            oMessage.Subject = "<strong>" + oMessage.SenderName + "</strong>" + " has requested for you to send your updated profile to on " + FormatDate(oMessage.CreatedDate);
                            break;

                        case 3://Challenge Request
                        case 15:// Accepted Challenge
                        case 16://Rejected Challenge      
                                //     oMessage.Message = oMessage.Master_User.Master_Profile.FirstOrDefault().FR_Bio;

                            oUserMessage.IconPath = "../assets/images/challenge.svg";
                            if (oUserMessage.FK_MessageTypeID == 3)// Challenge Request
                            {
                                //oMessage.Message = "<strong>" + "Message: " + "</strong>" + oMessage.Message;
                                oMessage.Subject = "<strong>" + oMessage.SenderName + "</strong>" + " has challenged you.";
                                oMessage.Message = "<strong>" + "Message: " + "</strong>" + " Accepting or Rejecting this challenge will be posted to the public Challenge board. Message this person directly through their profile." +
                                   "<br/> <br />" + "Only accept challenges with fighters that are comparable to your experience and your ‘proven’ skill level. You can publicly discuss this challenge on Fight’s Forum.";
                                AddMsgToTrackedUser(oUserMessage, Constants.RequestReceived);
                            }
                            else if (oUserMessage.FK_MessageTypeID == 15)// Accepted Challenge
                            {
                                oUserMessage.IconPath = "../assets/images/accept.svg";
                                oMessage.Message = "<strong>" + "Message: I accept your challenge." + "</strong>" + " Contact me through my profile and let’s find away to make this happen.";
                                oMessage.Subject = "<strong>" + oMessage.SenderName + "</strong>" + " Accepted your challenge.";
                                AddMsgToTrackedUser(oUserMessage, Constants.RequestAccepted);

                            }
                            else if (oUserMessage.FK_MessageTypeID == 16)//Rejected Challenge
                            {
                                oUserMessage.IconPath = "../assets/images/reject.svg";
                                oMessage.Message = "<strong>" + "Message: " + "</strong>" + " I appreciate the opportunity, but I am declining your challenge.";
                                oMessage.Subject = "<strong>" + oMessage.SenderName + "</strong>" + " Rejected your challenge.";
                                AddMsgToTrackedUser(oUserMessage, Constants.RequestRejected);
                            }

                            break;

                        case 8://Sponsor Request
                        case 10:// Sponsor Reply 
                        case 13:// Sponsor Thanks
                            oUserMessage.IconPath = "../assets/images/wallet.svg";
                            if (oUserMessage.FK_MessageTypeID == 8)//Sponsor Request
                            {
                                oMessage.Subject = "<strong>" + oMessage.SenderName + "</strong>" + " would like to discuss sponsorship opportunities with you.";
                                oMessage.Message = "<strong>" + "Message:" + "</strong>" + " Please contact me regarding sponsorship opportunities." + FormatDate(oMessage.CreatedDate)
                                + "<br/> <br/>" + "By selecting “View Profile” you can confirm this contact’s information and respond to this opportunity. Remember, use of " + " <strong> " + "FightReg" + " </strong > " + " registry is private. Interaction between users in not the responsibility of " + " <strong> " + "FightReg." + " </strong> " +
                                   "<br/> <br/>" + "Select “Reply” to send a message. Include your email or messaging address if you chose to maintain contact." + " <br/> <br/> " + "Good luck with this sponsorship opportunity and your fight career!";
                            }
                            else if (oUserMessage.FK_MessageTypeID == 13)// Sponsor Thanks   
                            {
                                oUserMessage.IconPath = "../assets/images/thanks.svg";
                                oMessage.Subject = "<strong>" + oMessage.SenderName + "</strong>" + " sent thanks to your sponsor request";
                                oMessage.Message = "<strong>" + "Message: " + "</strong>" + "Thank you for your willingness to Sponsor me.";
                            }

                            break;


                        case 5://Follower    
                            oMessage.Message = "<strong>" + "Message: " + "</strong>" + oMessage.Message;
                            oUserMessage.IconPath = "../assets/images/toast.svg";
                            oMessage.Subject = "<strong>" + oMessage.SenderName + "</strong>" + " made some changes.";
                            break;


                        case 19://track                                      
                            oUserMessage.IconPath = "../assets/images/track.svg";
                            /*oMessage.Subject = "<strong>" + oMessage.SenderName + "</strong>" + " is tracking your FightReg updates.";
                            oMessage.Message = "<strong>" + "Message: " + "</strong>" + "<strong>" + "Message:" + "</strong>" + " Hello! Good luck in your fight career! I am looking forward to seeing your training and fight updates in your FightReg blog!.";*/
                            oMessage.Subject = "<strong>" + oMessage.SenderName + "</strong>" + " is tracking your " + "<strong>" + "FightReg " + "</strong>" + "updates.";
                            oMessage.Message = "<strong>" + "Message: " + "</strong>" + " Hello! Good luck in your fight career! I am looking forward to seeing your training and fight updates in your" + "<strong>" + " FightReg" + "</strong>" + " blog!.";
                            break;


                        case 20://Cheer thanks                                      
                            oUserMessage.IconPath = "../assets/images/thanks.svg";
                            oMessage.Subject = "<strong>" + oMessage.SenderName + "</strong>" + " sent you a thank you.";
                            oMessage.Message = "<strong>" + "Message:" + "</strong>" + " Thank you for cheering me!!";
                            break;

                        case 17://Untrack                                      
                                //    oUserMessage.IconPath = "../assets/images/un-track.svg";
                                //    oMessage.Subject = "<strong>" + oMessage.SenderName + "</strong>" + "un tracked you.";
                                //    oMessage.Message = "<strong>" + "Message: " + "</strong>" + "I am not intrested to receive the details about your events and updates.";
                            break;

                        case 6://Cheer                                       
                            oUserMessage.IconPath = "../assets/images/toast.svg";
                            oMessage.Subject = "<strong>" + oMessage.SenderName + "</strong>" + " is cheering for you! " + FormatDate(oMessage.CreatedDate);
                            oMessage.Message = "<strong>" + "Message:" + "</strong>" + " I would like to cheer you.";
                            break;

                        //case 18://Uncheer                                       
                        //    oUserMessage.IconPath = "../assets/images/un-cheer.svg";
                        //    oMessage.Subject = "<strong>" + oMessage.SenderName + "</strong>" + " sent you an uncheer on " + FormatDate(oMessage.CreatedDate);
                        //    oMessage.Message = "<strong>" + "Message: " + "</strong>" + "I would like to uncheer you.";
                        //    break;

                        case 7://Update Profile                                   
                            oUserMessage.IconPath = "../assets/images/update_profile.svg";
                            /*oMessage.Subject = "<strong>"+oMessage.SenderName+"</strong>" + " Requested to update your profile on " + FormatDate(oMessage.CreatedDate);
                            oMessage.Message = "<strong>" + "Message: " + "</strong>" + oMessage.SenderName+ "has asked that you to ensure your FightReg profile is up-to-date and that you email them a copy of your FightReg-PDF profile."
                                 + "<br/> <br />" + "<strong>" + "Caution:" + "</strong>" + " Always confirm the identity of anyone you plan to send your FightReg PDF to " + "<u>" + "BEFORE YOU SEND YOUR Records" + "</u>" + ". FightReg is not responsible for your safety  or for confirming the identity of profile owners."
                                    + "<br/> <br />" + "Use the “View” button on this message to navigate to the requesting profile. There you can see their contact information of send them a message."
                                    + "<br/> <br />" + "Good luck with your fight opportunity and your fight career!";*/
                            oMessage.Subject = "<strong>" + oMessage.SenderName + "</strong>" + " Requested to update your profile on " + FormatDate(oMessage.CreatedDate);
                            oMessage.Message = "<strong>" + "Message: " + "</strong>" + oMessage.SenderName + " has requested confirmation that your profile is up to date and to email them a copy of your" + "<strong> " + "PDF FightReg " + "</strong>" + "profile." +
"<br/> <br />" + "By selecting “View Profile” you can confirm this contact’s information and respond to this opportunity.Remember, use of " + "<strong>" + "FightReg" + "</strong>" + " registry is private. Interaction between users in not the responsibility of " + "<strong>" + "FightReg." + " </strong > " +
"<br/> <br />" + "Good luck with your fight opportunity and the next step in your career.";
                            break;

                        case 9://Dispute                              
                            oUserMessage.IconPath = "../assets/images/add.svg";
                            oMessage.Subject = "<strong>" + oMessage.SenderName + "</strong>" + " Reported some dispute on " + FormatDate(oMessage.CreatedDate);
                            break;

                    }


                    oMessage.IconPath = oUserMessage.IconPath;
                    oMessage.ReadFlag = false;
                    dbContext.Master_Messages.Add(oMessage);
                    dbContext.SaveChanges();


                    oUserMessage.PK_MessageID = oMessage.PK_MessageID;


                    if (oUserMessage.FK_MessageTypeID == 1)
                    {
                        Master_Spar oSparRequest = new Master_Spar
                        {
                            FK_SparFromID = oUserMessage.FK_SenderID,
                            FK_SparToID = oUserMessage.FK_RecipientID,
                            CreatedDate = DateTime.Today,
                            FK_MessageID = oUserMessage.PK_MessageID,
                            SparTo = GetUserName(oUserMessage.FK_RecipientID),
                            SparFrom = GetUserName(oUserMessage.FK_SenderID),
                            RequestedTime = DateTime.Today

                        };
                        dbContext.Master_Spar.Add(oSparRequest);
                    }
                    else if (oUserMessage.FK_MessageTypeID == 11)//Accepted Spar
                    {
                        dbContext.Master_Spar
                            .Where(p => p.FK_MessageID == oMessage.ParentMessageID)
                            .FirstOrDefault().FK_SparStatusID = 1;

                        dbContext.Master_Spar
                              .Where(p => p.FK_MessageID == oMessage.ParentMessageID)
                              .FirstOrDefault().AcceptedTime = DateTime.Today;
                    }
                    else if (oUserMessage.FK_MessageTypeID == 12)//Rejected Spar
                    {
                        dbContext.Master_Spar
                             .Where(p => p.FK_MessageID == oMessage.ParentMessageID)
                             .FirstOrDefault().FK_SparStatusID = 2;
                    }
                    else if (oUserMessage.FK_MessageTypeID == 14)//Spar Reminder
                    {
                        dbContext.Master_Spar
                             .Where(p => p.FK_MessageID == oMessage.ParentMessageID)
                             .FirstOrDefault().ReminderSendOn = DateTime.Today;
                    }

                    else if (oUserMessage.FK_MessageTypeID == 3)// Challenge Request
                    {
                        Master_Challenge oChallengeRequest = new Master_Challenge
                        {

                            FK_ChallengeFromID = oUserMessage.FK_SenderID,
                            ChallengeFrom = GetUserName(oUserMessage.FK_SenderID),
                            FK_ChallengerToID = oUserMessage.FK_RecipientID,
                            ChallengerTo = GetUserName(oUserMessage.FK_RecipientID),
                            CreatedDate = DateTime.Today,
                            FK_MessageID = oUserMessage.PK_MessageID,
                            ChallengeTime = DateTime.Today
                        };
                        dbContext.Master_Challenge.Add(oChallengeRequest);

                    }
                    else if (oUserMessage.FK_MessageTypeID == 15)// Accepted Challenge
                    {
                        dbContext.Master_Challenge
                             .Where(p => p.FK_MessageID == oMessage.ParentMessageID)
                             .FirstOrDefault().FK_ChallengeStatusID = 1;

                        dbContext.Master_Challenge
                            .Where(p => p.FK_MessageID == oMessage.ParentMessageID)
                            .FirstOrDefault().AcceptedTime = DateTime.Today;
                    }
                    else if (oUserMessage.FK_MessageTypeID == 16)//Rejected Challenge
                    {
                        dbContext.Master_Challenge
                             .Where(p => p.FK_MessageID == oMessage.ParentMessageID)
                             .FirstOrDefault().FK_ChallengeStatusID = 2;

                    }
                    else if (oUserMessage.FK_MessageTypeID == 8)//Sponsor Request
                    {
                        Master_Sponsership oSponsorRequest = new Master_Sponsership
                        {

                            FK_MessageID = oUserMessage.PK_MessageID,
                            SponsershipFrom = oUserMessage.FK_SenderID,
                            SponsershipTo = oUserMessage.FK_RecipientID,
                            CreatedDate = DateTime.Today,
                            FK_CreatedBy = oUserMessage.FK_SenderID
                        };
                        dbContext.Master_Sponsership.Add(oSponsorRequest);
                    }
                    else if (oUserMessage.FK_MessageTypeID == 19)//Track
                    {

                        Master_Follower oFollower = dbContext.Master_Follower.Where(p => p.FK_FollowerFrom == oUserMessage.FK_SenderID && p.FK_FollowerTo == oUserMessage.FK_RecipientID).FirstOrDefault();
                        if (oFollower == null)
                        {
                            oFollower = new Master_Follower
                            {
                                FK_FollowerFrom = oUserMessage.FK_SenderID,
                                FK_FollowerTo = oUserMessage.FK_RecipientID,
                                CreatedDate = DateTime.Today
                            };
                            dbContext.Master_Follower.Add(oFollower);


                        }
                        else
                        {
                            oFollower.DelFlag = false;

                        }


                    }
                    else if (oUserMessage.FK_MessageTypeID == 17)//un Track
                    {

                        Master_Follower oFollower = dbContext.Master_Follower.Where(p => p.FK_FollowerFrom == oUserMessage.FK_SenderID && p.FK_FollowerTo == oUserMessage.FK_RecipientID).FirstOrDefault();
                        if (oFollower != null)
                        {
                            oFollower.DelFlag = true;


                        }



                    }
                    else if (oUserMessage.FK_MessageTypeID == 5)//Follower
                    {

                        Master_Follower oFollower = dbContext.Master_Follower.Where(p => p.FK_FollowerFrom == oUserMessage.FK_SenderID && p.FK_FollowerTo == oUserMessage.FK_RecipientID).FirstOrDefault();
                        if (oFollower != null)
                        {

                            Details_Follower oFollowerDetails = new Details_Follower
                            {

                                FK_MessageID = oUserMessage.PK_MessageID,
                                FK_FollowerID = oFollower.PK_FollowerID
                            };

                            dbContext.Details_Follower.Add(oFollowerDetails);

                        }
                    }
                    else if (oUserMessage.FK_MessageTypeID == 6)//Cheer
                    {
                        Master_Cheer oCheer = null;
                        oCheer = dbContext.Master_Cheer.Where(p => p.FK_CheerFromID == oUserMessage.FK_SenderID && p.FK_CheerToID == oUserMessage.FK_RecipientID).FirstOrDefault();
                        if (oCheer == null)
                        {
                            oCheer = new Master_Cheer
                            {
                                FK_MessageID = oUserMessage.PK_MessageID,
                                FK_CheerFromID = oUserMessage.FK_SenderID,
                                FK_CheerToID = oUserMessage.FK_RecipientID,
                                CreatedDate = DateTime.Today,
                                CheerFrom = GetUserName(oUserMessage.FK_SenderID),
                                CheerTo = GetUserName(oUserMessage.FK_RecipientID),
                            };
                            dbContext.Master_Cheer.Add(oCheer);
                        }
                        else
                        {
                            oCheer.DelFlag = false;
                        }
                        dbContext.SaveChanges();
                        int CheerCount = 0;

                        CheerCount = dbContext.Master_Cheer.Where(p => p.FK_CheerToID == oUserMessage.FK_RecipientID && p.DelFlag == false).Count();

                        dbContext.Master_User
                           .Where(p => p.PK_UserID == oUserMessage.FK_RecipientID)
                           .FirstOrDefault().CheerCount = CheerCount;
                    }

                    else if (oUserMessage.FK_MessageTypeID == 18)//un Cheer
                    {
                        Master_Cheer oCheer = null;
                        oCheer = dbContext.Master_Cheer.Where(p => p.FK_CheerFromID == oUserMessage.FK_SenderID && p.FK_CheerToID == oUserMessage.FK_RecipientID).FirstOrDefault();
                        if (oCheer != null)
                        {
                            oCheer.DelFlag = true;
                        }
                        dbContext.SaveChanges();
                        int CheerCount = 0;

                        CheerCount = dbContext.Master_Cheer.Where(p => p.FK_CheerToID == oUserMessage.FK_RecipientID && p.DelFlag == false).Count();

                        dbContext.Master_User
                           .Where(p => p.PK_UserID == oUserMessage.FK_RecipientID)
                           .FirstOrDefault().CheerCount = CheerCount;
                    }

                    dbContext.SaveChanges();
                    transaction.Commit();
                    oResponse.status = "200";
                    oResponse.status = "Success";
                    oResponse.result = oUserMessage.PK_MessageID;

                }
                catch (Exception ex)
                {
                    oResponse.errors = "Service Error";
                    oResponse.status = "Exception";
                    oResponse.result = ex.Message;
                    transaction.Rollback();

                }

            }


            return Ok(oResponse);
        }



        public void AddMsgToTrackedUser(UserMessage oUserMessage, int Type)
        {
            List<Master_Follower> oFollowersOfReceiver = dbContext.Master_Follower.Where(p => p.FK_FollowerTo == oUserMessage.FK_RecipientID && p.DelFlag == false).ToList();
            List<Master_Follower> oFollowersOfSender = dbContext.Master_Follower.Where(p => p.FK_FollowerTo == oUserMessage.FK_SenderID && p.DelFlag == false).ToList();

            foreach (var oFollower in oFollowersOfReceiver)
            {

                try
                {
                    Master_Messages oMessage = new Master_Messages();

                    oMessage.CreatedDate = DateTime.Today;
                    oMessage.FK_CreatedBy = oUserMessage.FK_SenderID;
                    oMessage.FK_MessageTypeID = Constants.Follower;
                    oMessage.FK_RecipientID = oFollower.FK_FollowerFrom;
                    oMessage.FK_SenderID = oUserMessage.FK_SenderID;
                    oMessage.SenderName = GetUserName(oUserMessage.FK_SenderID);
                    oMessage.RecipientName = GetUserName(oUserMessage.FK_RecipientID);

                    oUserMessage.IconPath = "../assets/images/track.svg";

                    if (Type == Constants.SparReceived)
                    {
                        oMessage.Subject = "Tracked Notification";
                        oMessage.Message = "<strong>" + "Message: " + "<strong>" + oMessage.SenderName + "</strong>" + " has sent a sparring request to " + "<strong>" + oMessage.RecipientName + "</strong>" + ".";
                    }
                   else if (Type == Constants.SparAccepted)
                    {
                        oMessage.Subject = "Tracked Notification";
                        oMessage.Message = "<strong>" + "Message: " + "<strong>" + oMessage.SenderName + "</strong>" + " has accepted sparring request from " + "<strong>" + oMessage.RecipientName + "</strong>" + ".";
                    }
                    else if (Type == Constants.SparRejected)
                    {
                        oMessage.Subject = "Tracked Notification";
                        oMessage.Message = "<strong>" + "Message: " + "<strong>" + oMessage.SenderName + "</strong>" + " has rejected sparring request from " + "<strong>" + oMessage.RecipientName + "</strong>" + ".";
                    }
                    else if (Type == Constants.RequestReceived)
                    {
                        oMessage.Subject = "Tracked Notification";
                        oMessage.Message = "<strong>" + "Message: " + "<strong>" + oMessage.SenderName + "</strong>" + " has sent a challenge request to " + "<strong>" + oMessage.RecipientName + "</strong>" + ".";
                    }
                    else if (Type == Constants.RequestRejected)
                    {
                        oMessage.Subject = "Tracked Notification";
                        oMessage.Message = "<strong>" + "Message: " + "<strong>" + oMessage.SenderName + "</strong>" + " has reject a challenge request from " + "<strong>" + oMessage.RecipientName + "</strong>" + ".";
                    }
                    else if (Type == Constants.RequestAccepted)
                    {
                        oMessage.Subject = "Tracked Notification";
                        oMessage.Message = "<strong>" + "Message: " + "<strong>" + oMessage.SenderName + "</strong>" + " has accepted a challenge request from" + "<strong>" + oMessage.RecipientName + "</strong>" + ".";
                    }

                    oMessage.IconPath = oUserMessage.IconPath;
                    oMessage.ReadFlag = false;
                    dbContext.Master_Messages.Add(oMessage);
                    dbContext.SaveChanges();


                }
                catch (Exception exc)
                {

                }

            }
            foreach (var oFollower in oFollowersOfSender)
            {

                try
                {
                    Master_Messages oMessage = new Master_Messages();

                    oMessage.CreatedDate = DateTime.Today;
                    oMessage.FK_CreatedBy = oUserMessage.FK_SenderID;
                    oMessage.FK_MessageTypeID = Constants.Follower;
                    oMessage.FK_RecipientID = oFollower.FK_FollowerFrom;
                    oMessage.FK_SenderID = oUserMessage.FK_SenderID;
                    oMessage.SenderName = GetUserName(oMessage.FK_SenderID);
                    oMessage.RecipientName = GetUserName(oUserMessage.FK_RecipientID);

                    oUserMessage.IconPath = "../assets/images/track.svg";
                     if (Type == Constants.SparReceived)
                    {
                        oMessage.Subject = "Tracked Notification";
                        oMessage.Message = "<strong>" + "Message: " + "<strong>" + oMessage.SenderName + "</strong>" + " has sent a sparring request to " + "<strong>" + oMessage.RecipientName + "</strong>" + ".";
                    }
                  else  if (Type == Constants.SparAccepted)
                    {
                        oMessage.Subject = "Tracked Notification";
                        oMessage.Message = "<strong>" + "Message: " + "<strong>" + oMessage.SenderName + "</strong>" + " has accepted sparring request from " + "<strong>" + oMessage.RecipientName + "</strong>" + ".";
                    }
                    else if (Type == Constants.SparRejected)
                    {
                        oMessage.Subject = "Tracked Notification";
                        oMessage.Message = "<strong>" + "Message: " + "<strong>" + oMessage.SenderName + "</strong>" + " has rejected sparring request from " + "<strong>" + oMessage.RecipientName + "</strong>" + ".";
                    }

                    else if (Type == Constants.RequestReceived)
                    {
                        oMessage.Subject = "Tracked Notification";
                        oMessage.Message = "<strong>" + "Message: " + "<strong>" + oMessage.SenderName + "</strong>" + " has sent a challenge request to " + "<strong>" + oMessage.RecipientName + "</strong>" + ".";
                    }
                    else if (Type == Constants.RequestRejected)
                    {
                        oMessage.Subject = "Tracked Notification";
                        oMessage.Message = "<strong>" + "Message: " + "<strong>" + oMessage.SenderName + "</strong>" + " has reject a challenge request from " + "<strong>" + oMessage.RecipientName + "</strong>" + ".";
                    }
                    else if (Type == Constants.RequestAccepted)
                    {
                        oMessage.Subject = "Tracked Notification";
                        oMessage.Message = "<strong>" + "Message: " + "<strong>" + oMessage.SenderName + "</strong>" + " has accepted a challenge request from " + "<strong>" + oMessage.RecipientName + "</strong>" + ".";
                    }
                    else if (Type == Constants.newBlog)
                    {
                        oMessage.Subject = "Tracked Notification";
                        oMessage.Message = "<strong>" + "Message: " + "<strong>" + oMessage.SenderName + "</strong>" + " has added a new blog.";
                    }
                    oMessage.IconPath = oUserMessage.IconPath;
                    oMessage.ReadFlag = false;
                    dbContext.Master_Messages.Add(oMessage);
                    dbContext.SaveChanges();


                }
                catch (Exception exc)
                {

                }


            }

        }

        private string FormatDate(DateTime SendDate)
        {

            string SendDateString = SendDate.ToString("MMMM") + " " + SendDate.Day + ", " + SendDate.Year;
            return SendDateString;
        }

        private string GetUserName(int UserID)
        {
            var User = (from user in dbContext.Master_User
                        where user.PK_UserID == UserID && user.DelFlag == false
                        select new { user.LegalFirstName, user.LegalMiddleName, user.LastName }).FirstOrDefault();

            if (User == null)
                return null;

            if (User.LegalMiddleName == null || User.LegalMiddleName == "" || User.LegalMiddleName == " ")
                return User.LegalFirstName + " " + User.LastName;
            else
                return User.LegalFirstName + " " + User.LegalMiddleName + " " + User.LastName;


        }



        [HttpGet]
        [Route("api/Message/GetUserMessages/{id}")]
        public IHttpActionResult GetUserMessages(int id)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                List<UserMessage> oUserMessage = new List<UserMessage>();

                oUserMessage = (from Message in dbContext.Master_Messages
                                where Message.DelFlag == false && Message.FK_RecipientID == id
                                select new UserMessage
                                {
                                    PK_MessageID = Message.PK_MessageID,
                                    SendDate = Message.CreatedDate,
                                    FK_MessageTypeID = Message.FK_MessageTypeID,
                                    FK_RecipientID = Message.FK_RecipientID,
                                    FK_SenderID = Message.FK_SenderID,
                                    Message = Message.Message,
                                    SenderName = Message.SenderName,
                                    RecipientName = Message.RecipientName,
                                    ParentMessageID = Message.ParentMessageID,
                                    Subject = Message.Subject,
                                    IconPath = Message.IconPath,
                                    ReadFlag = Message.ReadFlag,
                                    MessageStatus = Message.MessageStatus

                                }).OrderByDescending(p => p.PK_MessageID).ToList();

                if (oUserMessage.Count >= 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = oUserMessage;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }

        [HttpPost]
        [Route("api/Message/FilteredMessages")]
        public IHttpActionResult FilteredMessages(FilterMessage oFilterMessage)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                List<UserMessage> oUserMessage = new List<UserMessage>();

                if (oFilterMessage.AllMessage)
                {
                    oUserMessage = (from Message in dbContext.Master_Messages
                                    where Message.DelFlag == false && Message.FK_RecipientID == oFilterMessage.UserID
                                    select new UserMessage
                                    {
                                        PK_MessageID = Message.PK_MessageID,
                                        SendDate = Message.CreatedDate,
                                        FK_MessageTypeID = Message.FK_MessageTypeID,
                                        FK_RecipientID = Message.FK_RecipientID,
                                        FK_SenderID = Message.FK_SenderID,
                                        Message = Message.Message,
                                        SenderName = Message.SenderName,
                                        RecipientName = Message.RecipientName,
                                        ParentMessageID = Message.ParentMessageID,
                                        Subject = Message.Subject,
                                        IconPath = Message.IconPath,
                                        ReadFlag = Message.ReadFlag,
                                        MessageStatus = Message.MessageStatus

                                    }).OrderByDescending(p => p.PK_MessageID).ToList();

                }
                else
                {
                    List<UserMessage> oTempMessage = null;

                    if (oFilterMessage.ReadMessage)
                    {
                        oTempMessage = (from Message in dbContext.Master_Messages
                                        where Message.DelFlag == false && Message.FK_RecipientID == oFilterMessage.UserID && Message.ReadFlag == true
                                        select new UserMessage
                                        {
                                            PK_MessageID = Message.PK_MessageID,
                                            SendDate = Message.CreatedDate,
                                            FK_MessageTypeID = Message.FK_MessageTypeID,
                                            FK_RecipientID = Message.FK_RecipientID,
                                            FK_SenderID = Message.FK_SenderID,
                                            Message = Message.Message,
                                            SenderName = Message.SenderName,
                                            RecipientName = Message.RecipientName,
                                            ParentMessageID = Message.ParentMessageID,
                                            Subject = Message.Subject,
                                            IconPath = Message.IconPath,
                                            ReadFlag = Message.ReadFlag,
                                            MessageStatus = Message.MessageStatus

                                        }).OrderByDescending(p => p.PK_MessageID).ToList();

                        foreach (UserMessage UserMessage in oTempMessage)
                        {
                            var chkUserMsg = (from UserMsg in oUserMessage
                                              where UserMsg.PK_MessageID == UserMessage.PK_MessageID
                                              select UserMsg).FirstOrDefault();
                            if (chkUserMsg == null)
                            {
                                UserMessage TmpUserMessage = new UserMessage();
                                TmpUserMessage.FK_MessageTypeID = UserMessage.FK_MessageTypeID;
                                TmpUserMessage.FK_RecipientID = UserMessage.FK_RecipientID;
                                TmpUserMessage.FK_SenderID = UserMessage.FK_SenderID;
                                TmpUserMessage.IconPath = UserMessage.IconPath;
                                TmpUserMessage.Message = UserMessage.Message;
                                TmpUserMessage.MessageStatus = UserMessage.MessageStatus;
                                TmpUserMessage.ParentMessageID = UserMessage.ParentMessageID;
                                TmpUserMessage.PK_MessageID = UserMessage.PK_MessageID;
                                TmpUserMessage.ReadFlag = UserMessage.ReadFlag;
                                TmpUserMessage.RecipientName = UserMessage.RecipientName;
                                TmpUserMessage.SendDate = UserMessage.SendDate;
                                TmpUserMessage.SenderName = UserMessage.SenderName;
                                TmpUserMessage.Subject = UserMessage.Subject;

                                oUserMessage.Add(TmpUserMessage);
                            }


                        }
                    }
                    if (oFilterMessage.UnreadMessage)
                    {
                        oTempMessage = null;

                        oTempMessage = (from Message in dbContext.Master_Messages
                                        where Message.DelFlag == false && Message.FK_RecipientID == oFilterMessage.UserID && Message.ReadFlag == false
                                        select new UserMessage
                                        {
                                            PK_MessageID = Message.PK_MessageID,
                                            SendDate = Message.CreatedDate,
                                            FK_MessageTypeID = Message.FK_MessageTypeID,
                                            FK_RecipientID = Message.FK_RecipientID,
                                            FK_SenderID = Message.FK_SenderID,
                                            Message = Message.Message,
                                            SenderName = Message.SenderName,
                                            RecipientName = Message.RecipientName,
                                            ParentMessageID = Message.ParentMessageID,
                                            Subject = Message.Subject,
                                            IconPath = Message.IconPath,
                                            ReadFlag = Message.ReadFlag,
                                            MessageStatus = Message.MessageStatus

                                        }).OrderByDescending(p => p.PK_MessageID).ToList();

                        foreach (UserMessage UserMessage in oTempMessage)
                        {
                            var chkUserMsg = (from UserMsg in oUserMessage
                                              where UserMsg.PK_MessageID == UserMessage.PK_MessageID
                                              select UserMsg).FirstOrDefault();
                            if (chkUserMsg == null)
                            {
                                UserMessage TmpUserMessage = new UserMessage();
                                TmpUserMessage.FK_MessageTypeID = UserMessage.FK_MessageTypeID;
                                TmpUserMessage.FK_RecipientID = UserMessage.FK_RecipientID;
                                TmpUserMessage.FK_SenderID = UserMessage.FK_SenderID;
                                TmpUserMessage.IconPath = UserMessage.IconPath;
                                TmpUserMessage.Message = UserMessage.Message;
                                TmpUserMessage.MessageStatus = UserMessage.MessageStatus;
                                TmpUserMessage.ParentMessageID = UserMessage.ParentMessageID;
                                TmpUserMessage.PK_MessageID = UserMessage.PK_MessageID;
                                TmpUserMessage.ReadFlag = UserMessage.ReadFlag;
                                TmpUserMessage.RecipientName = UserMessage.RecipientName;
                                TmpUserMessage.SendDate = UserMessage.SendDate;
                                TmpUserMessage.SenderName = UserMessage.SenderName;
                                TmpUserMessage.Subject = UserMessage.Subject;

                                oUserMessage.Add(TmpUserMessage);
                            }


                        }
                    }
                    if (oFilterMessage.RejectedMessage)
                    {
                        oTempMessage = null;

                        oTempMessage = (from Message in dbContext.Master_Messages
                                        where Message.DelFlag == false && Message.FK_RecipientID == oFilterMessage.UserID && Message.MessageStatus == "REJECTED"
                                        select new UserMessage
                                        {
                                            PK_MessageID = Message.PK_MessageID,
                                            SendDate = Message.CreatedDate,
                                            FK_MessageTypeID = Message.FK_MessageTypeID,
                                            FK_RecipientID = Message.FK_RecipientID,
                                            FK_SenderID = Message.FK_SenderID,
                                            Message = Message.Message,
                                            SenderName = Message.SenderName,
                                            RecipientName = Message.RecipientName,
                                            ParentMessageID = Message.ParentMessageID,
                                            Subject = Message.Subject,
                                            IconPath = Message.IconPath,
                                            ReadFlag = Message.ReadFlag,
                                            MessageStatus = Message.MessageStatus

                                        }).OrderByDescending(p => p.PK_MessageID).ToList();
                        foreach (UserMessage UserMessage in oTempMessage)
                        {
                            var chkUserMsg = (from UserMsg in oUserMessage
                                              where UserMsg.PK_MessageID == UserMessage.PK_MessageID
                                              select UserMsg).FirstOrDefault();
                            if (chkUserMsg == null)
                            {
                                UserMessage TmpUserMessage = new UserMessage();
                                TmpUserMessage.FK_MessageTypeID = UserMessage.FK_MessageTypeID;
                                TmpUserMessage.FK_RecipientID = UserMessage.FK_RecipientID;
                                TmpUserMessage.FK_SenderID = UserMessage.FK_SenderID;
                                TmpUserMessage.IconPath = UserMessage.IconPath;
                                TmpUserMessage.Message = UserMessage.Message;
                                TmpUserMessage.MessageStatus = UserMessage.MessageStatus;
                                TmpUserMessage.ParentMessageID = UserMessage.ParentMessageID;
                                TmpUserMessage.PK_MessageID = UserMessage.PK_MessageID;
                                TmpUserMessage.ReadFlag = UserMessage.ReadFlag;
                                TmpUserMessage.RecipientName = UserMessage.RecipientName;
                                TmpUserMessage.SendDate = UserMessage.SendDate;
                                TmpUserMessage.SenderName = UserMessage.SenderName;
                                TmpUserMessage.Subject = UserMessage.Subject;

                                oUserMessage.Add(TmpUserMessage);
                            }


                        }
                    }
                    if (oFilterMessage.AcceptedMessage)
                    {

                        oTempMessage = null;

                        oTempMessage = (from Message in dbContext.Master_Messages
                                        where Message.DelFlag == false && Message.FK_RecipientID == oFilterMessage.UserID && Message.MessageStatus == "ACCEPTED" && Message.ReadFlag == true
                                        select new UserMessage
                                        {
                                            PK_MessageID = Message.PK_MessageID,
                                            SendDate = Message.CreatedDate,
                                            FK_MessageTypeID = Message.FK_MessageTypeID,
                                            FK_RecipientID = Message.FK_RecipientID,
                                            FK_SenderID = Message.FK_SenderID,
                                            Message = Message.Message,
                                            SenderName = Message.SenderName,
                                            RecipientName = Message.RecipientName,
                                            ParentMessageID = Message.ParentMessageID,
                                            Subject = Message.Subject,
                                            IconPath = Message.IconPath,
                                            ReadFlag = Message.ReadFlag,
                                            MessageStatus = Message.MessageStatus

                                        }).OrderByDescending(p => p.PK_MessageID).ToList();

                        foreach (UserMessage UserMessage in oTempMessage)
                        {
                            var chkUserMsg = (from UserMsg in oUserMessage
                                              where UserMsg.PK_MessageID == UserMessage.PK_MessageID
                                              select UserMsg).FirstOrDefault();
                            if (chkUserMsg == null)
                            {
                                UserMessage TmpUserMessage = new UserMessage();
                                TmpUserMessage.FK_MessageTypeID = UserMessage.FK_MessageTypeID;
                                TmpUserMessage.FK_RecipientID = UserMessage.FK_RecipientID;
                                TmpUserMessage.FK_SenderID = UserMessage.FK_SenderID;
                                TmpUserMessage.IconPath = UserMessage.IconPath;
                                TmpUserMessage.Message = UserMessage.Message;
                                TmpUserMessage.MessageStatus = UserMessage.MessageStatus;
                                TmpUserMessage.ParentMessageID = UserMessage.ParentMessageID;
                                TmpUserMessage.PK_MessageID = UserMessage.PK_MessageID;
                                TmpUserMessage.ReadFlag = UserMessage.ReadFlag;
                                TmpUserMessage.RecipientName = UserMessage.RecipientName;
                                TmpUserMessage.SendDate = UserMessage.SendDate;
                                TmpUserMessage.SenderName = UserMessage.SenderName;
                                TmpUserMessage.Subject = UserMessage.Subject;

                                oUserMessage.Add(TmpUserMessage);
                            }


                        }
                    }



                }
                if (oUserMessage.Count >= 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = oUserMessage.OrderByDescending(p => p.PK_MessageID).ToList();
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }









        [HttpPost]
        [Route("api/Message/MarkAsRead/{id}")]
        public IHttpActionResult MarkAsRead(int id)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {



                if (id == 0)
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                    return Ok(oResponse);
                }


                dbContext.Master_Messages
                                .Where(p => p.PK_MessageID == id)
                                .FirstOrDefault().ReadFlag = true;
                dbContext.SaveChanges();

                oResponse.status = "Success";
                oResponse.result = id.ToString();
                //  IHttpActionResult oResult = GetUserProfile(oUserDetails.PK_UserID);               
                // return oResult;
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;


            }

            return Ok(oResponse);
        }
    }
}