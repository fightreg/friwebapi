﻿using FRIWebAPI.Database;
using FRIWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using static FRIWebAPI.ClassFiles.Common;

namespace FRIWebAPI.Controllers
{
   
    public class FighterHomeController : ApiController
    {
        FRIEntities dbContext = null;
        public FighterHomeController()
        {
            dbContext = new FRIEntities();
        }

        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        [HttpGet]       
        public IHttpActionResult GetProfileDetails()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                List<FighterHomeModel> oFighter = new List<FighterHomeModel>();

                oFighter = (from user in dbContext.Master_User
                            where user.IsFeaturedfighter == true&&
                            user.FK_Status==Constants.Active
                            orderby user.ModifiedDate descending
                            select new FighterHomeModel
                            {
                                UserID = user.PK_UserID,
                                FullName = user.LegalFirstName + " " +user.LegalMiddleName+" "+ user.LastName,
                                FighterName = user.FighterName,
                                ProfilePhoto = user.ProfilePhotoPath,
                                IsFeaturedfighter = user.IsFeaturedfighter
                            }).ToList();

                if (oFighter.Count >= 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = oFighter;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;
                
            }

            return Ok(oResponse);
        }


    }
}
