﻿using FRIWebAPI.ClassFiles;
using FRIWebAPI.Database;
using FRIWebAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Xml.Linq;
using static FRIWebAPI.ClassFiles.Common;

namespace FRIWebAPI.Controllers
{
    public class UserController : ApiController
    {

        FRIEntities dbContext = null;
        Mail omail = new Mail();
        public UserController()
        {
            dbContext = new FRIEntities();
        }

        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        [HttpPost]
        [Route("api/User/UserSiginIn")]
        public IHttpActionResult UserSiginIn(LoginInfo oUserSignIn)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                if (oUserSignIn == null)
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                    return Ok(oResponse);
                }
                string encryptedPassword = FRICrypto.EncryptString(oUserSignIn.Password, "FRI@AOT");
                UserSignIn SignInUser = (from user in dbContext.Master_User
                                         where user.EmailAddress.Equals(oUserSignIn.EmailAddress, StringComparison.InvariantCultureIgnoreCase) && (encryptedPassword == user.Password)
                                         //&& user.FK_Status == Constants.Active
                                         select new UserSignIn
                                         {
                                             PK_UserID = user.PK_UserID,
                                             LastName = user.LastName,
                                             LegalFirstName = user.LegalFirstName,
                                             EmailAddress = user.EmailAddress,
                                             ProfilePhotoPath = user.ProfilePhotoPath,
                                             IsSuperadmin = user.IsSuperAdmin,
                                             ProfileStatus = user.FK_Status,
                                         }).FirstOrDefault();

                if (SignInUser != null)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = SignInUser;
                    return Ok(oResponse);
                }
                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "Invalid Login Credentials.";
                    oResponse.result = null;
                    return Ok(oResponse);
                }

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }
            return Ok(oResponse);
        }
        [HttpPost]
        [Route("api/User/UserSignOut/{Id}")]
        public IHttpActionResult UserSignOut(int Id)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                UserSignIn SignInUser = (from user in dbContext.Master_User
                                         where user.PK_UserID == Id

                                         select new UserSignIn
                                         {
                                             PK_UserID = user.PK_UserID,
                                             LastName = user.LastName,
                                             ProfileStatus = user.FK_Status,
                                         }).FirstOrDefault();

                if (SignInUser != null)
                {
                    string result = new Common().RemoveToken(Id);
                    if (result == "Success")
                    {
                        oResponse.status = "Success";
                        oResponse.errors = null;
                        oResponse.result = "Success";
                        return Ok(oResponse);
                    }
                    else
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }
                }
                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                    return Ok(oResponse);
                }

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }
            return Ok(oResponse);
        }

        [HttpGet]
        [Route("api/User/GetUserSelectedRoles/{id}")] //   /api/Lookup//GetUserRoles?Id=8
        public IHttpActionResult GetUserSelectedRoles(int id)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var result = (from MapUserRole in dbContext.Map_User_Role

                              select new { RoleId = MapUserRole.FK_RoleID, Description = MapUserRole.Lookup_Role.Description, UnqID = MapUserRole.PK_RoleMapID, UserId = MapUserRole.FK_UserID, DelFlag = MapUserRole.DelFlag }).OrderBy(o => new { o.RoleId }).Where(W => W.UserId == id && W.DelFlag == false).ToList().Distinct();

                if (result.Count() > 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = result;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);


        }
        
        [HttpGet]
        [Route("api/User/GetUserSelectedProfiles/{id}")] //   /api/Lookup//GetUserRoles?Id=8
        public IHttpActionResult GetUserSelectedProfiles(string id)
        {
            string data = FRICrypto.decryption(id);
            int userId = Convert.ToInt32(data);
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var result = (from MapUserProfile in dbContext.Map_User_Profile

                              select new { ProfileId = MapUserProfile.FK_ProfileID, Description = MapUserProfile.Lookup_Profile.Description, UnqID = MapUserProfile.PK_ProfileMapID, UserId = MapUserProfile.FK_UserID, DelFlag = MapUserProfile.DelFlag, profilestatus = MapUserProfile.Master_User.FK_Status }).OrderBy(o => new { o.ProfileId }).Where(W => W.UserId == userId && W.DelFlag == false).ToList().Distinct();

                if (result.Count() > 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = result;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);


        }
        /// <summary>
        /// Created by:Vinaya Joseph
        /// Date:25/June/2017
        /// Description:To Update profile status 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/User/Updatestatus")]
        public HttpResponseMessage Updatestatus(string Id, int status)
        {
            int iUserId = Convert.ToInt32(Id);
            string Response = string.Empty;
            string BodyHtml = string.Empty;
            var response = new HttpResponseMessage();
            try
            {
                var result = dbContext.Master_User
                                .FirstOrDefault(p => p.PK_UserID == iUserId);
                if (result != null)
                {
                    result.FK_Status = status;
                }
                dbContext.SaveChanges();
                if (result.PK_UserID > 0)
                {

                    if (status == Constants.Active)
                    {
                        BodyHtml = omail.GenerateMailResponse(Constants.commontemplate, Constants.accepted);
                        omail.SelectMailTemplate(iUserId, Constants.commontemplate, Constants.accepted, result.LegalFirstName);
                    }
                    else if (status == Constants.Rejected)
                    {
                        BodyHtml = omail.GenerateMailResponse(Constants.commontemplate, Constants.rejected);
                        omail.SelectMailTemplate(iUserId, Constants.commontemplate, Constants.rejected, result.LegalFirstName);
                    }


                    response.Content = new StringContent(BodyHtml);
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                }

                else
                {
                    Response = "Failed";
                }
            }
            catch (Exception ex)
            {
                Response = "Service Error";


            }

            return response;


        }
        [HttpGet]
        [Route("api/User/Updatestatus/{Id}/{status}")]
        public IHttpActionResult Updatestatus(int Id, int status)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var result = dbContext.Master_User
                                .FirstOrDefault(p => p.PK_UserID == Id);
                if (result != null)
                {
                    result.FK_Status = status;
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = result;
                }
                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
                dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);


        }
        /// <summary>
        /// Created by:Vinaya Joseph
        /// Date:25/June/2017
        /// Description:To Update featured fighter status
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/User/UpdateFeaturedFighter")]//for mail
        public HttpResponseMessage UpdateFeaturedFighter(string Id, bool status)
        {
            int iUserId = Convert.ToInt32(Id);
            string Response = string.Empty;
            string BodyHtml = string.Empty;
            var response = new HttpResponseMessage();
            try
            {
                var result = dbContext.Master_User
                                .FirstOrDefault(p => p.PK_UserID == iUserId);
                if (result != null)
                {
                    result.IsFeaturedfighter = status;
                }
                dbContext.SaveChanges();
                if (result.PK_UserID > 0)
                {
                    if (status == true)
                    {
                        BodyHtml = omail.GenerateMailResponse(Constants.commontemplate, Constants.featuredfighteron); //Mail response
                        omail.SelectMailTemplate(iUserId, Constants.commontemplate, Constants.featuredfighteron, result.LegalFirstName);//to send mail to user
                    }
                    else if (status == false)
                    {
                        BodyHtml = omail.GenerateMailResponse(Constants.commontemplate, Constants.featuredfighteroff);
                        omail.SelectMailTemplate(iUserId, Constants.commontemplate, Constants.featuredfighteroff, result.LegalFirstName);
                    }

                    response.Content = new StringContent(BodyHtml);
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                }

                else
                {
                    Response = "Failed";
                }
            }
            catch (Exception ex)
            {
                Response = "Service Error";


            }

            return response;


        }
        [HttpGet]
        // [Route("api/User/UpdateFeaturedFighterstatus{Id}/{status}")]//for app
        [Route("api/User/UpdateFeaturedFighterstatus/{Id}/{status}")]
        public IHttpActionResult UpdateFeaturedFighterstatus(int Id, int status)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var result = dbContext.Master_User
                                .FirstOrDefault(p => p.PK_UserID == Id);
                if (result != null)
                {
                    result.IsFeaturedfighter = Convert.ToBoolean(status);
                    result.ModifiedDate = DateTime.UtcNow;
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = result;
                }
                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
                dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }
            return Ok(oResponse);
        }
        /// <summary>
        /// Created by:Vinaya Joseph
        /// Date:25/June/2017
        /// Description:To Update profile status for fighter
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/User/UpdateProfileStatus")]
        public HttpResponseMessage UpdateProfileStatus(string Id, bool status)
        {
            int iUserId = Convert.ToInt32(Id);
            string Response = string.Empty;
            string BodyHtml = string.Empty;
            var response = new HttpResponseMessage();
            try
            {
                var result = dbContext.Master_Profile
                                .FirstOrDefault(p => p.FK_UserID == iUserId);
                if (result != null)
                {
                    result.FR_ProfileStatus = status;
                }
                dbContext.SaveChanges();
                if (result.FK_UserID > 0)
                {
                    if (status == true)
                    {
                        BodyHtml = omail.GenerateMailResponse(Constants.commontemplate, Constants.profileon);
                        omail.SelectMailTemplate(iUserId, Constants.commontemplate, Constants.profileon, result.Master_User.LegalFirstName);
                    }
                    else if (status == false)
                    {
                        BodyHtml = omail.GenerateMailResponse(Constants.commontemplate, Constants.profileoff);
                        omail.SelectMailTemplate(iUserId, Constants.commontemplate, Constants.profileoff, result.Master_User.LegalFirstName);
                    }

                    response.Content = new StringContent(BodyHtml);
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                }

                else
                {
                    Response = "Failed";
                }
            }
            catch (Exception ex)
            {
                Response = "Service Error";


            }

            return response;


        }
    }
}