﻿using FRIWebAPI.ClassFiles;
using FRIWebAPI.Database;
using FRIWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Cors;
using static FRIWebAPI.ClassFiles.Common;

namespace FRIWebAPI.Controllers
{
    public class ProfileController : ApiController
    {
        FRIEntities dbContext = null;
        Mail omail = new Mail();
        public ProfileController()
        {
            dbContext = new FRIEntities();
        }

        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        public class CustomMultipartFormDataStreamProvider : MultipartFormDataStreamProvider
        {
            public CustomMultipartFormDataStreamProvider(string path) : base(path) { }

            public override string GetLocalFileName(HttpContentHeaders headers)
            {
                return headers.ContentDisposition.FileName.Replace("\"", string.Empty);
            }
        }

        //[HttpPost]
        //[Route("api/Profile/UploadProfilePic")]
        //public async Task<HttpResponseMessage> UploadProfilePic()
        //{
        //    // Check whether the POST operation is MultiPart?
        // //   if (!Request.Content.IsMimeMultipartContent())
        // //   {
        //       // throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
        // //   }

        //    // Prepare CustomMultipartFormDataStreamProvider in which our multipart form
        //    // data will be loaded.
        //    string fileSaveLocation = HttpContext.Current.Server.MapPath("~/Uploads/ProfilePhoto");
        //    CustomMultipartFormDataStreamProvider provider = new CustomMultipartFormDataStreamProvider(fileSaveLocation);
        //    List<string> files = new List<string>();

        //    try
        //    {
        //        // Read all contents of multipart message into CustomMultipartFormDataStreamProvider.
        //        await Request.Content.ReadAsMultipartAsync(provider);

        //        foreach (MultipartFileData file in provider.FileData)
        //        {
        //            files.Add(Path.GetFileName(file.LocalFileName));
        //        }

        //        // Send OK Response along with saved file names to the client.
        //        return Request.CreateResponse(HttpStatusCode.OK, files);
        //    }
        //    catch (System.Exception e)
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
        //    }
        //}




        [HttpPost]
        [Route("api/Profile/CreateProfilePhoto")]
        public ReturnResponse CreateProfilePhoto()
        {
            ReturnResponse oResponse = new ReturnResponse();
            PhofilePhoto oprofilepic = new PhofilePhoto();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count > 0)
                {

                    var postedFile = httpRequest.Files[0];
                    string ext = System.IO.Path.GetExtension(postedFile.FileName);
                    string csNewFileName = Guid.NewGuid().ToString() + ext;
                    var filePath = HttpContext.Current.Server.MapPath("~/Uploads/ProfilePhoto/" + csNewFileName);
                    postedFile.SaveAs(filePath);

                    // System.IO.Stream fs = postedFile.InputStream;
                    // System.IO.BinaryReader br = new System.IO.BinaryReader(fs);
                    //Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                    //string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                    //string Base64Image =  base64String;
                    string Imageurl = WebConfigurationManager.AppSettings["Imageurl"];
                    Imageurl = Imageurl + "ProfilePhoto/";
                    oprofilepic.Imageurl = Imageurl + csNewFileName;
                    //oprofilepic.Base64image = Base64Image;

                    dbContext.SaveChanges();
                    oResponse.status = "200";
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = oprofilepic;



                }

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }
            return oResponse;
        }



        [HttpPost]
        [Route("api/Profile/UploadProfilePhoto/{id}")]
        public ReturnResponse UploadProfilePhoto(int id)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {




                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count > 0)
                {

                    var postedFile = httpRequest.Files[0];
                    string ext = System.IO.Path.GetExtension(postedFile.FileName);
                    var filePath = HttpContext.Current.Server.MapPath("~/Uploads/ProfilePhoto/" + id.ToString() + ext);
                    postedFile.SaveAs(filePath);

                    Master_User oUser = (from u in dbContext.Master_User
                                         where u.PK_UserID == id
                                         select u).FirstOrDefault();

                    if (oUser != null)
                    {
                        string Imageurl = WebConfigurationManager.AppSettings["Imageurl"];
                        Imageurl = Imageurl + "ProfilePhoto/";
                        oUser.ProfilePhotoPath = Imageurl + id.ToString() + ext;
                        dbContext.SaveChanges();
                        oResponse.status = "200";
                        oResponse.status = "Success";
                        oResponse.errors = null;
                        oResponse.result = oUser.ProfilePhotoPath.ToString();


                    }




                }

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }
            return oResponse;




            //HttpResponseMessage response = new HttpResponseMessage();
            //var httpRequest = HttpContext.Current.Request;
            //if (httpRequest.Files.Count > 0)
            //{
            //    foreach (string file in httpRequest.Files)
            //    {
            //        var postedFile = httpRequest.Files[file];
            //        var filePath = HttpContext.Current.Server.MapPath("~/Uploads/ProfilePhoto/" + postedFile.FileName);
            //        postedFile.SaveAs(filePath);
            //    }
            //}
            //return response;



            //if (HttpContext.Current.Request.Files.AllKeys.Any())
            //{
            //    // Get the uploaded image from the Files collection
            //    var httpPostedFile = HttpContext.Current.Request.Files["UploadedFile"];

            //    if (httpPostedFile != null)
            //    {
            //        // Validate the uploaded image(optional)

            //        // Get the complete file path
            //        var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Uploads/ProfilePhoto"), httpPostedFile.FileName);

            //        httpPostedFile.SaveAs(fileSavePath);
            //    }
            //}
        }



        [HttpPost]
        [Route("api/Profile/UploadPhoto")]
        public ReturnResponse UploadPhoto()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count > 0)
                {

                    var postedFile = httpRequest.Files[0];
                    string ext = System.IO.Path.GetExtension(postedFile.FileName);
                    string csNewFileName = Guid.NewGuid().ToString() + ext;
                    var filePath = HttpContext.Current.Server.MapPath("~/Uploads/Photos/" + csNewFileName);
                    postedFile.SaveAs(filePath);

                    dbContext.SaveChanges();
                    oResponse.status = "200";
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    string Imageurl = WebConfigurationManager.AppSettings["Imageurl"];
                    Imageurl = Imageurl + "Photos/";
                    oResponse.result = Imageurl + csNewFileName;



                }

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }
            return oResponse;
        }



        [HttpPost]
        [Route("api/Profile/EmailDuplicationCheck")]
        public IHttpActionResult EmailDuplicationCheck(DemoGraphicInfo oDemoGraphicInfo)
        {
            ReturnResponse oResponse = new ReturnResponse();

            try
            {
                Master_User MasterUser = (from user in dbContext.Master_User
                                          where user.EmailAddress == oDemoGraphicInfo.EmailAddress && user.PK_UserID != oDemoGraphicInfo.PK_UserID
                                          select user).FirstOrDefault();
                if (MasterUser != null)
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "Email address duplication.";
                    oResponse.result = null;
                    return Ok(oResponse);
                }
                else

                {
                    oResponse.status = "200";
                    oResponse.status = "Success";
                    return Ok(oResponse);
                }



            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);

        }


        [HttpPost]
        [Route("api/Profile/UploadVideo")]
        public ReturnResponse UploadVideo()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count > 0)
                {

                    var postedFile = httpRequest.Files[0];
                    string ext = System.IO.Path.GetExtension(postedFile.FileName);
                    string csNewFileName = Guid.NewGuid().ToString() + ext;
                    var filePath = HttpContext.Current.Server.MapPath("~/Uploads/Video/" + csNewFileName);
                    postedFile.SaveAs(filePath);

                    dbContext.SaveChanges();
                    oResponse.status = "200";
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    string Imageurl = WebConfigurationManager.AppSettings["Imageurl"];
                    Imageurl = Imageurl + "Video/";
                    oResponse.result = Imageurl + csNewFileName;

                }

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }
            return oResponse;
        }


        [HttpPost]
        [Route("api/Profile/CreateProfile")]
        public IHttpActionResult CreateProfile(DemoGraphicInfo UserDemoGraphicInfo)
        {
            ReturnResponse oResponse = new ReturnResponse();

            using (var transaction = dbContext.Database.BeginTransaction())
            {
                try
                {

                    Master_User oUser = new Master_User();

                    //Fill custom data
                    UserDemoGraphicInfo.CreatedDate = DateTime.Today;

                    //Adding user data
                    oUser.City = UserDemoGraphicInfo.City;
                    oUser.CreatedDate = UserDemoGraphicInfo.CreatedDate;

                    //oUser.DOB = UserDemoGraphicInfo.DOB;
                    oUser.EmailAddress = UserDemoGraphicInfo.EmailAddress;
                    oUser.FK_Country = UserDemoGraphicInfo.FK_Country;
                    oUser.FK_ProvinceOrState = UserDemoGraphicInfo.FK_ProvinceOrState;
                    oUser.Gender = UserDemoGraphicInfo.Gender;
                    oUser.LastName = UserDemoGraphicInfo.LastName;
                    oUser.LegalFirstName = UserDemoGraphicInfo.LegalFirstName;
                    oUser.LegalMiddleName = UserDemoGraphicInfo.LegalMiddleName;
                    oUser.Password = FRICrypto.EncryptString(UserDemoGraphicInfo.Password, "FRI@AOT");
                    oUser.PersonalWebsite = UserDemoGraphicInfo.PersonalWebsite;
                    oUser.Phone = UserDemoGraphicInfo.Phone;
                    oUser.PostalOrZipCode = UserDemoGraphicInfo.PostalOrZipCode;
                    oUser.ProfilePhotoPath = UserDemoGraphicInfo.ProfilePhotoPath;
                    oUser.Street = UserDemoGraphicInfo.Street;
                    oUser.IsAmbassador = UserDemoGraphicInfo.IsAmbassador;
                    oUser.SecurityQuestion = UserDemoGraphicInfo.SecurityQuestion;
                    oUser.SecurityAnswer = UserDemoGraphicInfo.SecurityAnswer;
                    oUser.EmergencyContactName = UserDemoGraphicInfo.EmergencyContactName;
                    oUser.EmergencyContactNumber = UserDemoGraphicInfo.EmergencyContactNumber;
                    oUser.FK_Status = Constants.Active;
                    dbContext.Master_User.Add(oUser);
                    dbContext.SaveChanges();


                    //Get userId
                    UserDemoGraphicInfo.PK_UserID = oUser.PK_UserID;

                    var CountryDetails = (from Country in dbContext.Lookup_Country
                                          select new { Country.Description, Country.PK_CountryID }).Where(W => W.PK_CountryID == UserDemoGraphicInfo.FK_Country).FirstOrDefault();

                    if (CountryDetails != null)
                        oUser.CountryName = CountryDetails.Description;


                    var StateDetails = (from State in dbContext.Lookup_StateOrProvince
                                        select new { State.Description, State.PK_StateOrProvinceID }).Where(W => W.PK_StateOrProvinceID == UserDemoGraphicInfo.FK_ProvinceOrState).FirstOrDefault();

                    if (StateDetails != null)
                        oUser.StateName = StateDetails.Description;


                    // Fill social media
                    int Count = UserDemoGraphicInfo.SocialMediaRecords.Count();
                    for (int index = 0; index < Count; index++)
                    {
                        // Adding custom data
                        UserDemoGraphicInfo.SocialMediaRecords[index].CreatedDate = DateTime.Today;
                        UserDemoGraphicInfo.SocialMediaRecords[index].FK_CreatedBy = UserDemoGraphicInfo.PK_UserID;
                        UserDemoGraphicInfo.SocialMediaRecords[index].FK_UserID = UserDemoGraphicInfo.PK_UserID;

                        // Adding social media
                        Details_UserSocialMedia oSocialMediaRecord = new Details_UserSocialMedia();
                        oSocialMediaRecord.CreatedDate = UserDemoGraphicInfo.SocialMediaRecords[index].CreatedDate;
                        oSocialMediaRecord.FK_SocialMediaID = UserDemoGraphicInfo.SocialMediaRecords[index].FK_SocialMediaID;
                        oSocialMediaRecord.SocialMedia = UserDemoGraphicInfo.SocialMediaRecords[index].SocialMedia;
                        oSocialMediaRecord.URL = UserDemoGraphicInfo.SocialMediaRecords[index].URL;
                        oSocialMediaRecord.FK_UserID = UserDemoGraphicInfo.PK_UserID;
                        oSocialMediaRecord.FK_CreatedBy = UserDemoGraphicInfo.PK_UserID;
                        dbContext.Details_UserSocialMedia.Add(oSocialMediaRecord);
                        dbContext.SaveChanges();

                        // Get SocialMediaID
                        UserDemoGraphicInfo.SocialMediaRecords[index].PK_UserSocialMediaID = oSocialMediaRecord.PK_UserSocialMediaID;

                    }
                    Master_Profile oProfile = new Master_Profile();
                    oProfile.FK_UserID = UserDemoGraphicInfo.PK_UserID;
                    oProfile.FR_ProfileStatus = true;
                    dbContext.Master_Profile.Add(oProfile);
                    dbContext.SaveChanges();



                    Map_User_Profile UserProfileMap = new Map_User_Profile
                    {
                        FK_ProfileID = (byte)PROFILES.Fan,
                        FK_UserID = UserDemoGraphicInfo.PK_UserID,
                        CreatedDate = DateTime.Today,
                        FK_CreatedBy = UserDemoGraphicInfo.PK_UserID

                    };
                    dbContext.Map_User_Profile.Add(UserProfileMap);

                    Map_User_Role UserRoleMap = new Map_User_Role
                    {
                        FK_RoleID = (byte)ROLES.Fan,
                        FK_UserID = UserDemoGraphicInfo.PK_UserID,
                        CreatedDate = DateTime.Today,
                        FK_CreatedBy = UserDemoGraphicInfo.PK_UserID

                    };
                    dbContext.Map_User_Role.Add(UserRoleMap);


                    dbContext.SaveChanges();

                    transaction.Commit();
                    oResponse.status = "200";
                    oResponse.status = "Success";
                    CreateUserDetails oCreateUserDetails = new CreateUserDetails();
                    oCreateUserDetails.PK_UserID = UserDemoGraphicInfo.PK_UserID;
                    oCreateUserDetails.PK_UserProfileID = oProfile.PK_UserProfileID;
                    oResponse.result = oCreateUserDetails;

                    //  IHttpActionResult oResult = GetUserProfile(UserDemoGraphicInfo.PK_UserID);
                    // return oResult;
                }
                catch (Exception ex)
                {
                    oResponse.errors = "Service Error";
                    oResponse.status = "Exception";
                    oResponse.result = ex.Message;
                    transaction.Rollback();

                }

            }


            return Ok(oResponse);
        }


        [HttpPost]
        [Route("api/Profile/CreateCommission")]
        public IHttpActionResult CreateCommission(CommissionInfo CreateCommissionDetails)
        {
            ReturnResponse oResponse = new ReturnResponse();
            using (var transaction = dbContext.Database.BeginTransaction())
            {
                try
                {

                    Master_User oUser = new Master_User();

                    //Fill custom data
                    CreateCommissionDetails.CreatedDate = DateTime.Today;

                    //Adding user data               
                    oUser.EmailAddress = CreateCommissionDetails.EmailAddress;
                    oUser.Password = FRICrypto.EncryptString(CreateCommissionDetails.Password, "FRI@AOT");
                    oUser.SecurityQuestion = CreateCommissionDetails.SecurityQuestion;
                    oUser.SecurityAnswer = CreateCommissionDetails.SecurityAnswer;
                    oUser.CreatedDate = DateTime.Now;
                    oUser.LegalFirstName = CreateCommissionDetails.Title;
                    oUser.ProfilePhotoPath = CreateCommissionDetails.ProfilePhotoPath;

                    oUser.Phone = CreateCommissionDetails.Phone;
                    oUser.FK_Country = CreateCommissionDetails.FK_Country;
                    oUser.FK_ProvinceOrState = CreateCommissionDetails.FK_ProvinceOrState;
                    oUser.City = CreateCommissionDetails.City;
                    oUser.Street = CreateCommissionDetails.Street;
                    oUser.PostalOrZipCode = CreateCommissionDetails.PostalOrZipCode;
                    oUser.FK_Status = Constants.Pendingstaus;

                    // Add created by.
                    dbContext.Master_User.Add(oUser);
                    dbContext.SaveChanges();
                    //Get userId
                    CreateCommissionDetails.PK_UserID = oUser.PK_UserID;

                    var CountryDetails = (from Country in dbContext.Lookup_Country
                                          select new { Country.Description, Country.PK_CountryID }).Where(W => W.PK_CountryID == CreateCommissionDetails.FK_Country).FirstOrDefault();

                    if (CountryDetails != null)
                        oUser.CountryName = CountryDetails.Description;


                    var StateDetails = (from State in dbContext.Lookup_StateOrProvince
                                        select new { State.Description, State.PK_StateOrProvinceID }).Where(W => W.PK_StateOrProvinceID == CreateCommissionDetails.FK_ProvinceOrState).FirstOrDefault();

                    if (StateDetails != null)
                        oUser.StateName = StateDetails.Description;

                    // Fill social media
                    int Count = CreateCommissionDetails.SocialMediaRecords.Count();
                    for (int index = 0; index < Count; index++)
                    {
                        // Adding custom data
                        CreateCommissionDetails.SocialMediaRecords[index].CreatedDate = DateTime.Today;
                        CreateCommissionDetails.SocialMediaRecords[index].FK_CreatedBy = CreateCommissionDetails.PK_UserID;
                        CreateCommissionDetails.SocialMediaRecords[index].FK_UserID = CreateCommissionDetails.PK_UserID;

                        // Adding social media
                        Details_UserSocialMedia oSocialMediaRecord = new Details_UserSocialMedia();
                        oSocialMediaRecord.CreatedDate = CreateCommissionDetails.SocialMediaRecords[index].CreatedDate;
                        oSocialMediaRecord.FK_SocialMediaID = CreateCommissionDetails.SocialMediaRecords[index].FK_SocialMediaID;
                        oSocialMediaRecord.SocialMedia = CreateCommissionDetails.SocialMediaRecords[index].SocialMedia;
                        oSocialMediaRecord.URL = CreateCommissionDetails.SocialMediaRecords[index].URL;
                        oSocialMediaRecord.FK_UserID = CreateCommissionDetails.PK_UserID;
                        oSocialMediaRecord.FK_CreatedBy = CreateCommissionDetails.PK_UserID;
                        dbContext.Details_UserSocialMedia.Add(oSocialMediaRecord);
                        dbContext.SaveChanges();

                        // Get SocialMediaID
                        CreateCommissionDetails.SocialMediaRecords[index].PK_UserSocialMediaID = oSocialMediaRecord.PK_UserSocialMediaID;

                    }

                    Master_Profile oProfile = new Master_Profile();
                    oProfile.FK_UserID = CreateCommissionDetails.PK_UserID;
                    oProfile.CN_Title = CreateCommissionDetails.Title;
                    oProfile.CN_Description = CreateCommissionDetails.Description;
                    oProfile.CN_FK_Country = CreateCommissionDetails.FK_Country;
                    oProfile.CN_FK_ProvinceOrState = CreateCommissionDetails.FK_ProvinceOrState;
                    oProfile.CN_City = CreateCommissionDetails.City;
                    oProfile.CN_Street = CreateCommissionDetails.Street;
                    oProfile.CN_PostalOrZipCode = CreateCommissionDetails.PostalOrZipCode;
                    oProfile.CN_phone = CreateCommissionDetails.Phone;

                    oProfile.CN_DistrictOfAuthorization = CreateCommissionDetails.DistrictOfAuthorization;
                    oProfile.CN_AuthorizingBody = CreateCommissionDetails.AuthorizingBody;
                    oProfile.CN_webaddress = CreateCommissionDetails.Website;
                    oProfile.CN_webaddress = CreateCommissionDetails.Website;
                    oProfile.CN_PriContactLegalFirstName = CreateCommissionDetails.PriContactLegalFirstName;
                    oProfile.CN_PriContactLastName = CreateCommissionDetails.PriContactLastName;
                    oProfile.CN_PriContactLegalMiddleName = CreateCommissionDetails.PriContactLegalMiddleName;
                    oProfile.CN_PrimaryEmail = CreateCommissionDetails.EmailAddress;
                    oProfile.CN_SecContactLegalFirstName = CreateCommissionDetails.SecContactLegalFirstName;
                    oProfile.CN_SecContactLastName = CreateCommissionDetails.SecContactLastName;
                    oProfile.CN_SecContactLegalMiddleName = CreateCommissionDetails.SecContactLegalMiddleName;
                    oProfile.CN_SecondaryEmail = CreateCommissionDetails.SecContactEmail;
                    oProfile.CN_PrimaryContact = CreateCommissionDetails.PriContactEmail;
                    dbContext.Master_Profile.Add(oProfile);
                    dbContext.SaveChanges();


                    UpdateProfileAndRole(ref oUser, oUser.PK_UserID, (byte)ROLES.MemberCommission, (byte)PROFILES.Commission);




                    dbContext.SaveChanges();

                    transaction.Commit();
                    oResponse.status = "200";
                    oResponse.status = "Success";
                    oResponse.result = CreateCommissionDetails.PK_UserID;
                    omail.SelectMailTemplate(oUser.PK_UserID, Constants.commisionapproval, Constants.newcommission, oUser.LegalFirstName);
                    //  IHttpActionResult oResult = GetUserProfile(UserDemoGraphicInfo.PK_UserID);
                    // return oResult;
                }
                catch (Exception ex)
                {
                    oResponse.errors = "Service Error";
                    oResponse.status = "Exception";
                    oResponse.result = ex.Message;
                    transaction.Rollback();

                }

            }


            return Ok(oResponse);
        }
        [HttpPost]
        [Route("api/Profile/UpdateCommission")]
        public IHttpActionResult UpdateCommission(CommissionInfo UpdateCommissionDetails)
        {
            ReturnResponse oResponse = new ReturnResponse();
            using (var transaction = dbContext.Database.BeginTransaction())
            {
                try
                {

                    if (UpdateCommissionDetails == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data available";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    Master_User MasterUser = (from user in dbContext.Master_User
                                              where user.PK_UserID == UpdateCommissionDetails.PK_UserID && user.DelFlag == false
                                              select user).FirstOrDefault();
                    if (MasterUser == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data available.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    Master_Profile MasterProfile = (from userprofile in MasterUser.Master_Profile
                                                    where userprofile.FK_UserID == UpdateCommissionDetails.PK_UserID
                                                    select userprofile).FirstOrDefault();
                    if (MasterProfile == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }


                    //Update date              
                    MasterUser.EmailAddress = UpdateCommissionDetails.EmailAddress;
                    MasterUser.SecurityQuestion = UpdateCommissionDetails.SecurityQuestion;
                    MasterUser.SecurityAnswer = UpdateCommissionDetails.SecurityAnswer;
                    MasterUser.LegalFirstName = UpdateCommissionDetails.Title;
                    MasterUser.ProfilePhotoPath = UpdateCommissionDetails.ProfilePhotoPath;

                    MasterUser.Phone = UpdateCommissionDetails.Phone;
                    MasterUser.FK_Country = UpdateCommissionDetails.FK_Country;
                    MasterUser.FK_ProvinceOrState = UpdateCommissionDetails.FK_ProvinceOrState;
                    MasterUser.City = UpdateCommissionDetails.City;
                    MasterUser.Street = UpdateCommissionDetails.Street;
                    MasterUser.PostalOrZipCode = UpdateCommissionDetails.PostalOrZipCode;

                    var CountryDetails = (from Country in dbContext.Lookup_Country
                                          select new { Country.Description, Country.PK_CountryID }).Where(W => W.PK_CountryID == UpdateCommissionDetails.FK_Country).FirstOrDefault();

                    if (CountryDetails != null)
                        MasterUser.CountryName = CountryDetails.Description;


                    var StateDetails = (from State in dbContext.Lookup_StateOrProvince
                                        select new { State.Description, State.PK_StateOrProvinceID }).Where(W => W.PK_StateOrProvinceID == UpdateCommissionDetails.FK_ProvinceOrState).FirstOrDefault();

                    if (StateDetails != null)
                        MasterUser.StateName = StateDetails.Description;

                    int UserID = MasterUser.PK_UserID;
                    if (UpdateCommissionDetails.SocialMediaRecords.Count > 0)
                    {
                        dbContext.Details_UserSocialMedia
                                .Where(p => p.FK_UserID == UserID)
                                .ToList().ForEach(r => r.DelFlag = true);
                    }
                    for (int Index = 0; Index < UpdateCommissionDetails.SocialMediaRecords.Count(); Index++)
                    {
                        //int SocialMediaRecordID = UpdateCommissionDetails.SocialMediaRecords.ElementAt(Index).PK_UserSocialMediaID;

                        //fill  social media  list start


                        //var oUserSocialMedias = (from UserSocialMedia in dbContext.Details_UserSocialMedia
                        //where UserSocialMedia.FK_UserID == UserID
                        //select UserSocialMedia).ToList();

                        //var result = oUserSocialMedias.FindAll(x => x.PK_UserSocialMediaID == SocialMediaRecordID).FirstOrDefault();
                        //if (result == null) // Insert 
                        //{

                        Details_UserSocialMedia oUserSocialMedia = new Details_UserSocialMedia
                        {
                            FK_UserID = UserID,
                            FK_SocialMediaID = UpdateCommissionDetails.SocialMediaRecords[Index].FK_SocialMediaID,
                            SocialMedia = UpdateCommissionDetails.SocialMediaRecords[Index].SocialMedia,
                            URL = UpdateCommissionDetails.SocialMediaRecords[Index].URL,
                            CreatedDate = DateTime.Today,
                            FK_CreatedBy = UserID

                        };
                        dbContext.Details_UserSocialMedia.Add(oUserSocialMedia);
                        //  dbContext.SaveChanges();

                        //}
                        //else // update, request to delete 
                        //{
                        //    var temp = dbContext.Details_UserSocialMedia
                        //        .FirstOrDefault(p => p.PK_UserSocialMediaID == SocialMediaRecordID);

                        //    temp.FK_SocialMediaID = UpdateCommissionDetails.SocialMediaRecords[Index].FK_SocialMediaID;
                        //    temp.SocialMedia = UpdateCommissionDetails.SocialMediaRecords[Index].SocialMedia;
                        //    temp.URL = UpdateCommissionDetails.SocialMediaRecords[Index].URL;
                        //    temp.DelFlag = false;
                        //    temp.ModifiedDate = DateTime.Today;
                        //    temp.FK_ModifiedBy = UserID;



                        //}



                    }

                    MasterProfile.FK_UserID = UpdateCommissionDetails.PK_UserID;
                    MasterProfile.CN_Title = UpdateCommissionDetails.Title;
                    MasterProfile.CN_Description = UpdateCommissionDetails.Description;
                    MasterProfile.CN_PriContactLegalFirstName = UpdateCommissionDetails.PriContactLegalFirstName;
                    MasterProfile.CN_PriContactLastName = UpdateCommissionDetails.PriContactLastName;
                    MasterProfile.CN_PriContactLegalMiddleName = UpdateCommissionDetails.PriContactLegalMiddleName;
                    MasterProfile.CN_PrimaryEmail = UpdateCommissionDetails.EmailAddress;
                    MasterProfile.CN_SecContactLegalFirstName = UpdateCommissionDetails.SecContactLegalFirstName;
                    MasterProfile.CN_SecContactLastName = UpdateCommissionDetails.SecContactLastName;
                    MasterProfile.CN_SecContactLegalMiddleName = UpdateCommissionDetails.SecContactLegalMiddleName;
                    MasterProfile.CN_SecondaryEmail = UpdateCommissionDetails.SecContactEmail;
                    MasterProfile.CN_PrimaryContact = UpdateCommissionDetails.PriContactEmail;
                    MasterProfile.CN_AuthorizingBody = UpdateCommissionDetails.AuthorizingBody;

                    MasterProfile.CN_DistrictOfAuthorization = UpdateCommissionDetails.DistrictOfAuthorization;
                    MasterProfile.CN_webaddress = UpdateCommissionDetails.Website;

                    //Map_User_Profile UserProfileMap = new Map_User_Profile
                    //{
                    //    FK_ProfileID = (byte)PROFILES.Commission,
                    //    FK_UserID = UpdateCommissionDetails.PK_UserID,
                    //    CreatedDate = DateTime.Today,
                    //    FK_CreatedBy = UpdateCommissionDetails.PK_UserID

                    //};

                    //dbContext.Map_User_Profile.Add(UserProfileMap);

                    //Map_User_Role UserRoleMap = new Map_User_Role
                    //{
                    //    FK_RoleID = (byte)ROLES.MemberCommission,
                    //    FK_UserID = UpdateCommissionDetails.PK_UserID,
                    //    CreatedDate = DateTime.Today,
                    //    FK_CreatedBy = UpdateCommissionDetails.PK_UserID

                    //};
                    //dbContext.Map_User_Role.Add(UserRoleMap);


                    //UpdateProfileAndRole(ref MasterUser, UpdateCommissionDetails.PK_UserID, (byte)ROLES.MemberCommission, (byte)PROFILES.Commission);

                    dbContext.SaveChanges();

                    transaction.Commit();



                    oResponse.status = "200";
                    oResponse.status = "Success";
                    oResponse.result = UpdateCommissionDetails.PK_UserID;

                    //  IHttpActionResult oResult = GetUserProfile(UserDemoGraphicInfo.PK_UserID);
                    // return oResult;
                }
                catch (Exception ex)
                {
                    oResponse.errors = "Service Error";
                    oResponse.status = "Exception";
                    oResponse.result = ex.Message;
                    transaction.Rollback();

                }

                return Ok(oResponse);
            }
        }

        [HttpPost]
        [Route("api/Profile/CreateSanctioningBody")]
        public IHttpActionResult CreateSanctioningBody(SanctioningBody CreateSanctioningBody)
        {
            ReturnResponse oResponse = new ReturnResponse();

            using (var transaction = dbContext.Database.BeginTransaction())
            {
                try
                {

                    Master_User oUser = new Master_User();

                    //Fill custom data
                    CreateSanctioningBody.CreatedDate = DateTime.Today;

                    //Adding user data  
                    oUser.CreatedDate = CreateSanctioningBody.CreatedDate;
                    oUser.EmailAddress = CreateSanctioningBody.EmailAddress;
                    oUser.Password = FRICrypto.EncryptString(CreateSanctioningBody.Password, "FRI@AOT");
                    oUser.SecurityQuestion = CreateSanctioningBody.SecurityQuestion;
                    oUser.SecurityAnswer = CreateSanctioningBody.SecurityAnswer;
                    oUser.CreatedDate = DateTime.Now;
                    oUser.LegalFirstName = CreateSanctioningBody.Title;
                    oUser.ProfilePhotoPath = CreateSanctioningBody.ProfilePhotoPath;

                    oUser.Phone = CreateSanctioningBody.Phone;
                    oUser.FK_Country = CreateSanctioningBody.FK_Country;
                    oUser.FK_ProvinceOrState = CreateSanctioningBody.FK_ProvinceOrState;
                    oUser.City = CreateSanctioningBody.City;
                    oUser.Street = CreateSanctioningBody.Street;
                    oUser.PostalOrZipCode = CreateSanctioningBody.PostalOrZipCode;
                    oUser.FK_Status = Constants.Pendingstaus;

                    dbContext.Master_User.Add(oUser);
                    dbContext.SaveChanges();
                    //Get userId
                    CreateSanctioningBody.PK_UserID = oUser.PK_UserID;
                    var CountryDetails = (from Country in dbContext.Lookup_Country
                                          select new { Country.Description, Country.PK_CountryID }).Where(W => W.PK_CountryID == CreateSanctioningBody.FK_Country).FirstOrDefault();

                    if (CountryDetails != null)
                        oUser.CountryName = CountryDetails.Description;


                    var StateDetails = (from State in dbContext.Lookup_StateOrProvince
                                        select new { State.Description, State.PK_StateOrProvinceID }).Where(W => W.PK_StateOrProvinceID == CreateSanctioningBody.FK_ProvinceOrState).FirstOrDefault();

                    if (StateDetails != null)
                        oUser.StateName = StateDetails.Description;

                    Master_Profile oProfile = new Master_Profile();
                    oProfile.FK_UserID = CreateSanctioningBody.PK_UserID;
                    oProfile.SB_Title = CreateSanctioningBody.Title;
                    oProfile.SB_Description = CreateSanctioningBody.Description;
                    oProfile.SB_WebAddress = CreateSanctioningBody.WebAddres;

                    oProfile.SB_SecondaryContact = CreateSanctioningBody.SecContactEmail;
                    oProfile.CN_PrimaryEmail = CreateSanctioningBody.PriContactEmail;
                    oProfile.SB_PriContactLegalFirstName = CreateSanctioningBody.PriContactLegalFirstName;
                    oProfile.SB_PriContactLastName = CreateSanctioningBody.PriContactLastName;
                    oProfile.SB_PriContactLegalMiddleName = CreateSanctioningBody.PriContactLegalMiddleName;

                    oProfile.SB_SecContactLegalFirstName = CreateSanctioningBody.SecContactLegalFirstName;
                    oProfile.SB_SecContactLastName = CreateSanctioningBody.SecContactLastName;
                    oProfile.SB_SecContactLegalMiddleName = CreateSanctioningBody.SecContactLegalMiddleName;
                    dbContext.Master_Profile.Add(oProfile);
                    dbContext.SaveChanges();


                    UpdateProfileAndRole(ref oUser, oUser.PK_UserID, (byte)ROLES.SanctioningBody, (byte)PROFILES.SanctioningBody);



                    int iCount = CreateSanctioningBody.SocialMediaRecords.Count();
                    for (int index = 0; index < iCount; index++)
                    {
                        // Adding custom data
                        CreateSanctioningBody.SocialMediaRecords[index].CreatedDate = DateTime.Today;
                        CreateSanctioningBody.SocialMediaRecords[index].FK_CreatedBy = CreateSanctioningBody.PK_UserID;
                        CreateSanctioningBody.SocialMediaRecords[index].FK_UserID = CreateSanctioningBody.PK_UserID;

                        // Adding social media
                        Details_UserSocialMedia oSocialMediaRecord = new Details_UserSocialMedia();
                        oSocialMediaRecord.CreatedDate = CreateSanctioningBody.SocialMediaRecords[index].CreatedDate;
                        oSocialMediaRecord.FK_SocialMediaID = CreateSanctioningBody.SocialMediaRecords[index].FK_SocialMediaID;
                        oSocialMediaRecord.SocialMedia = CreateSanctioningBody.SocialMediaRecords[index].SocialMedia;
                        oSocialMediaRecord.URL = CreateSanctioningBody.SocialMediaRecords[index].URL;
                        oSocialMediaRecord.FK_UserID = CreateSanctioningBody.PK_UserID;
                        oSocialMediaRecord.FK_CreatedBy = CreateSanctioningBody.PK_UserID;
                        dbContext.Details_UserSocialMedia.Add(oSocialMediaRecord);
                        dbContext.SaveChanges();

                        // Get SocialMediaID
                        CreateSanctioningBody.SocialMediaRecords[index].PK_UserSocialMediaID = oSocialMediaRecord.PK_UserSocialMediaID;

                    }

                    dbContext.SaveChanges();

                    transaction.Commit();
                    oResponse.status = "200";
                    oResponse.status = "Success";
                    oResponse.result = CreateSanctioningBody.PK_UserID;
                    omail.SelectMailTemplate(oUser.PK_UserID, Constants.santionbodyapproval, Constants.newsantionbody, oUser.LegalFirstName);
                    //  IHttpActionResult oResult = GetUserProfile(UserDemoGraphicInfo.PK_UserID);
                    // return oResult;
                }
                catch (Exception ex)
                {
                    oResponse.errors = "Service Error";
                    oResponse.status = "Exception";
                    oResponse.result = ex.Message;
                    transaction.Rollback();

                }

            }


            return Ok(oResponse);
        }

        [HttpPost]
        [Route("api/Profile/UpdateSanctioningBody")]
        public IHttpActionResult UpdateSanctioningBody(SanctioningBody UpdateSanctioningBody)
        {
            ReturnResponse oResponse = new ReturnResponse();

            using (var transaction = dbContext.Database.BeginTransaction())
            {
                try
                {

                    if (UpdateSanctioningBody == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data available";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    Master_User MasterUser = (from user in dbContext.Master_User
                                              where user.PK_UserID == UpdateSanctioningBody.PK_UserID && user.DelFlag == false
                                              select user).FirstOrDefault();
                    if (MasterUser == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data available.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    Master_Profile MasterProfile = (from userprofile in MasterUser.Master_Profile
                                                    where userprofile.FK_UserID == UpdateSanctioningBody.PK_UserID
                                                    select userprofile).FirstOrDefault();
                    if (MasterProfile == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data available";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }
                    //Update date              
                    MasterUser.EmailAddress = UpdateSanctioningBody.EmailAddress;
                    MasterUser.SecurityQuestion = UpdateSanctioningBody.SecurityQuestion;
                    MasterUser.SecurityAnswer = UpdateSanctioningBody.SecurityAnswer;
                    MasterUser.LegalFirstName = UpdateSanctioningBody.Title;
                    MasterUser.ProfilePhotoPath = UpdateSanctioningBody.ProfilePhotoPath;

                    MasterUser.Phone = UpdateSanctioningBody.Phone;
                    MasterUser.FK_Country = UpdateSanctioningBody.FK_Country;
                    MasterUser.FK_ProvinceOrState = UpdateSanctioningBody.FK_ProvinceOrState;
                    MasterUser.City = UpdateSanctioningBody.City;
                    MasterUser.Street = UpdateSanctioningBody.Street;
                    MasterUser.PostalOrZipCode = UpdateSanctioningBody.PostalOrZipCode;

                    int UserID = MasterUser.PK_UserID;

                    var CountryDetails = (from Country in dbContext.Lookup_Country
                                          select new { Country.Description, Country.PK_CountryID }).Where(W => W.PK_CountryID == UpdateSanctioningBody.FK_Country).FirstOrDefault();

                    if (CountryDetails != null)
                        MasterUser.CountryName = CountryDetails.Description;


                    var StateDetails = (from State in dbContext.Lookup_StateOrProvince
                                        select new { State.Description, State.PK_StateOrProvinceID }).Where(W => W.PK_StateOrProvinceID == UpdateSanctioningBody.FK_ProvinceOrState).FirstOrDefault();

                    if (StateDetails != null)
                        MasterUser.StateName = StateDetails.Description;
                    if (UpdateSanctioningBody.SocialMediaRecords.Count > 0)
                    {
                        dbContext.Details_UserSocialMedia
                                     .Where(p => p.FK_UserID == UserID)
                                     .ToList().ForEach(r => r.DelFlag = true);
                    }
                    // Fill social media
                    for (int Index = 0; Index < UpdateSanctioningBody.SocialMediaRecords.Count(); Index++)
                    {
                        int SocialMediaRecordID = UpdateSanctioningBody.SocialMediaRecords.ElementAt(Index).PK_UserSocialMediaID;

                        //fill  social media  list start


                        //var oUserSocialMedias = (from UserSocialMedia in dbContext.Details_UserSocialMedia
                        //                         where UserSocialMedia.FK_UserID == UserID
                        //                         select UserSocialMedia).ToList();

                        //var result = oUserSocialMedias.FindAll(x => x.PK_UserSocialMediaID == SocialMediaRecordID).FirstOrDefault();
                        //if (result == null) // Insert 
                        //{

                        Details_UserSocialMedia oUserSocialMedia = new Details_UserSocialMedia
                        {
                            FK_UserID = UserID,
                            FK_SocialMediaID = UpdateSanctioningBody.SocialMediaRecords[Index].FK_SocialMediaID,
                            SocialMedia = UpdateSanctioningBody.SocialMediaRecords[Index].SocialMedia,
                            URL = UpdateSanctioningBody.SocialMediaRecords[Index].URL,
                            CreatedDate = DateTime.Today,
                            FK_CreatedBy = UserID,


                        };
                        dbContext.Details_UserSocialMedia.Add(oUserSocialMedia);
                        //  dbContext.SaveChanges();

                        //}
                        //else // update, request to delete 
                        //{
                        //    var temp = dbContext.Details_UserSocialMedia
                        //           .FirstOrDefault(p => p.PK_UserSocialMediaID == SocialMediaRecordID);

                        //    temp.FK_SocialMediaID = UpdateSanctioningBody.SocialMediaRecords[Index].FK_SocialMediaID;
                        //    temp.SocialMedia = UpdateSanctioningBody.SocialMediaRecords[Index].SocialMedia;
                        //    temp.URL = UpdateSanctioningBody.SocialMediaRecords[Index].URL;
                        //    temp.DelFlag = false;
                        //    temp.ModifiedDate = DateTime.Today;
                        //    temp.FK_ModifiedBy = UserID;
                        //}
                    }

                    MasterProfile.FK_UserID = UpdateSanctioningBody.PK_UserID;
                    MasterProfile.SB_Title = UpdateSanctioningBody.Title;
                    MasterProfile.SB_WebAddress = UpdateSanctioningBody.Website;


                    MasterProfile.SB_SecondaryContact = UpdateSanctioningBody.SecContactEmail;
                    MasterProfile.CN_PrimaryEmail = UpdateSanctioningBody.PriContactEmail;
                    MasterProfile.SB_PriContactLegalFirstName = UpdateSanctioningBody.PriContactLegalFirstName;
                    MasterProfile.SB_PriContactLastName = UpdateSanctioningBody.PriContactLastName;
                    MasterProfile.SB_PriContactLegalMiddleName = UpdateSanctioningBody.PriContactLegalMiddleName;

                    MasterProfile.SB_SecContactLegalFirstName = UpdateSanctioningBody.SecContactLegalFirstName;
                    MasterProfile.SB_SecContactLastName = UpdateSanctioningBody.SecContactLastName;
                    MasterProfile.SB_SecContactLegalMiddleName = UpdateSanctioningBody.SecContactLegalMiddleName;


                    // UpdateProfileAndRole(ref MasterUser, UpdateSanctioningBody.PK_UserID, (byte)ROLES.SanctioningBody, (byte)PROFILES.SanctioningBody);

                    //Context.Master_Profile.Add(MasterProfile);
                    dbContext.SaveChanges();


                    //Map_User_Profile UserProfileMap = new Map_User_Profile
                    //{
                    //    FK_ProfileID = (byte)PROFILES.Commission,
                    //    FK_UserID = UpdateSanctioningBody.PK_UserID,
                    //    CreatedDate = DateTime.Today,
                    //    FK_CreatedBy = UpdateSanctioningBody.PK_UserID

                    //};
                    //dbContext.Map_User_Profile.Add(UserProfileMap);

                    //Map_User_Role UserRoleMap = new Map_User_Role
                    //{
                    //    FK_RoleID = (byte)ROLES.MemberCommission,
                    //    FK_UserID = UpdateSanctioningBody.PK_UserID,
                    //    CreatedDate = DateTime.Today,
                    //    FK_CreatedBy = UpdateSanctioningBody.PK_UserID

                    //};
                    //bContext.Map_User_Role.Add(UserRoleMap);


                    dbContext.SaveChanges();

                    transaction.Commit();
                    oResponse.status = "200";
                    oResponse.status = "Success";
                    oResponse.result = UpdateSanctioningBody.PK_UserID;

                    //  IHttpActionResult oResult = GetUserProfile(UserDemoGraphicInfo.PK_UserID);
                    // return oResult;
                }
                catch (Exception ex)
                {
                    oResponse.errors = "Service Error";
                    oResponse.status = "Exception";
                    oResponse.result = ex.Message;
                    transaction.Rollback();

                }
                return Ok(oResponse);
            }
        }

        [HttpPost]
        [Route("api/Profile/UpdateProfile")]
        public IHttpActionResult UpdateProfile(UserDetails oUserDetails)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {



                if (oUserDetails == null)
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                    return Ok(oResponse);
                }

                Master_User MasterUser = (from user in dbContext.Master_User
                                          where user.PK_UserID == oUserDetails.PK_UserID && user.DelFlag == false
                                          select user).FirstOrDefault();

                if (MasterUser == null)
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                    return Ok(oResponse);
                }

                Master_Profile MasterProfile = (from userprofile in MasterUser.Master_Profile
                                                where userprofile.PK_UserProfileID == oUserDetails.PK_UserProfileID
                                                select userprofile).FirstOrDefault();
                //if(MasterProfile!=null&&MasterProfile.FR_PrimaryDiscipline==null&& oUserDetails.FighterProfile != null)//check fighter is new,then send mail
                //{
                //    omail.SelectMailTemplate(oUserDetails.PK_UserID,Constants.fightertemplate,Constants.newfighter);
                //}
                if (MasterUser == null)
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                    return Ok(oResponse);
                }

                using (var transaction = dbContext.Database.BeginTransaction())
                {
                    try
                    {
                        //// fill profile list start


                        var oUserProfileMaps = (from Profilelist in MasterUser.Map_User_Profile
                                                where Profilelist.FK_UserID == oUserDetails.PK_UserID
                                                select new
                                                {
                                                    DelFlag = Profilelist.DelFlag,
                                                    FK_ProfileID = Profilelist.FK_ProfileID,
                                                    PK_ProfileMapID = Profilelist.PK_ProfileMapID
                                                }).ToList();

                        MasterUser.Map_User_Profile
                                .Where(p => p.FK_UserID == oUserDetails.PK_UserID)
                                .ToList().ForEach(r => r.DelFlag = true);

                        UserProfile TempUserProfile = new UserProfile();
                        TempUserProfile.FK_ProfileID = (byte)PROFILES.Fan;
                        oUserDetails.UserProfiles.Add(TempUserProfile);


                        if (oUserDetails.DemoGraphicInfo != null)
                        {
                            if (!UpdateDemoGraphicInfo(ref MasterUser, oUserDetails.DemoGraphicInfo))
                                throw new Exception("Error while updating DemoGraphic Details");
                        }

                        for (int Index = 0; Index < oUserDetails.UserProfiles.Count; Index++)
                        {

                            var result = oUserProfileMaps.FindAll(x => x.FK_ProfileID == oUserDetails.UserProfiles.ElementAt(Index).FK_ProfileID).FirstOrDefault();
                            byte ProfileID = oUserDetails.UserProfiles.ElementAt(Index).FK_ProfileID;

                            if (result == null) // Insert 
                            {

                                Map_User_Profile UserProfileMap = new Map_User_Profile
                                {
                                    FK_ProfileID = ProfileID,
                                    FK_UserID = oUserDetails.PK_UserID,
                                    CreatedDate = DateTime.Today,
                                    FK_CreatedBy = oUserDetails.PK_UserID

                                };
                                MasterUser.Map_User_Profile.Add(UserProfileMap);


                            }
                            else  // update, request to delete 
                            {

                                MasterUser.Map_User_Profile
                                    .Where(p => p.FK_ProfileID == ProfileID)
                                    .FirstOrDefault().DelFlag = false;
                            }


                            if (oUserDetails.FighterProfile != null && ProfileID == (byte)PROFILES.Fighter) // Fighter
                            {
                                if (!UpdateFighterProfile(ref MasterUser, ref MasterProfile, oUserDetails.FighterProfile))
                                    throw new Exception("Error while updating fighter profile");
                            }

                            if (oUserDetails.OfficialProfile != null && ProfileID == (byte)PROFILES.Official) // Official
                            {
                                if (!UpdateOfficialProfile(ref MasterProfile, oUserDetails.OfficialProfile))
                                    throw new Exception("Error while updating official profile");
                            }
                            if (oUserDetails.PromotorProfile != null && ProfileID == (byte)PROFILES.Promoter) // Promoter
                            {
                                if (!UpdatePromoterProfile(ref MasterProfile, oUserDetails.PromotorProfile))
                                    throw new Exception("Error while updating Promotor profile");
                            }
                        }



                        bool GymOwner = false;
                        if (oUserDetails.FighterProfile != null)
                            GymOwner = oUserDetails.FighterProfile.FR_IsOwner;
                        UpdateUserRoles(ref MasterUser, oUserDetails.DemoGraphicInfo.IsAmbassador, GymOwner);
                        dbContext.SaveChanges();


                    }

                    catch (Exception ex)
                    {
                        oResponse.errors = "Service Error";
                        oResponse.status = "Exception";
                        oResponse.result = ex.Message;
                        transaction.Rollback();
                        return Ok(oResponse);

                    }


                    transaction.Commit();
                }



                oResponse.status = "Success";
                oResponse.result = oUserDetails.PK_UserID;
                //  IHttpActionResult oResult = GetUserProfile(oUserDetails.PK_UserID);               
                // return oResult;
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;


            }

            return Ok(oResponse);
        }

        private bool UpdatePromoterProfile(ref Master_Profile oMasterProfile, PromotorProfile promotorProfile)
        {
            try
            {
                int ProfileID = oMasterProfile.PK_UserProfileID;
                int UserID = oMasterProfile.FK_UserID;

                oMasterProfile.PR_Address = promotorProfile.PR_Address;
                oMasterProfile.PR_Description = promotorProfile.PR_Description;
                oMasterProfile.PR_EventNameOrSeries = promotorProfile.PR_EventNameOrSeries;
                oMasterProfile.PR_Phone = promotorProfile.PR_Phone;
                oMasterProfile.PR_PriContactLastName = promotorProfile.PR_PriContactLastName;
                oMasterProfile.PR_PriContactLegalFirstName = promotorProfile.PR_PriContactLegalFirstName;
                oMasterProfile.PR_PriContactLegalMiddleName = promotorProfile.PR_PriContactLegalMiddleName;
                oMasterProfile.PR_SecContactLastName = promotorProfile.PR_SecContactLastName;
                oMasterProfile.PR_SecContactLegalFirstName = promotorProfile.PR_SecContactLegalFirstName;
                oMasterProfile.PR_SecContactLegalMiddleName = promotorProfile.PR_SecContactLegalMiddleName;
                oMasterProfile.PR_Title = promotorProfile.PR_Title;
                oMasterProfile.PR_Webaddress = promotorProfile.PR_Webaddress;

                return true;
            }

            catch (Exception ex)
            {

                return false;


            }
        }

        private bool UpdateOfficialProfile(ref Master_Profile oMasterProfile, OfficialProfile OfficialProfiles)
        {
            try
            {
                int ProfileID = oMasterProfile.PK_UserProfileID;
                int UserID = oMasterProfile.FK_UserID;

                //// fill video list start
                oMasterProfile.Details_Official
                          .Where(p => p.FK_ProfileID == ProfileID)
                          .ToList().ForEach(r => r.DelFlag = true);

                var Officials = (from OfficialProfile in oMasterProfile.Details_Official
                                 where OfficialProfile.FK_ProfileID == ProfileID
                                 select new
                                 {
                                     PK_OfficialProfileID = OfficialProfile.PK_OfficialProfileID,
                                     FK_ProfileID = OfficialProfile.FK_ProfileID,
                                     CertID = OfficialProfile.CertID,
                                     Citizenship = OfficialProfile.Citizenship,
                                     Description = OfficialProfile.Description,
                                     DelFlag = OfficialProfile.DelFlag,
                                     Discipline = OfficialProfile.Discipline,
                                     EmergencyContactName = OfficialProfile.EmergencyContactName,
                                     Licence = OfficialProfile.Licence,
                                     OfficialLevel = OfficialProfile.OfficialLevel,
                                     TypeOfOfficial = OfficialProfile.TypeOfOfficial,
                                     NumberofFights = OfficialProfile.NumberofFights,
                                     LicensedBy = OfficialProfile.LicensedBy

                                 }).ToList();



                for (int Index = 0; Index < OfficialProfiles.OfficialDetails.Count(); Index++)
                {
                    int OfficialDetailID = OfficialProfiles.OfficialDetails.ElementAt(Index).PK_OfficialProfileID;

                    var result = Officials.FindAll(x => x.PK_OfficialProfileID == OfficialDetailID).FirstOrDefault();
                    if (result == null) // Insert 
                    {

                        Details_Official oDetailsOfficial = new Details_Official
                        {

                            FK_ProfileID = OfficialProfiles.OfficialDetails.ElementAt(Index).FK_ProfileID,
                            CertID = OfficialProfiles.OfficialDetails.ElementAt(Index).CertID,
                            Citizenship = OfficialProfiles.OfficialDetails.ElementAt(Index).Citizenship,
                            Description = OfficialProfiles.OfficialDetails.ElementAt(Index).Description,
                            DelFlag = OfficialProfiles.OfficialDetails.ElementAt(Index).DelFlag,
                            Discipline = OfficialProfiles.OfficialDetails.ElementAt(Index).Discipline,
                            EmergencyContactName = OfficialProfiles.OfficialDetails.ElementAt(Index).EmergencyContactName,
                            Licence = OfficialProfiles.OfficialDetails.ElementAt(Index).Licence,
                            OfficialLevel = OfficialProfiles.OfficialDetails.ElementAt(Index).OfficialLevel,
                            TypeOfOfficial = OfficialProfiles.OfficialDetails.ElementAt(Index).TypeOfOfficial,
                            NumberofFights = OfficialProfiles.OfficialDetails.ElementAt(Index).NumberofFights,
                            LicensedBy = OfficialProfiles.OfficialDetails.ElementAt(Index).LicensedBy,
                            CreatedDate = DateTime.Today,
                            FK_CreatedBy = oMasterProfile.FK_UserID

                        };
                        oMasterProfile.Details_Official.Add(oDetailsOfficial);
                        //  dbContext.SaveChanges();

                    }
                    else // update, request to delete 
                    {
                        oMasterProfile.Details_Official
                            .Where(p => p.PK_OfficialProfileID == OfficialDetailID)
                            .ToList().ForEach((Official) =>
                            {

                                Official.FK_ProfileID = OfficialProfiles.OfficialDetails.ElementAt(Index).FK_ProfileID;
                                Official.CertID = OfficialProfiles.OfficialDetails.ElementAt(Index).CertID;
                                Official.Citizenship = OfficialProfiles.OfficialDetails.ElementAt(Index).Citizenship;
                                Official.Description = OfficialProfiles.OfficialDetails.ElementAt(Index).Description;
                                Official.DelFlag = false;
                                Official.Discipline = OfficialProfiles.OfficialDetails.ElementAt(Index).Discipline;
                                Official.EmergencyContactName = OfficialProfiles.OfficialDetails.ElementAt(Index).EmergencyContactName;
                                Official.Licence = OfficialProfiles.OfficialDetails.ElementAt(Index).Licence;
                                Official.OfficialLevel = OfficialProfiles.OfficialDetails.ElementAt(Index).OfficialLevel;
                                Official.TypeOfOfficial = OfficialProfiles.OfficialDetails.ElementAt(Index).TypeOfOfficial;
                                Official.NumberofFights = OfficialProfiles.OfficialDetails.ElementAt(Index).NumberofFights;
                                Official.LicensedBy = OfficialProfiles.OfficialDetails.ElementAt(Index).LicensedBy;
                                Official.ModifiedDate = DateTime.Today;
                                Official.FK_ModifiedBy = UserID;
                            });


                    }



                }

                return true;

            }

            catch (Exception ex)
            {

                return false;


            }
        }

        private bool UpdateUserRoles(ref Master_User MasterUser, bool IsAmbassador, bool GymOwner)
        {
            try
            {
                int UserID = MasterUser.PK_UserID;

                var oUserProfileMaps = (from Profilelist in MasterUser.Map_User_Profile
                                        where Profilelist.FK_UserID == UserID
                                        select new
                                        {
                                            FK_ProfileID = Profilelist.FK_ProfileID,
                                            PK_ProfileMapID = Profilelist.PK_ProfileMapID,
                                            DelFlag = Profilelist.DelFlag
                                        }).ToList();


                var oUserRoleMaps = (from Rolelist in MasterUser.Map_User_Role
                                     where Rolelist.FK_UserID == UserID
                                     select new
                                     {
                                         FK_RoleID = Rolelist.FK_RoleID,
                                         PK_ProfileMapID = Rolelist.PK_RoleMapID,
                                         DelFlag = Rolelist.DelFlag
                                     }).ToList();


                //  List<Lookup_Profile> Profiles = dbContext.Lookup_Profile.ToList();

                byte RoleID = 0;
                for (int Index = 0; Index < oUserProfileMaps.Count(); Index++)
                {
                    byte ProfileID = oUserProfileMaps.ElementAt(Index).FK_ProfileID;
                    bool DelFlag = oUserProfileMaps.ElementAt(Index).DelFlag;

                    // var result = oUserRoleMaps.FindAll(x => x. == ProfileID).FirstOrDefault();


                    switch (ProfileID)
                    {
                        case (byte)PROFILES.Commission:
                            RoleID = (byte)ROLES.MemberCommission;
                            break;

                        case (int)PROFILES.Fan:
                            RoleID = (byte)ROLES.Fan;
                            break;

                        case (int)PROFILES.Fighter:
                            RoleID = (byte)ROLES.Fighter;
                            break;

                        case (int)PROFILES.GymOwner:
                            RoleID = (byte)ROLES.GymOwner;
                            break;

                        case (int)PROFILES.Official:
                            RoleID = (byte)ROLES.Official;
                            break;

                        case (int)PROFILES.Promoter:
                            RoleID = (byte)ROLES.Promoter;
                            break;

                        case (int)PROFILES.SanctioningBody:
                            RoleID = (byte)ROLES.SanctioningBody;
                            break;

                        case (int)PROFILES.Sponsor:
                            RoleID = (byte)ROLES.Sponsor;
                            break;
                        default:
                            RoleID = 0;
                            break;

                    }

                    if (RoleID > 0)
                    {


                        var UserRoleMap = oUserRoleMaps.FindAll(x => x.FK_RoleID == RoleID).FirstOrDefault();
                        if (UserRoleMap == null)
                        {
                            Map_User_Role UserRole = new Map_User_Role
                            {
                                FK_RoleID = (byte)RoleID,
                                FK_UserID = UserID,
                                CreatedDate = DateTime.Today,
                                FK_CreatedBy = UserID,
                                DelFlag = DelFlag

                            };
                            MasterUser.Map_User_Role.Add(UserRole);
                        }
                        else
                        {
                            MasterUser.Map_User_Role
                            .Where(p => p.FK_UserID == UserID && p.FK_RoleID == RoleID)
                            .FirstOrDefault().DelFlag = DelFlag;
                        }


                    }

                }

                RoleID = (byte)ROLES.Ambassador;


                var UserRoleMapData = oUserRoleMaps.FindAll(x => x.FK_RoleID == RoleID).FirstOrDefault();
                if (UserRoleMapData == null && IsAmbassador)
                {

                    Map_User_Role UserRole = new Map_User_Role
                    {
                        FK_RoleID = (byte)RoleID,
                        FK_UserID = UserID,
                        CreatedDate = DateTime.Today,
                        FK_CreatedBy = UserID

                    };
                    MasterUser.Map_User_Role.Add(UserRole);
                }
                else
                {
                    var data = MasterUser.Map_User_Role
                        .Where(p => p.FK_UserID == UserID && p.FK_RoleID == RoleID)
                         .FirstOrDefault();
                    if (data != null)
                        data.DelFlag = !IsAmbassador;
                }


                RoleID = (byte)ROLES.GymOwner;
                UserRoleMapData = oUserRoleMaps.FindAll(x => x.FK_RoleID == RoleID).FirstOrDefault();
                if (UserRoleMapData == null && GymOwner)
                {

                    Map_User_Role UserRole = new Map_User_Role
                    {
                        FK_RoleID = (byte)RoleID,
                        FK_UserID = MasterUser.PK_UserID,
                        CreatedDate = DateTime.Today,
                        FK_CreatedBy = MasterUser.PK_UserID

                    };
                    dbContext.Map_User_Role.Add(UserRole);
                }
                else
                {
                    //MasterUser.Map_User_Role
                    //.Where(p => p.FK_UserID == UserID && p.FK_RoleID == RoleID)
                    // .FirstOrDefault().DelFlag = !GymOwner;
                    var data = MasterUser.Map_User_Role
                      .Where(p => p.FK_UserID == UserID && p.FK_RoleID == RoleID)
                       .FirstOrDefault();
                    if (data != null)
                        data.DelFlag = !GymOwner;
                }



                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool UpdateDemoGraphicInfo(ref Master_User MasterUser, DemoGraphicInfo demoGraphicInfo)
        {
            try
            {

                int UserID = MasterUser.PK_UserID;

                MasterUser.City = demoGraphicInfo.City;
                //MasterUser.DOB = demoGraphicInfo.DOB;
                MasterUser.EmailAddress = demoGraphicInfo.EmailAddress;
                MasterUser.FK_Country = demoGraphicInfo.FK_Country;
                MasterUser.FK_ProvinceOrState = demoGraphicInfo.FK_ProvinceOrState;
                MasterUser.Gender = demoGraphicInfo.Gender;
                MasterUser.IsAmbassador = demoGraphicInfo.IsAmbassador;
                MasterUser.LastName = demoGraphicInfo.LastName;
                MasterUser.LegalFirstName = demoGraphicInfo.LegalFirstName;
                MasterUser.LegalMiddleName = demoGraphicInfo.LegalMiddleName;
                // MasterUser.Password = FRICrypto.EncryptString(demoGraphicInfo.Password, "FRI@AOT"); 
                MasterUser.PersonalWebsite = demoGraphicInfo.PersonalWebsite;
                MasterUser.Phone = demoGraphicInfo.Phone;
                MasterUser.ProfilePhotoPath = demoGraphicInfo.ProfilePhotoPath;
                // MasterUser.Base64ProfilePhoto = demoGraphicInfo.Base64Image;
                MasterUser.Street = demoGraphicInfo.Street;
                MasterUser.SecurityQuestion = demoGraphicInfo.SecurityQuestion;
                MasterUser.SecurityAnswer = demoGraphicInfo.SecurityAnswer;
                MasterUser.EmergencyContactName = demoGraphicInfo.EmergencyContactName;
                MasterUser.EmergencyContactNumber = demoGraphicInfo.EmergencyContactNumber;
                MasterUser.PostalOrZipCode = demoGraphicInfo.PostalOrZipCode;


                var CountryDetails = (from Country in dbContext.Lookup_Country
                                      select new { Country.Description, Country.PK_CountryID }).Where(W => W.PK_CountryID == demoGraphicInfo.FK_Country).FirstOrDefault();

                if (CountryDetails != null)
                    MasterUser.CountryName = CountryDetails.Description;


                var StateDetails = (from State in dbContext.Lookup_StateOrProvince
                                    select new { State.Description, State.PK_StateOrProvinceID }).Where(W => W.PK_StateOrProvinceID == demoGraphicInfo.FK_ProvinceOrState).FirstOrDefault();

                if (StateDetails != null)
                    MasterUser.StateName = StateDetails.Description;

                //fill  social media  list start
                dbContext.Details_UserSocialMedia
                          .Where(p => p.FK_UserID == UserID)
                          .ToList().ForEach(r => r.DelFlag = true);

                var oUserSocialMedias = (from UserSocialMedia in dbContext.Details_UserSocialMedia
                                         where UserSocialMedia.FK_UserID == UserID
                                         select UserSocialMedia).ToList();



                for (int Index = 0; Index < demoGraphicInfo.SocialMediaRecords.Count(); Index++)
                {
                    int SocialMediaRecordID = demoGraphicInfo.SocialMediaRecords.ElementAt(Index).PK_UserSocialMediaID;

                    var result = oUserSocialMedias.FindAll(x => x.PK_UserSocialMediaID == SocialMediaRecordID).FirstOrDefault();
                    if (result == null) // Insert 
                    {

                        Details_UserSocialMedia oUserSocialMedia = new Details_UserSocialMedia
                        {
                            FK_UserID = UserID,
                            FK_SocialMediaID = demoGraphicInfo.SocialMediaRecords[Index].FK_SocialMediaID,
                            SocialMedia = demoGraphicInfo.SocialMediaRecords[Index].SocialMedia,
                            URL = demoGraphicInfo.SocialMediaRecords[Index].URL,
                            CreatedDate = DateTime.Today,
                            FK_CreatedBy = UserID

                        };
                        dbContext.Details_UserSocialMedia.Add(oUserSocialMedia);
                        //  dbContext.SaveChanges();

                    }
                    else // update, request to delete 
                    {
                        dbContext.Details_UserSocialMedia
                            .Where(p => p.PK_UserSocialMediaID == SocialMediaRecordID)
                            .ToList().ForEach((SocialMedia) =>
                            {
                                SocialMedia.FK_SocialMediaID = demoGraphicInfo.SocialMediaRecords[Index].FK_SocialMediaID;
                                SocialMedia.SocialMedia = demoGraphicInfo.SocialMediaRecords[Index].SocialMedia;
                                SocialMedia.URL = demoGraphicInfo.SocialMediaRecords[Index].URL;
                                SocialMedia.DelFlag = false;
                                SocialMedia.ModifiedDate = DateTime.Today;
                                SocialMedia.FK_ModifiedBy = UserID;
                            });
                    }



                }

                //// fill social media End



                return true;

            }

            catch (Exception ex)
            {

                return false;


            }
        }

        private bool UpdateFighterProfile(ref Master_User MasterUser, ref Master_Profile oMasterProfile, FighterProfile oFighterProfile)
        {

            try
            {
                int ProfileID = oMasterProfile.PK_UserProfileID;
                int UserID = MasterUser.PK_UserID;

                MasterUser.CheerCount = oFighterProfile.CheerCount;
                MasterUser.Draws = oFighterProfile.Draws;
                MasterUser.FighterName = oFighterProfile.FighterName;
                //  MasterUser.IsFeaturedfighter = oFighterProfile.IsFeaturedfighter;

                MasterUser.KO = oFighterProfile.KO;
                MasterUser.Losses = oFighterProfile.Losses;
                MasterUser.TKO = oFighterProfile.TKO;
                MasterUser.TotalFights = oFighterProfile.TotalFights;
                MasterUser.Wins = oFighterProfile.Wins;
                oMasterProfile.FR_Bio = oFighterProfile.FR_Bio;
                oMasterProfile.FR_Citizenship = oFighterProfile.FR_Citizenship;
                oMasterProfile.FR_CurrentWeight = oFighterProfile.FR_CurrentWeight;
                oMasterProfile.FR_CurrentWeightUnit = oFighterProfile.FR_CurrentWeightUnit;
                if (oFighterProfile.FR_CurrentWeightUnit == "Kgs")
                {
                    oMasterProfile.FR_CurrentWeightInKgs = oFighterProfile.FR_CurrentWeight;
                    oMasterProfile.FR_CurrentWeightinLbs = (Convert.ToDouble((oFighterProfile.FR_CurrentWeight)) * (2.2)).ToString("0.00");
                }
                else
                {
                    oMasterProfile.FR_CurrentWeightinLbs = oFighterProfile.FR_CurrentWeight;
                    oMasterProfile.FR_CurrentWeightInKgs = (Convert.ToDouble((oFighterProfile.FR_CurrentWeight)) / (2.2)).ToString("0.00");
                }
                oMasterProfile.FR_EmergencyContactName = oFighterProfile.FR_EmergencyContactName;
                oMasterProfile.FR_EmergencyContactPhone = oFighterProfile.FR_EmergencyContactPhone;
                oMasterProfile.FR_EyeColor = oFighterProfile.FR_EyeColor;
                oMasterProfile.FR_FighterStatus = oFighterProfile.FR_FighterStatus;
                oMasterProfile.FR_HairColor = oFighterProfile.FR_HairColor;
                oMasterProfile.FR_Height = oFighterProfile.FR_Height;
                oMasterProfile.FR_HeightUnit = oFighterProfile.FR_HeightUnit;
                MasterUser.DOB = oFighterProfile.FR_DOB;
                oMasterProfile.FR_HeightInFt = oFighterProfile.FR_HeightInFt;
                oMasterProfile.FR_HeightInInch = oFighterProfile.FR_HeightInInch;
                oMasterProfile.FR_HeightCM = oFighterProfile.FR_HeightCM;

                oMasterProfile.FR_IsAvailableMatch = oFighterProfile.FR_IsAvailableMatch;
                oMasterProfile.FR_IsAvailableSparing = oFighterProfile.FR_IsAvailableSparing;
                oMasterProfile.FR_LegReach = oFighterProfile.FR_LegReach;
                oMasterProfile.FR_LegReachUnit = oFighterProfile.FR_LegReachUnit;
                oMasterProfile.FR_IsOwner = oFighterProfile.FR_IsOwner;
                //     if (oFighterProfile.FR_IsOwner)
                {
                    oMasterProfile.GO_City = oFighterProfile.GO_City;
                    oMasterProfile.GO_Facebook = oFighterProfile.GO_Facebook;
                    oMasterProfile.GO_FK_Country = oFighterProfile.GO_FK_Country;
                    oMasterProfile.GO_FK_ProvinceOrState = oFighterProfile.GO_FK_ProvinceOrState;
                    oMasterProfile.GO_Phone = oFighterProfile.GO_Phone;
                    oMasterProfile.GO_PostalOrZipCode = oFighterProfile.GO_PostalOrZipCode;
                    oMasterProfile.GO_Street = oFighterProfile.GO_Street;
                    oMasterProfile.GO_Website = oFighterProfile.GO_Website;
                    oMasterProfile.GO_TrainerName = oFighterProfile.GO_TrainerName;
                    oMasterProfile.GO_TrainingFacilityName = oFighterProfile.GO_TrainingFacilityName;

                }

                oMasterProfile.FR_IsProfessional = oFighterProfile.FR_IsProfessional;
                oMasterProfile.FR_MaxWeight = oFighterProfile.FR_MaxWeight;
                oMasterProfile.FR_MaxWeightUnit = oFighterProfile.FR_MaxWeightUnit;
                oMasterProfile.FR_MinWeight = oFighterProfile.FR_MinWeight;
                oMasterProfile.FR_MinWeightUnit = oFighterProfile.FR_MinWeightUnit;
                oMasterProfile.FR_PrimaryDiscipline = oFighterProfile.FR_PrimaryDiscipline;
                oMasterProfile.FR_PrimarySkillLevel = oFighterProfile.FR_PrimarySkillLevel;
                oMasterProfile.FR_Reach = oFighterProfile.FR_Reach;
                oMasterProfile.FR_ReachUnit = oFighterProfile.FR_ReachUnit;
                oMasterProfile.FR_PrimaryDiscipline = oFighterProfile.FR_PrimaryDiscipline;
                oMasterProfile.FR_SecondaryDiscipline = oFighterProfile.FR_SecondaryDiscipline;
                oMasterProfile.FR_PrimarySkillLevel = oFighterProfile.FR_PrimarySkillLevel;
                oMasterProfile.FR_SecondarySkillLevel = oFighterProfile.FR_SecondarySkillLevel;
                oMasterProfile.FR_TertiaryDiscipline = oFighterProfile.FR_TertiaryDiscipline;
                oMasterProfile.FR_TertiarySkillLevel = oFighterProfile.FR_TertiarySkillLevel;
                //// fill video list start
                dbContext.Details_FighterVideo
                          .Where(p => p.FK_ProfileID == ProfileID)
                          .ToList().ForEach(r => r.DelFlag = true);

                var oFighterVideos = (from FighterVideo in dbContext.Details_FighterVideo
                                      where FighterVideo.FK_ProfileID == ProfileID
                                      select new
                                      {
                                          PK_FighterVideoID = FighterVideo.PK_FighterVideoID,
                                          VideoLink = FighterVideo.VideoLink,
                                          VideoDescription = FighterVideo.VideoDescription
                                      }).ToList();



                for (int Index = 0; Index < oFighterProfile.FighterVideoDetails.Count(); Index++)
                {
                    int FighterVideoID = oFighterProfile.FighterVideoDetails.ElementAt(Index).PK_FighterVideoID;

                    var result = oFighterVideos.FindAll(x => x.PK_FighterVideoID == FighterVideoID).FirstOrDefault();
                    if (result == null) // Insert 
                    {

                        Details_FighterVideo oDetailsFighterVideo = new Details_FighterVideo
                        {
                            FK_UserID = UserID,
                            FK_ProfileID = ProfileID,
                            VideoLink = oFighterProfile.FighterVideoDetails.ElementAt(Index).VideoLink,
                            VideoDescription = oFighterProfile.FighterVideoDetails.ElementAt(Index).VideoDescription,
                            CreatedDate = DateTime.Today,
                            FK_CreatedBy = UserID

                        };
                        dbContext.Details_FighterVideo.Add(oDetailsFighterVideo);
                        //  dbContext.SaveChanges();

                    }
                    else // update, request to delete 
                    {
                        dbContext.Details_FighterVideo
                            .Where(p => p.PK_FighterVideoID == FighterVideoID)
                            .ToList().ForEach((FighterVideo) =>
                            {
                                FighterVideo.DelFlag = false;
                                FighterVideo.VideoLink = oFighterProfile.FighterVideoDetails.ElementAt(Index).VideoLink;
                                FighterVideo.VideoDescription = oFighterProfile.FighterVideoDetails.ElementAt(Index).VideoDescription;
                                FighterVideo.ModifiedDate = DateTime.Today;
                                FighterVideo.FK_ModifiedBy = UserID;
                            });

                        //result.DelFlag = false;

                        //result.VideoDescription = result.VideoDescription;
                        //result.VideoLink = result.VideoLink;
                        //result.ModifiedDate = DateTime.Today;
                        //result.FK_ModifiedBy = UserID;
                    }



                }

                //// fill video list End

                //// fill photo list start
                dbContext.Details_FighterPhoto
                          .Where(p => p.FK_ProfileID == ProfileID)
                          .ToList().ForEach(r => r.DelFlag = true);

                var oFighterPhotos = (from FighterPhoto in dbContext.Details_FighterPhoto
                                      where FighterPhoto.FK_ProfileID == ProfileID
                                      select new
                                      {
                                          PK_FighterPhotoID = FighterPhoto.PK_FighterPhotoID,
                                          PhotoPath = FighterPhoto.PhotoPath
                                      }).ToList();



                for (int Index = 0; Index < oFighterProfile.FighterPhotoDetails.Count(); Index++)
                {
                    int FighterPhotoID = oFighterProfile.FighterPhotoDetails.ElementAt(Index).PK_FighterPhotoID;

                    var result = oFighterPhotos.FindAll(x => x.PK_FighterPhotoID == FighterPhotoID).FirstOrDefault();
                    if (result == null) // Insert 
                    {

                        Details_FighterPhoto oDetailsFighterPhoto = new Details_FighterPhoto
                        {
                            FK_UserID = UserID,
                            FK_ProfileID = ProfileID,
                            PhotoPath = oFighterProfile.FighterPhotoDetails.ElementAt(Index).PhotoPath,
                            CreatedDate = DateTime.Today,
                            FK_CreatedBy = UserID

                        };
                        dbContext.Details_FighterPhoto.Add(oDetailsFighterPhoto);
                        //  dbContext.SaveChanges();

                    }
                    else // update, request to delete 
                    {
                        dbContext.Details_FighterPhoto
                            .Where(p => p.PK_FighterPhotoID == FighterPhotoID)
                            .ToList().ForEach((FighterPhoto) =>
                            {
                                FighterPhoto.DelFlag = false;
                                FighterPhoto.PhotoPath = oFighterProfile.FighterPhotoDetails.ElementAt(Index).PhotoPath;
                                FighterPhoto.ModifiedDate = DateTime.Today;
                                FighterPhoto.FK_ModifiedBy = UserID;
                            });


                    }



                }

                //// fill Photo list End


                //// fill history list start

                dbContext.Details_FighterHistory
                          .Where(p => p.FK_ProfileID == ProfileID)
                          .ToList().ForEach(r => r.DelFlag = true);

                var oFighterHistorys = (from FighterHistory in dbContext.Details_FighterHistory
                                        where FighterHistory.FK_ProfileID == ProfileID
                                        select FighterHistory
                                      ).ToList();



                for (int Index = 0; Index < oFighterProfile.FighterHistoryRecords.Count(); Index++)
                {
                    int FighterHistoryID = oFighterProfile.FighterHistoryRecords.ElementAt(Index).PK_HistoryID;
                    var result = oFighterHistorys.FindAll(x => x.PK_HistoryID == FighterHistoryID).FirstOrDefault();
                    if (result == null) // Insert 
                    {

                        Details_FighterHistory oDetailsFighterHistory = new Details_FighterHistory
                        {
                            FK_UserID = UserID,
                            FK_ProfileID = ProfileID,
                            Date = oFighterProfile.FighterHistoryRecords.ElementAt(Index).Date,
                            Event = oFighterProfile.FighterHistoryRecords.ElementAt(Index).Event,
                            OpponentName = oFighterProfile.FighterHistoryRecords.ElementAt(Index).OpponentName,
                            OpponentGym = oFighterProfile.FighterHistoryRecords.ElementAt(Index).OpponentGym,
                            WeightCategory = oFighterProfile.FighterHistoryRecords.ElementAt(Index).WeightCategory,
                            Result = oFighterProfile.FighterHistoryRecords.ElementAt(Index).Result,
                            Decision = oFighterProfile.FighterHistoryRecords.ElementAt(Index).Decision,
                            TitleWon = oFighterProfile.FighterHistoryRecords.ElementAt(Index).TitleWon,
                            Venue = oFighterProfile.FighterHistoryRecords.ElementAt(Index).Venue,
                            StyleOfFight = oFighterProfile.FighterHistoryRecords.ElementAt(Index).StyleOfFight,
                            ScheduledRounds = oFighterProfile.FighterHistoryRecords.ElementAt(Index).ScheduledRounds,
                            StoppageRound = oFighterProfile.FighterHistoryRecords.ElementAt(Index).StoppageRound,
                            AmateurOrProfessional = oFighterProfile.FighterHistoryRecords.ElementAt(Index).AmateurOrProfessional,
                            TournamentStyle = oFighterProfile.FighterHistoryRecords.ElementAt(Index).TournamentStyle,
                            CreatedDate = DateTime.Today,
                            FK_CreatedBy = UserID

                        };
                        dbContext.Details_FighterHistory.Add(oDetailsFighterHistory);
                        //  dbContext.SaveChanges();

                    }
                    else // update, request to delete 
                    {
                        dbContext.Details_FighterHistory
                            .Where(p => p.PK_HistoryID == FighterHistoryID)
                            .ToList().ForEach((FighterHistory) =>
                            {
                                FighterHistory.Date = oFighterProfile.FighterHistoryRecords.ElementAt(Index).Date;
                                FighterHistory.Event = oFighterProfile.FighterHistoryRecords.ElementAt(Index).Event;
                                FighterHistory.OpponentName = oFighterProfile.FighterHistoryRecords.ElementAt(Index).OpponentName;
                                FighterHistory.OpponentGym = oFighterProfile.FighterHistoryRecords.ElementAt(Index).OpponentGym;
                                FighterHistory.WeightCategory = oFighterProfile.FighterHistoryRecords.ElementAt(Index).WeightCategory;
                                FighterHistory.Result = oFighterProfile.FighterHistoryRecords.ElementAt(Index).Result;
                                FighterHistory.Decision = oFighterProfile.FighterHistoryRecords.ElementAt(Index).Decision;
                                FighterHistory.TitleWon = oFighterProfile.FighterHistoryRecords.ElementAt(Index).TitleWon;
                                FighterHistory.Venue = oFighterProfile.FighterHistoryRecords.ElementAt(Index).Venue;
                                FighterHistory.StyleOfFight = oFighterProfile.FighterHistoryRecords.ElementAt(Index).StyleOfFight;
                                FighterHistory.ScheduledRounds = oFighterProfile.FighterHistoryRecords.ElementAt(Index).ScheduledRounds;
                                FighterHistory.StoppageRound = oFighterProfile.FighterHistoryRecords.ElementAt(Index).StoppageRound;
                                FighterHistory.AmateurOrProfessional = oFighterProfile.FighterHistoryRecords.ElementAt(Index).AmateurOrProfessional;
                                FighterHistory.TournamentStyle = oFighterProfile.FighterHistoryRecords.ElementAt(Index).TournamentStyle;
                                FighterHistory.DelFlag = false;
                                FighterHistory.Date = result.Date;
                                FighterHistory.ModifiedDate = DateTime.Today;
                                FighterHistory.FK_ModifiedBy = UserID;
                            });


                    }

                    //fill history list end

                }

                return true;

            }

            catch (Exception ex)
            {

                return false;


            }


        }
       
        [HttpGet]
        [Route("api/Profile/GetFeturedFighterProfile/{id}/{curid}")]
        public IHttpActionResult GetFeturedFighterProfile(string id, int curid)
        {
            string data = FRICrypto.decryption(id);
            int userId = Convert.ToInt32(data);
            ReturnResponse oResponse = new ReturnResponse();
            var re = Request;
            var headers = re.Headers;
            string token = "";
            if (headers.Contains("Token"))
            {
                token = headers.GetValues("Token").First();
            }
            bool IsSuperadmin = new Common().IsSuperAdmin(token);
            try
            {
                FeturedFighterProfile oFeturedFighterProfile = new FeturedFighterProfile();

                var MasterUser = (from user in dbContext.Master_User
                                  where user.PK_UserID == userId && user.DelFlag == false
                                  select user).FirstOrDefault();
                if (MasterUser.FK_Status == 0 && IsSuperadmin == false)
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "Authorization has been denied for this request.";
                    oResponse.result = null;
                    return Ok(oResponse);
                }
                else
                {
                    if (MasterUser == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    var MasterProfile = (from userprofile in dbContext.Master_Profile
                                         where userprofile.FK_UserID == userId
                                         select userprofile).FirstOrDefault();
                    if (MasterUser == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    oFeturedFighterProfile.PK_UserID = MasterUser.PK_UserID;
                    oFeturedFighterProfile.PK_UserProfileID = MasterProfile.PK_UserProfileID;
                    if (MasterUser.DOB != null)
                        oFeturedFighterProfile.Age = (int)((DateTime.Today - MasterUser.DOB.Value).TotalDays) / 365;
                    oFeturedFighterProfile.CheerCount = MasterUser.CheerCount;
                    oFeturedFighterProfile.Draws = MasterUser.Draws;
                    oFeturedFighterProfile.EmailAddress = MasterUser.EmailAddress;
                    oFeturedFighterProfile.FighterName = MasterUser.FighterName;
                    oFeturedFighterProfile.DOB = MasterUser.DOB;
                    oFeturedFighterProfile.FR_Bio = MasterProfile.FR_Bio;
                    oFeturedFighterProfile.IsFeaturedFighter = MasterProfile.Master_User.IsFeaturedfighter;
                    oFeturedFighterProfile.profilestatus = MasterProfile.Master_User.FK_Status;
                    if (string.IsNullOrEmpty(MasterProfile.FR_Reach))
                        oFeturedFighterProfile.Reach = "Not Mentioned";
                    else
                        oFeturedFighterProfile.Reach = MasterProfile.FR_Reach + " " + MasterProfile.FR_ReachUnit;
                    if (string.IsNullOrEmpty(MasterProfile.FR_LegReach))
                        oFeturedFighterProfile.LegReach = "Not Mentioned";
                    else
                        oFeturedFighterProfile.LegReach = MasterProfile.FR_LegReach + " " + MasterProfile.FR_LegReachUnit;
                    string Primary = (MasterProfile.FR_PrimaryDiscipline != null && MasterProfile.FR_PrimaryDiscipline != "") ? MasterProfile.FR_PrimaryDiscipline + ((MasterProfile.FR_PrimarySkillLevel != null && MasterProfile.FR_PrimarySkillLevel != "") ? " : " + MasterProfile.FR_PrimarySkillLevel : "") : "";
                    string Secondary = (MasterProfile.FR_SecondaryDiscipline != null && MasterProfile.FR_SecondaryDiscipline != "") ? MasterProfile.FR_SecondaryDiscipline + ((MasterProfile.FR_SecondarySkillLevel != null && MasterProfile.FR_SecondarySkillLevel != "") ? " : " + MasterProfile.FR_SecondarySkillLevel : "") : "";
                    string Tertiary = (MasterProfile.FR_TertiaryDiscipline != null && MasterProfile.FR_TertiaryDiscipline != "") ? MasterProfile.FR_TertiaryDiscipline + ((MasterProfile.FR_TertiarySkillLevel != null && MasterProfile.FR_TertiarySkillLevel != "") ? " : " + MasterProfile.FR_TertiarySkillLevel : "") : "";


                    oFeturedFighterProfile.FR_PrimaryDiscipline = Primary;
                    oFeturedFighterProfile.FR_SecondaryDiscipline = Secondary;
                    oFeturedFighterProfile.FR_TertiaryDiscipline = Tertiary;

                    // Primary = (Primary != null && Primary != "") ? Primary + " <br> " : null;
                    // Secondary = (Secondary != null && Secondary != "") ? Secondary + " <br> " : null;
                    // Tertiary = (Tertiary != null && Tertiary != "") ? Tertiary  : null;


                    // oFeturedFighterProfile.FR_PrimaryDiscipline = Primary + Secondary + Tertiary;

                    //  oFeturedFighterProfile.FR_PrimarySkillLevel = MasterProfile.FR_PrimarySkillLevel;
                    //if (string.IsNullOrEmpty(MasterProfile.FR_HeightCM))
                    oFeturedFighterProfile.Height = MasterProfile.FR_HeightCM.ToString() + " Centimeters";
                    //else
                    //oFeturedFighterProfile.Height = MasterProfile.FR_Height + "" + MasterProfile.FR_HeightUnit;
                    if (string.IsNullOrEmpty(MasterProfile.FR_CurrentWeight))
                        oFeturedFighterProfile.FR_CurrentWeight = "Not Mentioned";
                    else
                        oFeturedFighterProfile.FR_CurrentWeight = MasterProfile.FR_CurrentWeight + " " + MasterProfile.FR_CurrentWeightUnit;
                    string state = MasterUser.FK_ProvinceOrState > 0 ? MasterUser.Lookup_StateOrProvince.Description : null;
                    oFeturedFighterProfile.Location = MasterUser.City == null || MasterUser.City == "" ? state : MasterUser.City + ", " + state;
                    oFeturedFighterProfile.Losses = MasterUser.Losses;
                    oFeturedFighterProfile.Wins = MasterUser.Wins;
                    oFeturedFighterProfile.Name = MasterUser.LegalMiddleName == null || MasterUser.LegalMiddleName == "" ? MasterUser.LegalFirstName + " " + MasterUser.LastName : MasterUser.LegalFirstName + " " + MasterUser.LegalMiddleName + " " + MasterUser.LastName;


                    oFeturedFighterProfile.ProfilePhotoPath = MasterUser.ProfilePhotoPath;


                    List<SocialMediaRecord> SocialMediaRecords = new List<SocialMediaRecord>();
                    SocialMediaRecords = (from SocialMediaRecord in MasterUser.Details_UserSocialMedia
                                          where SocialMediaRecord.DelFlag == false
                                          select new SocialMediaRecord
                                          {

                                              FK_SocialMediaID = SocialMediaRecord.FK_SocialMediaID,
                                              PK_UserSocialMediaID = SocialMediaRecord.PK_UserSocialMediaID,
                                              SocialMedia = SocialMediaRecord.SocialMedia,
                                              URL = SocialMediaRecord.URL,
                                              IconPath = SocialMediaRecord.Lookup_SocialMediaPlatform.IconPath,
                                          }).ToList();

                    oFeturedFighterProfile.SocialMediaRecords = SocialMediaRecords;


                    //// fill video list start

                    List<FighterVideo> oFighterVideos = new List<FighterVideo>();
                    oFighterVideos = (from FighterVideo in MasterUser.Details_FighterVideo
                                      where FighterVideo.DelFlag == false
                                      select new FighterVideo
                                      {
                                          PK_FighterVideoID = FighterVideo.PK_FighterVideoID,
                                          VideoLink = FighterVideo.VideoLink,
                                          VideoDescription = FighterVideo.VideoDescription
                                      }).ToList();

                    oFeturedFighterProfile.FighterVideoDetails = oFighterVideos;
                    ////fill video list end


                    //// fill video list start
                    List<FighterPhoto> oFighterPhotos = new List<FighterPhoto>();
                    oFighterPhotos = (from FighterPhoto in MasterUser.Details_FighterPhoto
                                      where FighterPhoto.DelFlag == false
                                      select new FighterPhoto
                                      {
                                          PK_FighterPhotoID = FighterPhoto.PK_FighterPhotoID,
                                          PhotoPath = FighterPhoto.PhotoPath
                                      }).ToList();

                    oFeturedFighterProfile.FighterPhotoDetails = oFighterPhotos;


                    Master_Cheer oCheer = dbContext.Master_Cheer.Where(p => p.FK_CheerFromID == curid && p.FK_CheerToID == userId && p.DelFlag == false).FirstOrDefault();

                    if (oCheer == null)
                        oFeturedFighterProfile.IsCheered = false;
                    else
                        oFeturedFighterProfile.IsCheered = true;



                    Master_Follower oFollower = dbContext.Master_Follower.Where(p => p.FK_FollowerFrom == curid && p.FK_FollowerTo == userId && p.DelFlag == false).FirstOrDefault();

                    if (oFollower == null)
                        oFeturedFighterProfile.IsTracked = false;
                    else
                        oFeturedFighterProfile.IsTracked = true;



                    ////fill video list end



                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = oFeturedFighterProfile;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }



        [HttpGet]
        [Route("api/Profile/GetFeturedFighterProfile/{id}")]
        public IHttpActionResult GetFeturedFighterProfile(string id)
        {
            string data = FRICrypto.decryption(id);
            int userId = Convert.ToInt32(data);
            var re = Request;
            var headers = re.Headers;
            string token = "";
            if (headers.Contains("Token"))
            {
                token = headers.GetValues("Token").First();
            }
            bool IsSuperadmin = new Common().IsSuperAdmin(token);
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                FeturedFighterProfile oFeturedFighterProfile = new FeturedFighterProfile();

                var MasterUser = (from user in dbContext.Master_User
                                  where user.PK_UserID == userId && user.DelFlag == false
                                  select user).FirstOrDefault();
                if (MasterUser.FK_Status == 0 && IsSuperadmin == false)
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "Authorization has been denied for this request.";
                    oResponse.result = null;
                    return Ok(oResponse);
                }
                else
                {
                    if (MasterUser == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    var MasterProfile = (from userprofile in dbContext.Master_Profile
                                         where userprofile.FK_UserID == userId
                                         select userprofile).FirstOrDefault();

                    if (MasterProfile == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    oFeturedFighterProfile.PK_UserID = MasterUser.PK_UserID;
                    oFeturedFighterProfile.PK_UserProfileID = MasterProfile.PK_UserProfileID;
                    if (MasterUser.DOB != null)
                        oFeturedFighterProfile.Age = (int)((DateTime.Today - MasterUser.DOB.Value).TotalDays) / 365;

                    oFeturedFighterProfile.CheerCount = MasterUser.CheerCount;
                    oFeturedFighterProfile.Draws = MasterUser.Draws;
                    oFeturedFighterProfile.EmailAddress = MasterUser.EmailAddress;
                    oFeturedFighterProfile.FighterName = MasterUser.FighterName;
                    oFeturedFighterProfile.DOB = MasterUser.DOB;
                    oFeturedFighterProfile.FR_Bio = MasterProfile.FR_Bio;
                    oFeturedFighterProfile.Reach = MasterProfile.FR_Reach;
                    oFeturedFighterProfile.LegReach = MasterProfile.FR_LegReach;
                    oFeturedFighterProfile.profilestatus = MasterProfile.Master_User.FK_Status;
                    oFeturedFighterProfile.FR_CurrentWeight = MasterProfile.FR_CurrentWeight + " " + MasterProfile.FR_CurrentWeightUnit;
                    if (String.IsNullOrEmpty(oFeturedFighterProfile.FR_CurrentWeight))
                        oFeturedFighterProfile.FR_CurrentWeight = "Not Mentioned";


                    oFeturedFighterProfile.FR_PrimaryDiscipline = MasterProfile.FR_PrimaryDiscipline;
                    oFeturedFighterProfile.FR_PrimarySkillLevel = MasterProfile.FR_PrimarySkillLevel;
                    oFeturedFighterProfile.Height = MasterProfile.FR_Height + " " + MasterProfile.FR_HeightUnit;
                    if (string.IsNullOrEmpty(oFeturedFighterProfile.Height))
                        oFeturedFighterProfile.Height = "Not Mentioned";

                    oFeturedFighterProfile.LegReach = MasterProfile.FR_LegReach + " " + MasterProfile.FR_LegReachUnit;
                    if (string.IsNullOrEmpty(oFeturedFighterProfile.LegReach))
                        oFeturedFighterProfile.LegReach = "Not Mentioned";
                    string state = MasterUser.FK_ProvinceOrState > 0 ? MasterUser.Lookup_StateOrProvince.Description : null;
                    oFeturedFighterProfile.Location = MasterUser.City == null || MasterUser.City == "" ? state : MasterUser.City + ", " + state;
                    oFeturedFighterProfile.Losses = MasterUser.Losses;
                    oFeturedFighterProfile.Wins = MasterUser.Wins;
                    oFeturedFighterProfile.Draws = MasterUser.Draws;
                    oFeturedFighterProfile.Name = MasterUser.LegalMiddleName == null || MasterUser.LegalMiddleName == "" ? MasterUser.LegalFirstName + " " + MasterUser.LastName : MasterUser.LegalFirstName + " " + MasterUser.LegalMiddleName + " " + MasterUser.LastName;
                    oFeturedFighterProfile.Reach = MasterProfile.FR_Reach + " " + MasterProfile.FR_ReachUnit;
                    if (string.IsNullOrEmpty(oFeturedFighterProfile.Reach))
                        oFeturedFighterProfile.Reach = "Not Mentioned";
                    oFeturedFighterProfile.ProfilePhotoPath = MasterUser.ProfilePhotoPath;
                    List<SocialMediaRecord> SocialMediaRecords = new List<SocialMediaRecord>();
                    SocialMediaRecords = (from SocialMediaRecord in MasterUser.Details_UserSocialMedia
                                          where SocialMediaRecord.DelFlag == false
                                          select new SocialMediaRecord
                                          {

                                              FK_SocialMediaID = SocialMediaRecord.FK_SocialMediaID,
                                              PK_UserSocialMediaID = SocialMediaRecord.PK_UserSocialMediaID,
                                              SocialMedia = SocialMediaRecord.SocialMedia,
                                              URL = SocialMediaRecord.URL,
                                              IconPath = SocialMediaRecord.Lookup_SocialMediaPlatform.IconPath,
                                          }).ToList();

                    oFeturedFighterProfile.SocialMediaRecords = SocialMediaRecords;


                    //// fill video list start

                    List<FighterVideo> oFighterVideos = new List<FighterVideo>();
                    oFighterVideos = (from FighterVideo in MasterUser.Details_FighterVideo
                                      where FighterVideo.DelFlag == false
                                      select new FighterVideo
                                      {
                                          PK_FighterVideoID = FighterVideo.PK_FighterVideoID,
                                          VideoLink = FighterVideo.VideoLink,
                                          VideoDescription = FighterVideo.VideoDescription
                                      }).ToList();

                    oFeturedFighterProfile.FighterVideoDetails = oFighterVideos;
                    ////fill video list end


                    //// fill video list start
                    List<FighterPhoto> oFighterPhotos = new List<FighterPhoto>();
                    oFighterPhotos = (from FighterPhoto in MasterUser.Details_FighterPhoto
                                      where FighterPhoto.DelFlag == false
                                      select new FighterPhoto
                                      {
                                          PK_FighterPhotoID = FighterPhoto.PK_FighterPhotoID,
                                          PhotoPath = FighterPhoto.PhotoPath
                                      }).ToList();

                    oFeturedFighterProfile.FighterPhotoDetails = oFighterPhotos;



                    oFeturedFighterProfile.IsCheered = false;

                    oFeturedFighterProfile.IsTracked = false;




                    ////fill video list end



                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = oFeturedFighterProfile;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }

        [HttpGet]
        [Route("api/Profile/GetOfficialProfile/{id}")]
        public IHttpActionResult GetOfficialProfile(string id)
        {
            string data = FRICrypto.decryption(id);
            int userId = Convert.ToInt32(data);
            ReturnResponse oResponse = new ReturnResponse();
            var re = Request;
            var headers = re.Headers;
            string token = "";
            if (headers.Contains("Token"))
            {
                token = headers.GetValues("Token").First();
            }
            bool IsSuperadmin = new Common().IsSuperAdmin(token);

            try
            {
                OfficialDetailsView officialProfile = new OfficialDetailsView();

                var user = dbContext.Master_User
                    .Where(x => x.PK_UserID == userId && !x.DelFlag)
                    .Include(x => x.Lookup_StateOrProvince)
                    .FirstOrDefault();
                if (user.FK_Status == 0 && IsSuperadmin == false)
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "Authorization has been denied for this request.";
                    oResponse.result = null;
                    return Ok(oResponse);
                }
                else
                {
                    if (user == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    var masterProfile = (from userprofile in dbContext.Master_Profile
                                         where userprofile.FK_UserID == userId
                                         select userprofile).FirstOrDefault();

                    if (masterProfile == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    officialProfile.PK_UserID = user.PK_UserID;
                    officialProfile.PK_UserProfileID = masterProfile.PK_UserProfileID;
                    officialProfile.Name = String.Format("{0} {1} {2}", user.LegalFirstName, user.LegalMiddleName, user.LastName);
                    officialProfile.DistrictOfAuthorization = masterProfile.CN_DistrictOfAuthorization;
                    officialProfile.FighterName = user.FighterName;
                    officialProfile.profilestatus = masterProfile.Master_User.FK_Status;
                    officialProfile.commisionarname = masterProfile.CN_PriContactLegalFirstName + " " + masterProfile.CN_PriContactLegalMiddleName + " " + masterProfile.CN_PriContactLastName;
                    if (user.DOB.HasValue)
                        officialProfile.DateOfBirth = user.DOB.Value;

                    officialProfile.Email = user.EmailAddress;

                    if (String.IsNullOrEmpty(officialProfile.Email))
                        officialProfile.Email = "Not Mentioned";

                    officialProfile.Phone = user.Phone;
                    if (String.IsNullOrEmpty(officialProfile.Phone))
                        officialProfile.Phone = "Not Mentioned";

                    officialProfile.WebSite = masterProfile.CN_webaddress;
                    if (String.IsNullOrEmpty(officialProfile.WebSite))
                        officialProfile.WebSite = "Not Mentioned";

                    //Commented Nisha
                    //officialProfile.Country = user.CountryName;
                    //officialProfile.State = user.StateName;

                    //Additional fields

                    //string state = String.Empty;
                    //if (user.FK_ProvinceOrState > 0 && user.Lookup_StateOrProvince != null)
                    //{
                    //    state = user.Lookup_StateOrProvince.Description;
                    //}

                    string state = user.FK_ProvinceOrState > 0 ? user.Lookup_StateOrProvince.Description : null;
                    string country = user.FK_Country > 0 ? user.Lookup_Country.Description : null;
                    officialProfile.Location = string.IsNullOrEmpty(user.City) ? state : user.City + ", " + state;
                    officialProfile.Address = string.IsNullOrEmpty(user.City) ? state : user.City + ", " + state + ", " + country;
                    officialProfile.ProfilePhotoPath = user.ProfilePhotoPath;


                    var socialMediaRecords = dbContext.Details_UserSocialMedia.Where(x => x.FK_UserID == userId && !x.DelFlag).Select(x =>
                         new SocialMediaRecord
                         {
                             FK_SocialMediaID = x.FK_SocialMediaID,
                             PK_UserSocialMediaID = x.PK_UserSocialMediaID,
                             SocialMedia = x.SocialMedia,
                             URL = x.URL,
                             IconPath = x.Lookup_SocialMediaPlatform.IconPath,
                         }).ToList();


                    officialProfile.SocialMediaRecords = socialMediaRecords;

                    var officialDetails = dbContext.Details_Official
                        .Where(x => x.FK_ProfileID == masterProfile.PK_UserProfileID && !x.DelFlag)
                        .Select(x => new OfficialDetail
                        {
                            PK_OfficialProfileID = x.PK_OfficialProfileID,
                            OfficialLevel = x.OfficialLevel,
                            TypeOfOfficial = x.TypeOfOfficial,
                            Discipline = x.Discipline
                        }).ToList();

                    officialProfile.OfficialDetails = officialDetails;

                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = officialProfile;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }

        [HttpGet]
        [Route("api/Profile/GetPromotorProfile/{id}")]
        public IHttpActionResult GetPromotorProfile(string id)
        {
            ReturnResponse oResponse = new ReturnResponse();
            var re = Request;
            var headers = re.Headers;
            string token = "";
            if (headers.Contains("Token"))
            {
                token = headers.GetValues("Token").First();
            }
            bool IsSuperadmin = new Common().IsSuperAdmin(token);
            try
            {
                string data = FRICrypto.decryption(id);
                int userId = Convert.ToInt32(data);
                PromotorDetailsView promotorProfile = new PromotorDetailsView();

                var user = (from fighter in dbContext.Master_User
                            where fighter.PK_UserID == userId && fighter.DelFlag == false
                            select fighter).FirstOrDefault();

                if (user.FK_Status == 0 && IsSuperadmin == false)
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "Authorization has been denied for this request.";
                    oResponse.result = null;
                    return Ok(oResponse);
                }
                else
                {
                    if (user == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    var masterProfile = (from userprofile in dbContext.Master_Profile
                                         where userprofile.FK_UserID == userId
                                         select userprofile).FirstOrDefault();

                    if (masterProfile == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    promotorProfile.PK_UserID = user.PK_UserID;
                    promotorProfile.PK_UserProfileID = masterProfile.PK_UserProfileID;
                    promotorProfile.Name = String.Format("{0} {1} {2}", user.LegalFirstName, user.LegalMiddleName, user.LastName);
                    promotorProfile.FighterName = user.FighterName;
                    // promotorProfile.DateOfBirth = user.DOB.Value;
                    promotorProfile.Email = user.EmailAddress;
                    if (string.IsNullOrEmpty(promotorProfile.Email))
                        promotorProfile.Email = "Not Mentioned";
                    promotorProfile.Phone = user.Phone;
                    if (string.IsNullOrEmpty(promotorProfile.Phone))
                        promotorProfile.Phone = "Not Mentioned";
                    promotorProfile.WebSite = user.PersonalWebsite;
                    if (string.IsNullOrEmpty(promotorProfile.WebSite))
                        promotorProfile.WebSite = "Not Mentioned";
                    promotorProfile.Title = masterProfile.PR_Title;
                    if (string.IsNullOrEmpty(promotorProfile.Title))
                        promotorProfile.Title = "Not Mentioned";
                    promotorProfile.Country = user.CountryName;
                    if (string.IsNullOrEmpty(promotorProfile.Country))
                        promotorProfile.Country = "Not Mentioned";
                    promotorProfile.State = user.StateName;
                    if (string.IsNullOrEmpty(promotorProfile.State))
                        promotorProfile.State = "Not Mentioned";
                    promotorProfile.EventNameOrSeries = masterProfile.PR_EventNameOrSeries;
                    string state = user.FK_ProvinceOrState > 0 ? user.Lookup_StateOrProvince.Description : null;
                    promotorProfile.Location = user.City == null || user.City == "" ? state : user.City + ", " + state;
                    promotorProfile.ProfilePhotoPath = user.ProfilePhotoPath;
                    promotorProfile.profilestatus = masterProfile.Master_User.FK_Status;
                    List<SocialMediaRecord> SocialMediaRecords = new List<SocialMediaRecord>();
                    SocialMediaRecords = (from SocialMediaRecord in user.Details_UserSocialMedia
                                          where SocialMediaRecord.DelFlag == false
                                          select new SocialMediaRecord
                                          {

                                              FK_SocialMediaID = SocialMediaRecord.FK_SocialMediaID,
                                              PK_UserSocialMediaID = SocialMediaRecord.PK_UserSocialMediaID,
                                              SocialMedia = SocialMediaRecord.SocialMedia,
                                              URL = SocialMediaRecord.URL,
                                              IconPath = SocialMediaRecord.Lookup_SocialMediaPlatform.IconPath,
                                          }).ToList();

                    promotorProfile.SocialMediaRecords = SocialMediaRecords;

                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = promotorProfile;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;
            }

            return Ok(oResponse);
        }
        //Nisha
        [HttpGet]
        [Route("api/Profile/GetUserProfiles/{id}")]
        public IHttpActionResult GetUserProfiles(string id)
        {
            ReturnResponse oResponse = new ReturnResponse();
            var re = Request;
            var headers = re.Headers;
            string token = "";
            if (headers.Contains("Token"))
            {
                token = headers.GetValues("Token").First();
            }
            bool IsSuperadmin = new Common().IsSuperAdmin(token);

            try
            {
                string data = FRICrypto.decryption(id);
                int userId = Convert.ToInt32(data);
                UserDetailsView userProfile = new UserDetailsView();

                var user = (from fighter in dbContext.Master_User
                            where fighter.PK_UserID == userId && fighter.DelFlag == false
                            select fighter).FirstOrDefault();
                if (user.FK_Status == 0 && IsSuperadmin == false)
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "Authorization has been denied for this request.";
                    oResponse.result = null;
                    return Ok(oResponse);
                }
                else
                {
                    if (user == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    var masterProfile = (from userprofile in dbContext.Master_Profile
                                         where userprofile.FK_UserID == userId
                                         select userprofile).FirstOrDefault();

                    if (masterProfile == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    userProfile.PK_UserID = user.PK_UserID;
                    userProfile.PK_UserProfileID = masterProfile.PK_UserProfileID;
                    userProfile.Name = String.Format("{0} {1} {2}", user.LegalFirstName, user.LegalMiddleName, user.LastName);
                    userProfile.FighterName = user.FighterName;
                    if (user.DOB.HasValue)
                        userProfile.DateOfBirth = user.DOB.Value;
                    userProfile.Phone = user.Phone;
                    if (string.IsNullOrEmpty(userProfile.Phone))
                        userProfile.Phone = "Not Mentioned";
                    userProfile.WebSite = user.PersonalWebsite;
                    if (string.IsNullOrEmpty(userProfile.WebSite))
                        userProfile.WebSite = "Not Mentioned";
                    string state = user.FK_ProvinceOrState > 0 ? user.Lookup_StateOrProvince.Description : null;
                    userProfile.Location = user.City == null || user.City == "" ? state : user.City + ", " + state;
                    userProfile.ProfilePhotoPath = user.ProfilePhotoPath;

                    List<SocialMediaRecord> SocialMediaRecords = new List<SocialMediaRecord>();
                    SocialMediaRecords = (from SocialMediaRecord in user.Details_UserSocialMedia
                                          where SocialMediaRecord.DelFlag == false
                                          select new SocialMediaRecord
                                          {

                                              FK_SocialMediaID = SocialMediaRecord.FK_SocialMediaID,
                                              PK_UserSocialMediaID = SocialMediaRecord.PK_UserSocialMediaID,
                                              SocialMedia = SocialMediaRecord.SocialMedia,
                                              URL = SocialMediaRecord.URL,
                                              IconPath = SocialMediaRecord.Lookup_SocialMediaPlatform.IconPath,
                                          }).ToList();

                    userProfile.SocialMediaRecords = SocialMediaRecords;

                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = userProfile;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;
            }

            return Ok(oResponse);
        }

        [HttpGet]
        [Route("api/Profile/GetCommossionProfile/{id}")]
        public IHttpActionResult GetCommossionProfile(int id)
        {
            ReturnResponse oResponse = new ReturnResponse();
            var re = Request;
            var headers = re.Headers;
            string token = "";
            if (headers.Contains("Token"))
            {
                token = headers.GetValues("Token").First();
            }
            bool IsSuperadmin = new Common().IsSuperAdmin(token);
            try
            {
                CommissionDetailsView CommissionrProfile = new CommissionDetailsView();

                var user = (from fighter in dbContext.Master_User
                            where fighter.PK_UserID == id && fighter.DelFlag == false
                            select fighter).FirstOrDefault();
                if (user.FK_Status == 0 && IsSuperadmin == false)
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "Authorization has been denied for this request.";
                    oResponse.result = null;
                    return Ok(oResponse);
                }
                else
                {
                    if (user == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    var masterProfile = (from userprofile in dbContext.Master_Profile
                                         where userprofile.FK_UserID == id
                                         select userprofile).FirstOrDefault();

                    if (masterProfile == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    CommissionrProfile.PK_UserID = user.PK_UserID;
                    CommissionrProfile.PK_UserProfileID = masterProfile.PK_UserProfileID;
                    CommissionrProfile.Name = String.Format("{0} {1} {2}", user.LegalFirstName, user.LegalMiddleName, user.LastName);
                    CommissionrProfile.FighterName = user.FighterName;
                    CommissionrProfile.DateOfBirth = user.DOB.Value;
                    CommissionrProfile.Email = user.EmailAddress;
                    if (string.IsNullOrEmpty(CommissionrProfile.Email))
                        CommissionrProfile.Email = "Not Mentioned";
                    CommissionrProfile.Phone = user.Phone;
                    if (string.IsNullOrEmpty(CommissionrProfile.Phone))
                        CommissionrProfile.Phone = "Not Mentioned";
                    CommissionrProfile.WebSite = user.PersonalWebsite;
                    if (string.IsNullOrEmpty(CommissionrProfile.WebSite))
                        CommissionrProfile.WebSite = "Not Mentioned";
                    CommissionrProfile.RepresentCommission = masterProfile.CN_Commission;
                    CommissionrProfile.DistrictofAuthorization = masterProfile.CN_DistrictOfAuthorization;
                    string state = user.FK_ProvinceOrState > 0 ? user.Lookup_StateOrProvince.Description : null;
                    CommissionrProfile.Location = user.City == null || user.City == "" ? state : user.City + ", " + state;
                    CommissionrProfile.ProfilePhotoPath = user.ProfilePhotoPath;
                    CommissionrProfile.profilestatus = masterProfile.Master_User.FK_Status;
                    List<SocialMediaRecord> SocialMediaRecords = new List<SocialMediaRecord>();
                    SocialMediaRecords = (from SocialMediaRecord in user.Details_UserSocialMedia
                                          where SocialMediaRecord.DelFlag == false
                                          select new SocialMediaRecord
                                          {

                                              FK_SocialMediaID = SocialMediaRecord.FK_SocialMediaID,
                                              PK_UserSocialMediaID = SocialMediaRecord.PK_UserSocialMediaID,
                                              SocialMedia = SocialMediaRecord.SocialMedia,
                                              URL = SocialMediaRecord.URL,
                                              IconPath = SocialMediaRecord.Lookup_SocialMediaPlatform.IconPath,
                                          }).ToList();

                    CommissionrProfile.SocialMediaRecords = SocialMediaRecords;

                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = CommissionrProfile;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;
            }

            return Ok(oResponse);
        }


        [HttpGet]
        [Route("api/Profile/GetSanctioningBodyProfile/{id}")]
        public IHttpActionResult GetSanctioningBodyProfile(int id)
        {
            ReturnResponse oResponse = new ReturnResponse();
            var re = Request;
            var headers = re.Headers;
            string token = "";
            if (headers.Contains("Token"))
            {
                token = headers.GetValues("Token").First();
            }
            bool IsSuperadmin = new Common().IsSuperAdmin(token);
            try
            {
                SanctioningBodyView SanctioningBodyProfile = new SanctioningBodyView();

                var user = (from fighter in dbContext.Master_User
                            where fighter.PK_UserID == id && fighter.DelFlag == false
                            select fighter).FirstOrDefault();
                if (user.FK_Status == 0 && IsSuperadmin == false)
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "Authorization has been denied for this request.";
                    oResponse.result = null;
                    return Ok(oResponse);
                }
                else
                {
                    if (user == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    var masterProfile = (from userprofile in dbContext.Master_Profile
                                         where userprofile.FK_UserID == id
                                         select userprofile).FirstOrDefault();

                    if (masterProfile == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    SanctioningBodyProfile.PK_UserID = user.PK_UserID;
                    SanctioningBodyProfile.PK_UserProfileID = masterProfile.PK_UserProfileID;
                    SanctioningBodyProfile.Name = String.Format("{0} {1} {2}", user.LegalFirstName, user.LegalMiddleName, user.LastName);
                    SanctioningBodyProfile.FighterName = user.FighterName;
                    if (user.DOB.HasValue)
                        SanctioningBodyProfile.DateOfBirth = user.DOB.Value;
                    SanctioningBodyProfile.Email = user.EmailAddress;
                    if (string.IsNullOrEmpty(SanctioningBodyProfile.Email))
                        SanctioningBodyProfile.Email = "Not Mentioned";
                    SanctioningBodyProfile.Phone = user.Phone;
                    if (string.IsNullOrEmpty(SanctioningBodyProfile.Phone))
                        SanctioningBodyProfile.Phone = "Not Mentioned";
                    SanctioningBodyProfile.WebSite = user.PersonalWebsite;
                    if (string.IsNullOrEmpty(SanctioningBodyProfile.WebSite))
                        SanctioningBodyProfile.WebSite = "Not Mentioned";
                    SanctioningBodyProfile.Style = masterProfile.SB_Style;
                    SanctioningBodyProfile.Title = masterProfile.SB_Title;
                    string state = user.FK_ProvinceOrState > 0 ? user.Lookup_StateOrProvince.Description : null;
                    SanctioningBodyProfile.Location = user.City == null || user.City == "" ? state : user.City + ", " + state;
                    SanctioningBodyProfile.ProfilePhotoPath = user.ProfilePhotoPath;

                    List<SocialMediaRecord> SocialMediaRecords = new List<SocialMediaRecord>();
                    SocialMediaRecords = (from SocialMediaRecord in user.Details_UserSocialMedia
                                          where SocialMediaRecord.DelFlag == false
                                          select new SocialMediaRecord
                                          {

                                              FK_SocialMediaID = SocialMediaRecord.FK_SocialMediaID,
                                              PK_UserSocialMediaID = SocialMediaRecord.PK_UserSocialMediaID,
                                              SocialMedia = SocialMediaRecord.SocialMedia,
                                              URL = SocialMediaRecord.URL,
                                              IconPath = SocialMediaRecord.Lookup_SocialMediaPlatform.IconPath,
                                          }).ToList();

                    SanctioningBodyProfile.SocialMediaRecords = SocialMediaRecords;

                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = SanctioningBodyProfile;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;
            }

            return Ok(oResponse);
        }



        [HttpGet]
        [Route("api/Profile/GetSanctioningBodyDetails/{id}")]
        public IHttpActionResult GetSanctioningBodyDetails(string id)
        {
            string data = FRICrypto.decryption(id);
            int userId = Convert.ToInt32(data);
            ReturnResponse oResponse = new ReturnResponse();
            var re = Request;
            var headers = re.Headers;
            string token = "";
            if (headers.Contains("Token"))
            {
                token = headers.GetValues("Token").First();
            }
            bool IsSuperadmin = new Common().IsSuperAdmin(token);
            try
            {
                SanctioningBody SanctioningBodyProfile = new SanctioningBody();

                var user = (from fighter in dbContext.Master_User
                            where fighter.PK_UserID == userId && fighter.DelFlag == false
                            select fighter).FirstOrDefault();
                if (user.FK_Status == 0 && IsSuperadmin == false)
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "Authorization has been denied for this request.";
                    oResponse.result = null;
                    return Ok(oResponse);
                }
                else
                {
                    if (user == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    var masterProfile = (from userprofile in dbContext.Master_Profile
                                         where userprofile.FK_UserID == userId
                                         select userprofile).FirstOrDefault();

                    if (masterProfile == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }
                    SanctioningBodyProfile.Description = masterProfile.SB_Description;
                    if (string.IsNullOrEmpty(SanctioningBodyProfile.Description))
                        SanctioningBodyProfile.Description = "Not Mentioned";
                    SanctioningBodyProfile.Phone = user.Phone;
                    if (string.IsNullOrEmpty(SanctioningBodyProfile.Phone))
                        SanctioningBodyProfile.Phone = "Not Mentioned";
                    SanctioningBodyProfile.Website = masterProfile.SB_WebAddress;
                    if (string.IsNullOrEmpty(SanctioningBodyProfile.Website))
                        SanctioningBodyProfile.Website = "Not Mentioned";
                    SanctioningBodyProfile.EmailAddress = user.EmailAddress;
                    if (string.IsNullOrEmpty(user.EmailAddress))
                        SanctioningBodyProfile.EmailAddress = "Not Mentioned";
                    SanctioningBodyProfile.FK_Country = user.FK_Country;
                    SanctioningBodyProfile.FK_ProvinceOrState = user.FK_ProvinceOrState;
                    SanctioningBodyProfile.City = user.City;
                    string state = user.FK_ProvinceOrState > 0 ? user.Lookup_StateOrProvince.Description : null;
                    string country = user.FK_Country > 0 ? user.Lookup_Country.Description : null;
                    SanctioningBodyProfile.Address = string.IsNullOrEmpty(user.City) ? state : user.City + ", " + state + ", " + country;
                    SanctioningBodyProfile.PK_UserID = user.PK_UserID;
                    SanctioningBodyProfile.PostalOrZipCode = user.PostalOrZipCode;
                    SanctioningBodyProfile.Street = user.Street;
                    SanctioningBodyProfile.ProfilePhotoPath = user.ProfilePhotoPath;

                    SanctioningBodyProfile.PriContactLastName = masterProfile.SB_PriContactLastName;
                    SanctioningBodyProfile.PriContactLegalFirstName = masterProfile.SB_PriContactLegalFirstName;
                    SanctioningBodyProfile.PriContactLegalMiddleName = masterProfile.SB_PriContactLegalMiddleName;
                    SanctioningBodyProfile.FullName = masterProfile.SB_PriContactLegalFirstName + " " + masterProfile.SB_PriContactLegalMiddleName + " " + masterProfile.SB_PriContactLastName;

                    SanctioningBodyProfile.SecContactLastName = masterProfile.SB_SecContactLastName;
                    SanctioningBodyProfile.SecContactLegalFirstName = masterProfile.SB_SecContactLegalFirstName;
                    SanctioningBodyProfile.SecContactLegalMiddleName = masterProfile.SB_SecContactLegalMiddleName;
                    SanctioningBodyProfile.SecContactEmail = masterProfile.SB_SecondaryContact;
                    SanctioningBodyProfile.PriContactEmail = masterProfile.CN_PrimaryEmail;
                    SanctioningBodyProfile.SecurityAnswer = user.SecurityAnswer;
                    SanctioningBodyProfile.SecurityQuestion = user.SecurityQuestion;
                    SanctioningBodyProfile.PK_UserProfileID = masterProfile.PK_UserProfileID;
                    SanctioningBodyProfile.Title = masterProfile.SB_Title;
                    SanctioningBodyProfile.profilestatus = masterProfile.Master_User.FK_Status;
                    SanctioningBodyProfile.Location = user.City == null || user.City == "" ? state : user.City + ", " + state;
                    SanctioningBodyProfile.termsCondtnCheck = true;
                    SanctioningBodyProfile.codeOfconduct = true;
                    List<SocialMediaRecord> SocialMediaRecords = new List<SocialMediaRecord>();
                    SocialMediaRecords = (from SocialMediaRecord in user.Details_UserSocialMedia
                                          where SocialMediaRecord.DelFlag == false
                                          select new SocialMediaRecord
                                          {

                                              FK_SocialMediaID = SocialMediaRecord.FK_SocialMediaID,
                                              PK_UserSocialMediaID = SocialMediaRecord.PK_UserSocialMediaID,
                                              SocialMedia = SocialMediaRecord.SocialMedia,
                                              URL = SocialMediaRecord.URL,
                                              IconPath = SocialMediaRecord.Lookup_SocialMediaPlatform.IconPath,
                                          }).ToList();

                    SanctioningBodyProfile.SocialMediaRecords = SocialMediaRecords;

                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = SanctioningBodyProfile;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;
            }

            return Ok(oResponse);
        }


        [HttpGet]
        [Route("api/Profile/GetCommissionDetails/{id}")]
        public IHttpActionResult GetCommissionDetails(string id)
        {
            string data = FRICrypto.decryption(id);
            int userId = Convert.ToInt32(data);
            ReturnResponse oResponse = new ReturnResponse();
            var re = Request;
            var headers = re.Headers;
            string token = "";
            if (headers.Contains("Token"))
            {
                token = headers.GetValues("Token").First();
            }
            bool IsSuperadmin = new Common().IsSuperAdmin(token);
            try
            {
                CommissionInfo CommissionProfile = new CommissionInfo();

                var user = (from fighter in dbContext.Master_User
                            where fighter.PK_UserID == userId && fighter.DelFlag == false
                            select fighter).FirstOrDefault();
                if (user.FK_Status == 0 && IsSuperadmin == false)
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "Authorization has been denied for this request.";
                    oResponse.result = null;
                    return Ok(oResponse);
                }
                else
                {
                    if (user == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    var masterProfile = (from userprofile in dbContext.Master_Profile
                                         where userprofile.FK_UserID == userId
                                         select userprofile).FirstOrDefault();

                    if (masterProfile == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    CommissionProfile.Description = masterProfile.CN_Description;
                    CommissionProfile.EmailAddress = user.EmailAddress;
                    CommissionProfile.AuthorizingBody = masterProfile.CN_AuthorizingBody;
                    CommissionProfile.FK_Country = user.FK_Country;
                    CommissionProfile.FK_ProvinceOrState = user.FK_ProvinceOrState;
                    CommissionProfile.City = user.City;
                    CommissionProfile.Phone = user.Phone;
                    CommissionProfile.PK_UserID = user.PK_UserID;
                    CommissionProfile.PostalOrZipCode = user.PostalOrZipCode;
                    CommissionProfile.Street = user.Street;
                    CommissionProfile.ProfilePhotoPath = user.ProfilePhotoPath;

                    CommissionProfile.DistrictOfAuthorization = masterProfile.CN_DistrictOfAuthorization;
                    CommissionProfile.Website = masterProfile.CN_webaddress;

                    CommissionProfile.PriContactLastName = masterProfile.CN_PriContactLastName;
                    CommissionProfile.PriContactLegalFirstName = masterProfile.CN_PriContactLegalFirstName;
                    CommissionProfile.PriContactLegalMiddleName = masterProfile.CN_PriContactLegalMiddleName;

                    CommissionProfile.SecContactLastName = masterProfile.CN_SecContactLastName;
                    CommissionProfile.SecContactLegalFirstName = masterProfile.CN_SecContactLegalFirstName;
                    CommissionProfile.SecContactLegalMiddleName = masterProfile.CN_SecContactLegalMiddleName;
                    CommissionProfile.SecContactEmail = masterProfile.CN_SecondaryEmail;
                    CommissionProfile.PriContactEmail = masterProfile.CN_PrimaryContact;
                    CommissionProfile.SecurityAnswer = user.SecurityAnswer;
                    CommissionProfile.SecurityQuestion = user.SecurityQuestion;
                    CommissionProfile.PK_UserProfileID = masterProfile.PK_UserProfileID;
                    CommissionProfile.Title = masterProfile.CN_Title;
                    CommissionProfile.profilestatus = masterProfile.Master_User.FK_Status;
                    CommissionProfile.termsCondtnCheck = true;
                    CommissionProfile.codeOfconduct = true;
                    List<SocialMediaRecord> SocialMediaRecords = new List<SocialMediaRecord>();
                    SocialMediaRecords = (from SocialMediaRecord in user.Details_UserSocialMedia
                                          where SocialMediaRecord.DelFlag == false
                                          select new SocialMediaRecord
                                          {

                                              FK_SocialMediaID = SocialMediaRecord.FK_SocialMediaID,
                                              PK_UserSocialMediaID = SocialMediaRecord.PK_UserSocialMediaID,
                                              SocialMedia = SocialMediaRecord.SocialMedia,
                                              URL = SocialMediaRecord.URL,
                                              IconPath = SocialMediaRecord.Lookup_SocialMediaPlatform.IconPath,
                                          }).ToList();

                    CommissionProfile.SocialMediaRecords = SocialMediaRecords;

                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = CommissionProfile;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;
            }

            return Ok(oResponse);
        }


        [HttpPost]
        [Route("api/Profile/CreateFan")]
        public IHttpActionResult CreateFan(FanInfo CreateFanInfo)
        {
            ReturnResponse oResponse = new ReturnResponse();

            using (var transaction = dbContext.Database.BeginTransaction())
            {
                try
                {

                    Master_User oUser = new Master_User();

                    //Fill custom data
                    CreateFanInfo.CreatedDate = DateTime.Today;

                    //Adding user data               
                    oUser.EmailAddress = CreateFanInfo.EmailAddress;
                    oUser.Password = FRICrypto.EncryptString(CreateFanInfo.Password, "FRI@AOT");
                    oUser.SecurityQuestion = CreateFanInfo.SecurityQuestion;
                    oUser.SecurityAnswer = CreateFanInfo.SecurityAnswer;
                    oUser.CreatedDate = DateTime.Now;
                    oUser.LegalFirstName = CreateFanInfo.LegalFirstName;
                    oUser.LegalMiddleName = CreateFanInfo.LegalMiddleName;
                    oUser.LastName = CreateFanInfo.LastName;

                    oUser.Phone = CreateFanInfo.Phone;
                    oUser.FK_Country = CreateFanInfo.FK_Country;
                    oUser.FK_ProvinceOrState = CreateFanInfo.FK_ProvinceOrState;
                    oUser.City = CreateFanInfo.City;
                    oUser.Street = CreateFanInfo.Street;
                    oUser.PostalOrZipCode = CreateFanInfo.PostalOrZipCode;
                    oUser.FK_Status = Constants.Active;

                    // Add created by.
                    dbContext.Master_User.Add(oUser);
                    dbContext.SaveChanges();
                    //Get userId
                    CreateFanInfo.PK_UserID = oUser.PK_UserID;



                    var CountryDetails = (from Country in dbContext.Lookup_Country
                                          select new { Country.Description, Country.PK_CountryID }).Where(W => W.PK_CountryID == CreateFanInfo.FK_Country).FirstOrDefault();

                    if (CountryDetails != null)
                        oUser.CountryName = CountryDetails.Description;


                    var StateDetails = (from State in dbContext.Lookup_StateOrProvince
                                        select new { State.Description, State.PK_StateOrProvinceID }).Where(W => W.PK_StateOrProvinceID == CreateFanInfo.FK_ProvinceOrState).FirstOrDefault();

                    if (StateDetails != null)
                        oUser.StateName = StateDetails.Description;



                    Master_Profile oProfile = new Master_Profile();
                    oProfile.FK_UserID = CreateFanInfo.PK_UserID;
                    dbContext.Master_Profile.Add(oProfile);
                    dbContext.SaveChanges();


                    UpdateProfileAndRole(ref oUser, oUser.PK_UserID, (byte)ROLES.Fan, (byte)PROFILES.Fan);


                    dbContext.SaveChanges();

                    transaction.Commit();
                    oResponse.status = "200";
                    oResponse.status = "Success";
                    oResponse.result = CreateFanInfo.PK_UserID;

                    //  IHttpActionResult oResult = GetUserProfile(UserDemoGraphicInfo.PK_UserID);
                    // return oResult;
                }
                catch (Exception ex)
                {
                    oResponse.errors = "Service Error";
                    oResponse.status = "Exception";
                    oResponse.result = ex.Message;
                    transaction.Rollback();

                }

            }


            return Ok(oResponse);
        }

        public void UpdateProfileAndRole(ref Master_User oUser, int PK_UserID, byte RoleID, byte ProfileID)
        {
            try
            {


                var UserRoleMap = oUser.Map_User_Role.Where(x => x.FK_RoleID == (byte)ROLES.Fan).FirstOrDefault();
                if (UserRoleMap == null)
                {
                    Map_User_Role UserRole = new Map_User_Role
                    {
                        FK_RoleID = RoleID,
                        FK_UserID = PK_UserID,
                        CreatedDate = DateTime.Today,
                        FK_CreatedBy = PK_UserID,
                        DelFlag = false

                    };
                    oUser.Map_User_Role.Add(UserRole);
                }
                else
                {
                    oUser.Map_User_Role
                        .Where(p => p.FK_UserID == PK_UserID && p.FK_RoleID == (byte)RoleID)
                        .FirstOrDefault().DelFlag = false;
                }



                var UserProfileMap = oUser.Map_User_Profile.Where(x => x.FK_ProfileID == (byte)ROLES.Fan).FirstOrDefault();
                if (UserProfileMap == null)
                {
                    Map_User_Profile UserRole = new Map_User_Profile
                    {
                        FK_ProfileID = ProfileID,
                        FK_UserID = PK_UserID,
                        CreatedDate = DateTime.Today,
                        FK_CreatedBy = PK_UserID,
                        DelFlag = false
                    };

                    oUser.Map_User_Profile.Add(UserRole);
                }
                else
                {
                    oUser.Map_User_Profile
                        .Where(p => p.FK_UserID == PK_UserID && p.FK_ProfileID == (byte)RoleID)
                        .FirstOrDefault().DelFlag = false;
                }
            }
            catch (Exception ex)
            {
                return;

            }
        }




        [HttpPost]
        [Route("api/Profile/UpdateFan")]
        public IHttpActionResult UpdateFan(FanInfo UpdateFanInfo)
        {
            ReturnResponse oResponse = new ReturnResponse();
            using (var transaction = dbContext.Database.BeginTransaction())
            {
                try
                {

                    if (UpdateFanInfo == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data available";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    Master_User MasterUser = (from user in dbContext.Master_User
                                              where user.PK_UserID == UpdateFanInfo.PK_UserID && user.DelFlag == false
                                              select user).FirstOrDefault();
                    if (MasterUser == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data available.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    Master_Profile MasterProfile = (from userprofile in MasterUser.Master_Profile
                                                    where userprofile.FK_UserID == UpdateFanInfo.PK_UserID
                                                    select userprofile).FirstOrDefault();
                    if (MasterProfile == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }


                    //Update date              
                    MasterUser.EmailAddress = UpdateFanInfo.EmailAddress;
                    MasterUser.SecurityQuestion = UpdateFanInfo.SecurityQuestion;
                    MasterUser.SecurityAnswer = UpdateFanInfo.SecurityAnswer;

                    MasterUser.Phone = UpdateFanInfo.Phone;
                    MasterUser.FK_Country = UpdateFanInfo.FK_Country;
                    MasterUser.FK_ProvinceOrState = UpdateFanInfo.FK_ProvinceOrState;
                    MasterUser.City = UpdateFanInfo.City;
                    MasterUser.Street = UpdateFanInfo.Street;
                    MasterUser.PostalOrZipCode = UpdateFanInfo.PostalOrZipCode;
                    MasterUser.LegalFirstName = UpdateFanInfo.LegalFirstName;
                    MasterUser.LegalMiddleName = UpdateFanInfo.LegalMiddleName;
                    MasterUser.LastName = UpdateFanInfo.LastName;

                    var CountryDetails = (from Country in dbContext.Lookup_Country
                                          select new { Country.Description, Country.PK_CountryID }).Where(W => W.PK_CountryID == UpdateFanInfo.FK_Country).FirstOrDefault();

                    if (CountryDetails != null)
                        MasterUser.CountryName = CountryDetails.Description;


                    var StateDetails = (from State in dbContext.Lookup_StateOrProvince
                                        select new { State.Description, State.PK_StateOrProvinceID }).Where(W => W.PK_StateOrProvinceID == UpdateFanInfo.FK_ProvinceOrState).FirstOrDefault();

                    if (StateDetails != null)
                        MasterUser.StateName = StateDetails.Description;


                    MasterProfile.FK_UserID = UpdateFanInfo.PK_UserID;


                    UpdateProfileAndRole(ref MasterUser, UpdateFanInfo.PK_UserID, (byte)ROLES.Fan, (byte)PROFILES.Fan);

                    dbContext.SaveChanges();

                    transaction.Commit();



                    oResponse.status = "200";
                    oResponse.status = "Success";
                    oResponse.result = UpdateFanInfo.PK_UserID;

                    //  IHttpActionResult oResult = GetUserProfile(UserDemoGraphicInfo.PK_UserID);
                    // return oResult;
                }
                catch (Exception ex)
                {
                    oResponse.errors = "Service Error";
                    oResponse.status = "Exception";
                    oResponse.result = ex.Message;
                    transaction.Rollback();

                }

                return Ok(oResponse);
            }
        }


        [HttpGet]
        [Route("api/Profile/GetFanDetails/{id}")]
        public IHttpActionResult GetFanDetails(string id)
        {
            string data = FRICrypto.decryption(id);
            int userId = Convert.ToInt32(data);
            ReturnResponse oResponse = new ReturnResponse();
            var re = Request;
            var headers = re.Headers;
            string token = "";
            if (headers.Contains("Token"))
            {
                token = headers.GetValues("Token").First();
            }
            bool IsSuperadmin = new Common().IsSuperAdmin(token);


            try
            {
                FanInfo FanProfile = new FanInfo();

                var user = (from fighter in dbContext.Master_User
                            where fighter.PK_UserID == userId && fighter.DelFlag == false
                            select fighter).FirstOrDefault();
                if (user.FK_Status == 0 && IsSuperadmin == false)
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "Authorization has been denied for this request.";
                    oResponse.result = null;
                    return Ok(oResponse);
                }
                else
                {
                    if (user == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    var masterProfile = (from userprofile in dbContext.Master_Profile
                                         where userprofile.FK_UserID == userId
                                         select userprofile).FirstOrDefault();

                    if (masterProfile == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    FanProfile.EmailAddress = user.EmailAddress;

                    FanProfile.FK_Country = user.FK_Country;
                    FanProfile.FK_ProvinceOrState = user.FK_ProvinceOrState;
                    FanProfile.City = user.City;
                    FanProfile.Phone = user.Phone;
                    FanProfile.PK_UserID = user.PK_UserID;
                    FanProfile.PostalOrZipCode = user.PostalOrZipCode;
                    FanProfile.Street = user.Street;
                    FanProfile.LegalFirstName = user.LegalFirstName;
                    FanProfile.LegalMiddleName = user.LegalMiddleName;
                    FanProfile.LastName = user.LastName;
                    FanProfile.profilestatus = user.FK_Status;


                    FanProfile.SecurityAnswer = user.SecurityAnswer;
                    FanProfile.SecurityQuestion = user.SecurityQuestion;
                    FanProfile.PK_UserProfileID = masterProfile.PK_UserProfileID;

                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = FanProfile;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;
            }

            return Ok(oResponse);
        }



        [HttpGet]
        [Route("api/Profile/GetUserProfileList/{id}")]
        public IHttpActionResult GetUserProfileList(string id)
        {
            string data = FRICrypto.decryption(id);
            int userId = Convert.ToInt32(data);
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                //// fill profile list start
                List<UserProfile> oUserProfiles = new List<UserProfile>();

                oUserProfiles = (from Profilelist in dbContext.Map_User_Profile
                                     //join ProfileLookup in dbContext.Lookup_Profile on Profilelist.FK_ProfileID equals ProfileLookup.PK_ProfileID
                                 where Profilelist.FK_UserID == userId && Profilelist.DelFlag == false
                                 select new UserProfile
                                 {
                                     FK_ProfileID = Profilelist.FK_ProfileID,
                                     PK_ProfileMapID = Profilelist.PK_ProfileMapID
                                 }).ToList();



                oResponse.status = "Success";
                oResponse.errors = null;
                oResponse.result = oUserProfiles;
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }

        public String ConvertImageURLToBase64(String url)
        {
            StringBuilder _sb = new StringBuilder();

            Byte[] _byte = this.GetImage(url);

            _sb.Append(Convert.ToBase64String(_byte, 0, _byte.Length));

            return _sb.ToString();
        }
        private byte[] GetImage(string url)
        {
            Stream stream = null;
            byte[] buf;

            try
            {
                WebProxy myProxy = new WebProxy();
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
                stream = response.GetResponseStream();

                using (BinaryReader br = new BinaryReader(stream))
                {
                    int len = (int)(response.ContentLength);
                    buf = br.ReadBytes(len);
                    br.Close();
                }

                stream.Close();
                response.Close();
            }
            catch (Exception exp)
            {
                buf = null;
            }

            return (buf);
        }
        [HttpGet]
        [Route("api/Profile/GetUserProfile/{id}")]
        public IHttpActionResult GetUserProfile(int id)
        {
            ReturnResponse oResponse = new ReturnResponse();
            var re = Request;
            var headers = re.Headers;
            string token = "";
            if (headers.Contains("Token"))
            {
                token = headers.GetValues("Token").First();
            }
            bool IsSuperadmin = new Common().IsSuperAdmin(token);
            try
            {
                UserDetails UserDetails = new UserDetails();

                var MasterUser = (from user in dbContext.Master_User
                                  where user.PK_UserID == id && user.DelFlag == false
                                  select user).FirstOrDefault();
                if (MasterUser.FK_Status == 0 && IsSuperadmin == false)
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "Authorization has been denied for this request.";
                    oResponse.result = null;
                    return Ok(oResponse);
                }
                else
                {
                    if (MasterUser == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    var MasterProfile = (from userprofile in MasterUser.Master_Profile
                                         where userprofile.FK_UserID == id
                                         select userprofile).FirstOrDefault();
                    if (MasterUser == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data availabe.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    UserDetails.PK_UserID = MasterUser.PK_UserID;
                    UserDetails.PK_UserProfileID = MasterProfile.PK_UserProfileID;

                    //// fill profile list start
                    List<UserProfile> oUserProfiles = new List<UserProfile>();

                    oUserProfiles = (from Profilelist in MasterUser.Map_User_Profile
                                         //join ProfileLookup in dbContext.Lookup_Profile on Profilelist.FK_ProfileID equals ProfileLookup.PK_ProfileID
                                     where Profilelist.FK_UserID == id && Profilelist.DelFlag == false
                                     select new UserProfile
                                     {
                                         FK_ProfileID = Profilelist.FK_ProfileID,
                                         PK_ProfileMapID = Profilelist.PK_ProfileMapID
                                     }).ToList();

                    UserDetails.UserProfiles = oUserProfiles;
                    //// fill profile list end


                    //// fill demographic details start
                    DemoGraphicInfo oDemoGraphicInfo = new DemoGraphicInfo();
                    oDemoGraphicInfo.City = MasterUser.City;
                    //if (MasterUser.DOB != null)
                    //    oDemoGraphicInfo.DOB = MasterUser.DOB.Value;
                    oDemoGraphicInfo.EmailAddress = MasterUser.EmailAddress;
                    oDemoGraphicInfo.FK_Country = MasterUser.FK_Country;
                    oDemoGraphicInfo.FK_ProvinceOrState = MasterUser.FK_ProvinceOrState;
                    oDemoGraphicInfo.Gender = MasterUser.Gender;
                    oDemoGraphicInfo.IsAmbassador = MasterUser.IsAmbassador;

                    oDemoGraphicInfo.SecurityQuestion = MasterUser.SecurityQuestion;
                    oDemoGraphicInfo.SecurityAnswer = MasterUser.SecurityAnswer;
                    oDemoGraphicInfo.EmergencyContactName = MasterUser.EmergencyContactName;
                    oDemoGraphicInfo.EmergencyContactNumber = MasterUser.EmergencyContactNumber;

                    oDemoGraphicInfo.LastName = MasterUser.LastName;
                    oDemoGraphicInfo.LegalFirstName = MasterUser.LegalFirstName;
                    oDemoGraphicInfo.LegalMiddleName = MasterUser.LegalMiddleName;
                    //oDemoGraphicInfo.Password = MasterUser.Password;
                    oDemoGraphicInfo.PersonalWebsite = MasterUser.PersonalWebsite;
                    oDemoGraphicInfo.Phone = MasterUser.Phone;
                    oDemoGraphicInfo.PK_UserID = MasterUser.PK_UserID;
                    oDemoGraphicInfo.PostalOrZipCode = MasterUser.PostalOrZipCode;
                    if (MasterUser.ProfilePhotoPath != null)
                    {
                        oDemoGraphicInfo.ProfilePhotoPath = MasterUser.ProfilePhotoPath;
                        if (MasterUser.ProfilePhotoPath != "")
                            oDemoGraphicInfo.Base64Image = ConvertImageURLToBase64(MasterUser.ProfilePhotoPath);
                    }
                    oDemoGraphicInfo.Street = MasterUser.Street;
                    oDemoGraphicInfo.profilestatus = MasterUser.FK_Status;
                    UserDetails.DemoGraphicInfo = oDemoGraphicInfo;
                    List<SocialMediaRecord> SocialMediaRecords = new List<SocialMediaRecord>();
                    // Fill Social media start
                    //for (int iCount = 0; iCount < MasterUser.Details_UserSocialMedia.Count; iCount++)
                    //{
                    //    SocialMediaRecord oSocialMediaRecord = new SocialMediaRecord();
                    //    if (MasterUser.Details_UserSocialMedia.ElementAt(iCount).DelFlag == true)
                    //        continue;
                    //    oSocialMediaRecord.CreatedDate = MasterUser.Details_UserSocialMedia.ElementAt(iCount).CreatedDate;
                    //    oSocialMediaRecord.FK_UserID = MasterUser.Details_UserSocialMedia.ElementAt(iCount).FK_UserID;
                    //    oSocialMediaRecord.FK_SocialMediaID = MasterUser.Details_UserSocialMedia.ElementAt(iCount).FK_SocialMediaID;
                    //    oSocialMediaRecord.PK_UserSocialMediaID = MasterUser.Details_UserSocialMedia.ElementAt(iCount).PK_UserSocialMediaID;
                    //    oSocialMediaRecord.SocialMedia = MasterUser.Details_UserSocialMedia.ElementAt(iCount).SocialMedia;
                    //    oSocialMediaRecord.URL = MasterUser.Details_UserSocialMedia.ElementAt(iCount).URL;
                    //    oSocialMediaRecord.IconPath = MasterUser.Details_UserSocialMedia.ElementAt(iCount).Lookup_SocialMediaPlatform.IconPath;
                    //    SocialMediaRecords.Add(oSocialMediaRecord);

                    //}
                    //UserDetails.DemoGraphicInfo.SocialMediaRecords = SocialMediaRecords;

                    //// fill video list start

                    SocialMediaRecords = (from SocialMediaRecord in MasterUser.Details_UserSocialMedia
                                          where SocialMediaRecord.DelFlag == false
                                          select new SocialMediaRecord
                                          {

                                              FK_SocialMediaID = SocialMediaRecord.FK_SocialMediaID,
                                              PK_UserSocialMediaID = SocialMediaRecord.PK_UserSocialMediaID,
                                              SocialMedia = SocialMediaRecord.SocialMedia,
                                              URL = SocialMediaRecord.URL,
                                              IconPath = SocialMediaRecord.Lookup_SocialMediaPlatform.IconPath,
                                          }).ToList();

                    UserDetails.DemoGraphicInfo.SocialMediaRecords = SocialMediaRecords;
                    ////fill video list end

                    // Fill Social media end

                    for (int iCount = 0; iCount < UserDetails.UserProfiles.Count; iCount++)
                    {
                        switch (UserDetails.UserProfiles.ElementAt(iCount).FK_ProfileID)
                        {
                            case 1: //1 Fan                            
                                break;
                            case 2: //2   Fighter
                                FighterProfile oFighterProfile = new FighterProfile();
                                oFighterProfile = FillFighterProfile(MasterProfile, MasterUser);
                                UserDetails.FighterProfile = oFighterProfile;
                                break;
                            case 3: //3   Promoter
                                PromotorProfile oPromotorProfile = new PromotorProfile();
                                oPromotorProfile = FillPromotorProfile(MasterProfile);
                                UserDetails.PromotorProfile = oPromotorProfile;
                                break;
                            case 4: //4   Commission
                                CommissionProfile oCommissionProfile = new CommissionProfile();
                                oCommissionProfile = FilCommissionProfile(MasterProfile);
                                UserDetails.CommissionProfile = oCommissionProfile;
                                break;
                            case 5: //5   Sanctioning Body
                                SanctioningBodyProfile oSanctioningBodyProfile = new SanctioningBodyProfile();
                                oSanctioningBodyProfile = FillSanctioningBodyProfile(MasterProfile);
                                UserDetails.SanctioningBodyProfile = oSanctioningBodyProfile;
                                break;
                            case 6: //6   Sponsor
                                    // oFighterProfile = new FighterProfile();
                                    // oFighterProfile = FillFighterProfile(MasterProfile);
                                    // UserDetails.FighterProfile = oFighterProfile;
                                break;
                            case 7: //7   Gym Owner
                                break;
                            case 8: //8   Official
                                OfficialProfile oOfficialProfile = new OfficialProfile();
                                oOfficialProfile = FillOfficialProfile(MasterProfile);
                                UserDetails.OfficialProfile = oOfficialProfile;
                                break;

                        }


                    }




                    //// fill demographic details end


                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = UserDetails;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }

        private OfficialProfile FillOfficialProfile(Master_Profile masterProfile)
        {
            //// fill video list start

            OfficialProfile oOfficialProfile = new OfficialProfile();
            List<OfficialDetail> oOfficialProfiles = new List<OfficialDetail>();
            oOfficialProfiles = (from OfficialProfile in masterProfile.Details_Official
                                 where OfficialProfile.DelFlag == false
                                 select new OfficialDetail
                                 {
                                     PK_OfficialProfileID = OfficialProfile.PK_OfficialProfileID,
                                     FK_ProfileID = OfficialProfile.FK_ProfileID,
                                     CertID = OfficialProfile.CertID,
                                     Citizenship = OfficialProfile.Citizenship,
                                     Description = OfficialProfile.Description,
                                     DelFlag = OfficialProfile.DelFlag,
                                     Discipline = OfficialProfile.Discipline,
                                     EmergencyContactName = OfficialProfile.EmergencyContactName,
                                     Licence = OfficialProfile.Licence,
                                     OfficialLevel = OfficialProfile.OfficialLevel,
                                     TypeOfOfficial = OfficialProfile.TypeOfOfficial,
                                     NumberofFights = OfficialProfile.NumberofFights,
                                     LicensedBy = OfficialProfile.LicensedBy
                                 }).ToList();

            oOfficialProfile.PK_UserProfileID = masterProfile.PK_UserProfileID;
            oOfficialProfile.OfficialDetails = oOfficialProfiles;

            return oOfficialProfile;
            ////fill video list end
        }

        private SanctioningBodyProfile FillSanctioningBodyProfile(Master_Profile masterProfile)
        {
            return null;
        }

        private CommissionProfile FilCommissionProfile(Master_Profile masterProfile)
        {
            return null;
        }

        private PromotorProfile FillPromotorProfile(Master_Profile masterProfile)
        {
            PromotorProfile oPromotorProfile = new PromotorProfile();
            try
            {
                oPromotorProfile.PR_Address = masterProfile.PR_Address;
                oPromotorProfile.PR_Description = masterProfile.PR_Description;
                oPromotorProfile.PR_EventNameOrSeries = masterProfile.PR_EventNameOrSeries;
                oPromotorProfile.PR_Phone = masterProfile.PR_Phone;
                oPromotorProfile.PR_PriContactLastName = masterProfile.PR_PriContactLastName;
                oPromotorProfile.PR_PriContactLegalFirstName = masterProfile.PR_PriContactLegalFirstName;
                oPromotorProfile.PR_PriContactLegalMiddleName = masterProfile.PR_PriContactLegalMiddleName;
                oPromotorProfile.PR_SecContactLastName = masterProfile.PR_SecContactLastName;
                oPromotorProfile.PR_SecContactLegalFirstName = masterProfile.PR_SecContactLegalFirstName;
                oPromotorProfile.PR_SecContactLegalMiddleName = masterProfile.PR_SecContactLegalMiddleName;
                oPromotorProfile.PR_Title = masterProfile.PR_Title;
                oPromotorProfile.PR_Webaddress = masterProfile.PR_Webaddress;

            }
            catch (Exception ex)
            {
                oPromotorProfile = null;

            }
            return oPromotorProfile;
        }

        private FighterProfile FillFighterProfile(Master_Profile masterProfile, Master_User MasterUser)
        {
            FighterProfile oFighterProfile = new FighterProfile();
            try
            {
                oFighterProfile.CheerCount = MasterUser.CheerCount;
                oFighterProfile.Draws = MasterUser.Draws;
                oFighterProfile.FighterName = MasterUser.FighterName;
                oFighterProfile.IsFeaturedfighter = MasterUser.IsFeaturedfighter;
                oFighterProfile.IsFighter = true;
                oFighterProfile.KO = MasterUser.KO;
                oFighterProfile.Losses = MasterUser.Losses;
                oFighterProfile.TKO = MasterUser.TKO;
                oFighterProfile.TotalFights = MasterUser.TotalFights;
                oFighterProfile.Wins = MasterUser.Wins;
                if (MasterUser.DOB != null)
                    oFighterProfile.FR_DOB = MasterUser.DOB.Value;
                oFighterProfile.FR_Bio = masterProfile.FR_Bio;
                oFighterProfile.FR_Citizenship = masterProfile.FR_Citizenship;
                oFighterProfile.FR_CurrentWeight = masterProfile.FR_CurrentWeight;
                oFighterProfile.FR_CurrentWeightUnit = masterProfile.FR_CurrentWeightUnit;
                oFighterProfile.FR_EmergencyContactName = masterProfile.FR_EmergencyContactName;
                oFighterProfile.FR_EmergencyContactPhone = masterProfile.FR_EmergencyContactPhone;
                oFighterProfile.FR_EyeColor = masterProfile.FR_EyeColor;
                oFighterProfile.FR_FighterStatus = masterProfile.FR_FighterStatus;
                oFighterProfile.FR_HairColor = masterProfile.FR_HairColor;
                oFighterProfile.FR_Height = masterProfile.FR_Height;
                oFighterProfile.FR_HeightUnit = masterProfile.FR_HeightUnit;

                oFighterProfile.FR_HeightInFt = masterProfile.FR_HeightInFt;
                oFighterProfile.FR_HeightInInch = masterProfile.FR_HeightInInch;
                oFighterProfile.FR_HeightCM = masterProfile.FR_HeightCM;



                oFighterProfile.FR_IsAvailableMatch = masterProfile.FR_IsAvailableMatch;
                oFighterProfile.FR_IsAvailableSparing = masterProfile.FR_IsAvailableSparing;
                oFighterProfile.FR_IsOwner = masterProfile.FR_IsOwner;
                //  if (masterProfile.FR_IsOwner)
                {
                    oFighterProfile.GO_City = masterProfile.GO_City;
                    oFighterProfile.GO_Facebook = masterProfile.GO_Facebook;
                    oFighterProfile.GO_FK_Country = masterProfile.GO_FK_Country;
                    oFighterProfile.GO_FK_ProvinceOrState = masterProfile.GO_FK_ProvinceOrState;
                    oFighterProfile.GO_Phone = masterProfile.GO_Phone;
                    oFighterProfile.GO_PostalOrZipCode = masterProfile.GO_PostalOrZipCode;
                    oFighterProfile.GO_Street = masterProfile.GO_Street;
                    oFighterProfile.GO_Website = masterProfile.GO_Website;
                    oFighterProfile.GO_TrainerName = masterProfile.GO_TrainerName;
                    oFighterProfile.GO_TrainingFacilityName = masterProfile.GO_TrainingFacilityName;

                }

                oFighterProfile.FR_IsProfessional = masterProfile.FR_IsProfessional;
                oFighterProfile.FR_MaxWeight = masterProfile.FR_MaxWeight;
                oFighterProfile.FR_MaxWeightUnit = masterProfile.FR_MaxWeightUnit;
                oFighterProfile.FR_MinWeight = masterProfile.FR_MinWeight;
                oFighterProfile.FR_MinWeightUnit = masterProfile.FR_MinWeightUnit;
                oFighterProfile.FR_PrimaryDiscipline = masterProfile.FR_PrimaryDiscipline;
                oFighterProfile.FR_PrimarySkillLevel = masterProfile.FR_PrimarySkillLevel;
                oFighterProfile.FR_Reach = masterProfile.FR_Reach;
                oFighterProfile.FR_ReachUnit = masterProfile.FR_ReachUnit;
                oFighterProfile.FR_SecondaryDiscipline = masterProfile.FR_SecondaryDiscipline;
                oFighterProfile.FR_SecondarySkillLevel = masterProfile.FR_SecondarySkillLevel;
                oFighterProfile.FR_TertiaryDiscipline = masterProfile.FR_TertiaryDiscipline;
                oFighterProfile.FR_TertiarySkillLevel = masterProfile.FR_TertiarySkillLevel;
                oFighterProfile.FR_LegReach = masterProfile.FR_LegReach;
                oFighterProfile.FR_LegReachUnit = masterProfile.FR_LegReachUnit;
                //// fill video list start

                List<FighterVideo> oFighterVideos = new List<FighterVideo>();
                oFighterVideos = (from FighterVideo in masterProfile.Details_FighterVideo
                                  where FighterVideo.DelFlag == false
                                  select new FighterVideo
                                  {
                                      PK_FighterVideoID = FighterVideo.PK_FighterVideoID,
                                      VideoLink = FighterVideo.VideoLink,
                                      VideoDescription = FighterVideo.VideoDescription
                                  }).ToList();

                oFighterProfile.FighterVideoDetails = oFighterVideos;
                ////fill video list end


                //// fill video list start
                List<FighterPhoto> oFighterPhotos = new List<FighterPhoto>();
                oFighterPhotos = (from FighterPhoto in masterProfile.Details_FighterPhoto
                                  where FighterPhoto.DelFlag == false
                                  select new FighterPhoto
                                  {
                                      PK_FighterPhotoID = FighterPhoto.PK_FighterPhotoID,
                                      PhotoPath = FighterPhoto.PhotoPath
                                  }).ToList();

                oFighterProfile.FighterPhotoDetails = oFighterPhotos;
                ////fill video list end



                //// fill video list start
                List<FighterHistoryRecord> oFighterHistoryRecords = new List<FighterHistoryRecord>();
                oFighterHistoryRecords = (from FighterHistory in masterProfile.Details_FighterHistory
                                          where FighterHistory.DelFlag == false
                                          select new FighterHistoryRecord
                                          {
                                              Date = FighterHistory.Date,
                                              AmateurOrProfessional = FighterHistory.AmateurOrProfessional,
                                              Decision = FighterHistory.Decision,
                                              Event = FighterHistory.Event,
                                              OpponentGym = FighterHistory.OpponentGym,
                                              OpponentName = FighterHistory.OpponentName,
                                              PK_HistoryID = FighterHistory.PK_HistoryID,
                                              Result = FighterHistory.Result,
                                              ScheduledRounds = FighterHistory.ScheduledRounds,
                                              StoppageRound = FighterHistory.StoppageRound,
                                              StyleOfFight = FighterHistory.StyleOfFight,
                                              TitleWon = FighterHistory.TitleWon,
                                              Venue = FighterHistory.Venue,
                                              WeightCategory = FighterHistory.WeightCategory

                                          }).ToList();

                oFighterProfile.FighterHistoryRecords = oFighterHistoryRecords;
                ////fill video list end



            }
            catch (Exception ex)
            {
                oFighterProfile = null;

            }
            return oFighterProfile;


        }
    }
}