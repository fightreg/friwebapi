﻿using FRIWebAPI.App_Start;
using FRIWebAPI.ClassFiles;
using FRIWebAPI.Database;
using FRIWebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using static FRIWebAPI.ClassFiles.Common;

namespace FRIWebAPI.Controllers
{
    public class CommonController : ApiController
    {
        FRIEntities dbContext = null;
        Mail omail = new Mail();
        Common ocommon = new Common();
        public CommonController()
        {
            dbContext = new FRIEntities();
        }

        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        [HttpPost]
        [Route("api/Common/GetFighterSearchResult")]
        public IHttpActionResult GetFighterSearchResult(FighterSearch oFighterSearch)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                DBAccess oDB = new DBAccess();

                String strSearchCondition = null;

                if (oFighterSearch.AcceptMatch == 1)
                    strSearchCondition += " AND FR_IsAvailableMatch = 1";
                else if (oFighterSearch.AcceptMatch == 0)
                    strSearchCondition += " AND FR_IsAvailableMatch = 0";
               

                if (oFighterSearch.AcceptsSparingPartner == 1)
                    strSearchCondition += " AND FR_IsAvailableSparing = 1";
                else if (oFighterSearch.AcceptsSparingPartner == 0)
                    strSearchCondition += " AND FR_IsAvailableSparing = 0";
              

                if (oFighterSearch.Discipline != null && oFighterSearch.Discipline != "" && oFighterSearch.Discipline != " ")
                    strSearchCondition = strSearchCondition + " AND (FR_PrimaryDiscipline = '" + oFighterSearch.Discipline + "' OR FR_SecondaryDiscipline ='" + oFighterSearch.Discipline + "' OR  FR_TertiaryDiscipline = '" + oFighterSearch.Discipline + "')";

                if (oFighterSearch.SkillLevel != null && oFighterSearch.SkillLevel != "" && oFighterSearch.SkillLevel != " ")
                    strSearchCondition = strSearchCondition + " AND (FR_TertiarySkillLevel = '" + oFighterSearch.SkillLevel + "' OR FR_PrimarySkillLevel ='" + oFighterSearch.SkillLevel + "' OR  FR_SecondarySkillLevel = '" + oFighterSearch.SkillLevel + "')";


                if (oFighterSearch.FighterName != null && oFighterSearch.FighterName != "" && oFighterSearch.FighterName != " ")
                strSearchCondition += " AND (MU.LegalFirstName like '%" + oFighterSearch.FighterName + "%' OR " + "MU.LastName like '%" + oFighterSearch.FighterName + "%' OR " + "MU.FighterName like '%" + oFighterSearch.FighterName + "%' OR " + "concat(MU.LegalFirstName, ' ' , MU.LastName) LIKE '%" + oFighterSearch.FighterName + "%' OR " + "concat(MU.LegalFirstName, ' ' ,MU.LegalMiddleName,' ',MU.LastName) LIKE '%" + oFighterSearch.FighterName + "%' ) ";

                if (oFighterSearch.Gender != null && oFighterSearch.Gender != "" && oFighterSearch.Gender != " ")
                    strSearchCondition += " AND Gender = '" + oFighterSearch.Gender + "'";

                //if (oFighterSearch.Location != null && oFighterSearch.Location != "" && oFighterSearch.Location != " ")
                //  strSearchCondition += " AND (City like '%" + oFighterSearch.Location + "%' OR LC.Description like '%" + oFighterSearch.Location + "%' OR LS.Description like '%" + oFighterSearch.Location + "%')";

                if (oFighterSearch.City != null && oFighterSearch.City != "" && oFighterSearch.City != " ")
                    strSearchCondition += " AND City like '%" + oFighterSearch.City + "%'";

                if (oFighterSearch.State != null && oFighterSearch.State != "" && oFighterSearch.State != " ")
                    strSearchCondition += " AND FK_ProvinceOrState = " + oFighterSearch.State + " ";

                if (oFighterSearch.Country != null && oFighterSearch.Country != "" && oFighterSearch.Country != " ")
                    strSearchCondition += " AND FK_Country = " + oFighterSearch.Country + " ";

                if (oFighterSearch.HeightFrom!=null&& oFighterSearch.HeightTo != null&&Convert.ToInt32(oFighterSearch.HeightFrom) > 0 && Convert.ToInt32(oFighterSearch.HeightTo) > 0 && oFighterSearch.HeightUnit == "Ft")
                    strSearchCondition += " AND (FR_HeightInFt  >= " + oFighterSearch.HeightFrom.ToString() + " and FR_HeightInFt <= " + oFighterSearch.HeightTo.ToString() + "  )";

                else if (oFighterSearch.HeightFrom != null && oFighterSearch.HeightTo != null && Convert.ToInt32(oFighterSearch.HeightFrom) > 0 && Convert.ToInt32(oFighterSearch.HeightTo) > 0&& oFighterSearch.HeightUnit == "Centimeters")
                    strSearchCondition += " AND (FR_HeightCM  >= " + oFighterSearch.HeightFrom.ToString() + " and FR_HeightCM <= " + oFighterSearch.HeightTo.ToString() + "  )";

                //else if (oFighterSearch.HeightFrom > 0 && oFighterSearch.HeightTo > 0 && (oFighterSearch.HeightUnit == "Any" || oFighterSearch.HeightUnit == ""))
                //    strSearchCondition += " AND ((FR_HeightCM  >= " + oFighterSearch.HeightFrom.ToString() + " and FR_HeightCM <= " + oFighterSearch.HeightTo.ToString() + "  )" +
                //         " OR (FR_HeightInFt  >= " + oFighterSearch.HeightFrom.ToString() + " and FR_HeightInFt <= " + oFighterSearch.HeightTo.ToString() + " ) )";

                //if (oFighterSearch.WeightFrom > 0 && oFighterSearch.WeightTo > 0 && oFighterSearch.WeightUnit == "Kgs")
                //    strSearchCondition += " AND ((cast(FR_CurrentWeight as int)  >= " + oFighterSearch.WeightFrom.ToString() + " and cast(FR_CurrentWeight as int) <= " + oFighterSearch.WeightTo.ToString() + " AND FR_CurrentWeightUnit = 'Kgs' )  OR " +
                //        "(cast(FR_CurrentWeight as int) / 2.20462 >= " + oFighterSearch.WeightFrom.ToString() + " and cast(FR_CurrentWeight as int) <=  / 2.20462" + oFighterSearch.WeightTo.ToString() + " AND FR_CurrentWeightUnit = 'Lbs' ) )";

                //if (oFighterSearch.WeightFrom > 0 && oFighterSearch.WeightTo > 0 && oFighterSearch.WeightUnit == "Lbs")
                //    strSearchCondition += " AND ((cast(FR_CurrentWeight as int) *2.20462  >= " + oFighterSearch.WeightFrom.ToString() + " and cast(FR_CurrentWeight as int) *2.20462 <= " + oFighterSearch.WeightTo.ToString() + " AND FR_CurrentWeightUnit = 'Lbs' )  OR " +
                //        "(cast(FR_CurrentWeight as int)  >= " + oFighterSearch.WeightFrom.ToString() + " and cast(FR_CurrentWeight as int) <= " + oFighterSearch.WeightTo.ToString() + " AND FR_CurrentWeightUnit = 'Kgs' ) )";
                if (oFighterSearch.WeightFrom!=null&& oFighterSearch.WeightTo!=null&&Convert.ToInt32(oFighterSearch.WeightFrom )> 0 && Convert.ToInt32(oFighterSearch.WeightTo) > 0 && oFighterSearch.WeightUnit == "Kgs")
                    strSearchCondition += " AND ((cast(FR_CurrentWeightInKgs as float)  >= " + oFighterSearch.WeightFrom.ToString() + " and cast(FR_CurrentWeightInKgs as float) <= " + oFighterSearch.WeightTo.ToString() + "  ))";

                if (oFighterSearch.WeightFrom != null && oFighterSearch.WeightTo != null && Convert.ToInt32(oFighterSearch.WeightFrom) > 0 && Convert.ToInt32(oFighterSearch.WeightTo )> 0 && oFighterSearch.WeightUnit == "Lbs")
                    strSearchCondition += " AND ((cast(FR_CurrentWeightinLbs as float)  >= " + oFighterSearch.WeightFrom.ToString() + " and cast(FR_CurrentWeightinLbs as float) <= " + oFighterSearch.WeightTo.ToString() + "  ))";

                string SearchResult = oDB.SearchData(oFighterSearch.IsSuperadmin, 2, strSearchCondition);



                if (SearchResult != null)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = SearchResult;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }

        [HttpPost]
        [Route("api/Common/GetOfficialSearchResult")]
        public IHttpActionResult GetOfficialSearchResult(OfficialSearch oOfficialSearch)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                DBAccess oDB = new DBAccess();

                String strSearchCondition = null;



                if (oOfficialSearch.City != null && oOfficialSearch.City != "" && oOfficialSearch.City != " ")
                    strSearchCondition += " AND City like '%" + oOfficialSearch.City + "%'";


                if (oOfficialSearch.Discipline != null && oOfficialSearch.Discipline != "" && oOfficialSearch.Discipline != " ")
                    strSearchCondition += " AND Discipline like '%" + oOfficialSearch.Discipline + "%'";

                if (oOfficialSearch.OfficialLevel != null && oOfficialSearch.OfficialLevel != "" && oOfficialSearch.OfficialLevel != " ")
                    strSearchCondition += " AND OfficialLevel like '%" + oOfficialSearch.OfficialLevel + "%'";

                if (oOfficialSearch.TypeOfOfficial != null && oOfficialSearch.TypeOfOfficial != "" && oOfficialSearch.TypeOfOfficial != " ")
                    strSearchCondition += " AND TypeOfOfficial like '%" + oOfficialSearch.TypeOfOfficial + "%'";

                if (oOfficialSearch.City != null && oOfficialSearch.City != "" && oOfficialSearch.City != " ")
                    strSearchCondition += " AND City like '%" + oOfficialSearch.City + "%'";

                if (oOfficialSearch.State != null && oOfficialSearch.State != "" && oOfficialSearch.State != " ")
                    strSearchCondition += " AND FK_ProvinceOrState = " + oOfficialSearch.State + " ";

                if (oOfficialSearch.Country != null && oOfficialSearch.Country != "" && oOfficialSearch.Country != " ")
                    strSearchCondition += " AND FK_Country = " + oOfficialSearch.Country + " ";

                string SearchResult = oDB.SearchData(oOfficialSearch.IsSuperadmin, 3, strSearchCondition);



                if (SearchResult != null)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = SearchResult;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }

        [HttpPost]
        [Route("api/Common/GetPromoterSearchResult")]
        public IHttpActionResult GetPromoterSearchResult(PromoterSearch oPromoterSearch)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                DBAccess oDB = new DBAccess();

                String strSearchCondition = null;



                //if (oPromoterSearch.City != null && oPromoterSearch.City != "" && oPromoterSearch.City != " ")
                //    strSearchCondition += " AND City like '%" + oPromoterSearch.City + "%'";


                if (oPromoterSearch.EventName != null && oPromoterSearch.EventName != "" && oPromoterSearch.EventName != " ")
                    strSearchCondition += " AND PR_EventNameOrSeries like '%" + oPromoterSearch.EventName + "%'";

                if (oPromoterSearch.Title != null && oPromoterSearch.Title != "" && oPromoterSearch.Title != " ")
                    strSearchCondition += " AND PR_Title like '%" + oPromoterSearch.Title + "%'";

                if(oPromoterSearch.ContactName != null && oPromoterSearch.ContactName != "" && oPromoterSearch.ContactName != " ")
                    strSearchCondition += " AND (PR_PriContactLegalFirstName like '%" + oPromoterSearch.ContactName + "%' OR " + "PR_PriContactLegalMiddleName like '%" + oPromoterSearch.ContactName + "%' OR " + "PR_PriContactLastName like '%" + oPromoterSearch.ContactName + "%' OR " + "concat(PR_PriContactLegalFirstName, ' ' , PR_PriContactLastName) like '%" + oPromoterSearch.ContactName + "%'OR " + "concat(PR_PriContactLegalFirstName, ' ' ,PR_PriContactLegalMiddleName,' ',PR_PriContactLastName) like '%" + oPromoterSearch.ContactName + "%' ) ";

                if (oPromoterSearch.City != null && oPromoterSearch.City != "" && oPromoterSearch.City != " ")
                    strSearchCondition += " AND City like '%" + oPromoterSearch.City + "%'";

                if (oPromoterSearch.State != null && oPromoterSearch.State != "" && oPromoterSearch.State != " ")
                    strSearchCondition += " AND FK_ProvinceOrState = " + oPromoterSearch.State + " ";

                if (oPromoterSearch.Country != null && oPromoterSearch.Country != "" && oPromoterSearch.Country != " ")
                    strSearchCondition += " AND FK_Country = " + oPromoterSearch.Country + " ";

                string SearchResult = oDB.SearchData(oPromoterSearch.IsSuperadmin, 4, strSearchCondition);



                if (SearchResult != null)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = SearchResult;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }

        [HttpPost]
        [Route("api/Common/GetAmbassadorSearchResult")]
        public IHttpActionResult GetAmbassadorSearchResult(AmbassadorSearch oAmbassadorSearch)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                DBAccess oDB = new DBAccess();

                String strSearchCondition = null;


                if (oAmbassadorSearch.Name != null && oAmbassadorSearch.Name != "" && oAmbassadorSearch.Name != " ")
                    strSearchCondition += " AND (  MU.LegalFirstName like '%" + oAmbassadorSearch.Name + "%' OR " + "MU.LegalMiddleName like '%" + oAmbassadorSearch.Name + "%' OR " + "MU.LastName like '%" + oAmbassadorSearch.Name + "%' ) ";


                if (oAmbassadorSearch.City != null && oAmbassadorSearch.City != "" && oAmbassadorSearch.City != " ")
                    strSearchCondition += " AND City like '%" + oAmbassadorSearch.City + "%'";

                if (oAmbassadorSearch.State != null && oAmbassadorSearch.State != "" && oAmbassadorSearch.State != " ")
                    strSearchCondition += " AND FK_ProvinceOrState = " + oAmbassadorSearch.State + " ";

                if (oAmbassadorSearch.Country != null && oAmbassadorSearch.Country != "" && oAmbassadorSearch.Country != " ")
                    strSearchCondition += " AND FK_Country = " + oAmbassadorSearch.Country + " ";

                if (oAmbassadorSearch.Gender != null && oAmbassadorSearch.Gender != "" && oAmbassadorSearch.Gender != " ")
                    strSearchCondition += " AND Gender = '" + oAmbassadorSearch.Gender + "'";



                string SearchResult = oDB.SearchData(oAmbassadorSearch.IsSuperadmin, 7, strSearchCondition);



                if (SearchResult != null)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = SearchResult;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }

        [HttpPost]
        [Route("api/Common/GetFanSearchResult")]
        public IHttpActionResult GetFanSearchResult(AmbassadorSearch oFanSearch)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                DBAccess oDB = new DBAccess();

                String strSearchCondition = null;


                if (oFanSearch.Name != null && oFanSearch.Name != "" && oFanSearch.Name != " ")
                    strSearchCondition += " AND (  MU.LegalFirstName like '%" + oFanSearch.Name + "%' OR " + "MU.LegalMiddleName like '%" + oFanSearch.Name + "%' OR " + "MU.LastName like '%" + oFanSearch.Name + "%' ) ";


                if(oFanSearch.City != null && oFanSearch.City != "" && oFanSearch.City != " ")
                    strSearchCondition += " AND City like '%" + oFanSearch.City + "%'";

                if (oFanSearch.State != null && oFanSearch.State != "" && oFanSearch.State != " ")
                    strSearchCondition += " AND FK_ProvinceOrState = " + oFanSearch.State + " ";

                if (oFanSearch.Country != null && oFanSearch.Country != "" && oFanSearch.Country != " ")
                    strSearchCondition += " AND FK_Country = " + oFanSearch.Country + " ";

                if (oFanSearch.State != null && oFanSearch.State != "" && oFanSearch.State != " ")
                    strSearchCondition += " AND LS.Description = '" + oFanSearch.State + "'";

                if (oFanSearch.Gender != null && oFanSearch.Gender != "" && oFanSearch.Gender != " ")
                    strSearchCondition += " AND Gender = '" + oFanSearch.Gender + "'";



                string SearchResult = oDB.SearchData(oFanSearch.IsSuperadmin, 10, strSearchCondition);



                if (SearchResult != null)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = SearchResult;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }

        [HttpPost]
        [Route("api/Common/GetGymownerSearchResult")]
        public IHttpActionResult GetGymownerSearchResult(AmbassadorSearch oGymownerSearch)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                DBAccess oDB = new DBAccess();

                String strSearchCondition = null;


                if (oGymownerSearch.Name != null && oGymownerSearch.Name != "" && oGymownerSearch.Name != " ")
                    strSearchCondition += " AND (  MU.LegalFirstName like '%" + oGymownerSearch.Name + "%' OR " + "MU.LegalMiddleName like '%" + oGymownerSearch.Name + "%' OR " + "MU.LastName like '%" + oGymownerSearch.Name + "%' ) ";


                if (oGymownerSearch.City != null && oGymownerSearch.City != "" && oGymownerSearch.City != " ")
                    strSearchCondition += " AND City like '%" + oGymownerSearch.City + "%'";


                if (oGymownerSearch.State != null && oGymownerSearch.State != "" && oGymownerSearch.State != " ")
                    strSearchCondition += " AND FK_ProvinceOrState = " + oGymownerSearch.State + " ";

                if (oGymownerSearch.Country != null && oGymownerSearch.Country != "" && oGymownerSearch.Country != " ")
                    strSearchCondition += " AND FK_Country = " + oGymownerSearch.Country + " ";

                if (oGymownerSearch.Gender != null && oGymownerSearch.Gender != "" && oGymownerSearch.Gender != " ")
                    strSearchCondition += " AND Gender = '" + oGymownerSearch.Gender + "'";



                string SearchResult = oDB.SearchData(oGymownerSearch.IsSuperadmin, 9, strSearchCondition);



                if (SearchResult != null)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = SearchResult;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }

        [HttpPost]
        [Route("api/Common/GetCommissionSearchResult")]
        public IHttpActionResult GetCommissionSearchResult(CommissionSearch oCommissionSearch)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                DBAccess oDB = new DBAccess();

                String strSearchCondition = null;

              
                if (oCommissionSearch.ContactName != null && oCommissionSearch.ContactName != "" && oCommissionSearch.ContactName != " ")
                    strSearchCondition += " AND (MP.CN_PriContactLegalFirstName like '%" + oCommissionSearch.ContactName + "%' OR " + "MP.CN_PriContactLegalMiddleName like '%" + oCommissionSearch.ContactName + "%' OR " + "MP.CN_PriContactLastName like '%" + oCommissionSearch.ContactName + "%' OR " + "concat(MP.CN_PriContactLegalFirstName, ' ' , MP.CN_PriContactLastName) like '%" + oCommissionSearch.ContactName + "%'OR " + "concat(MP.CN_PriContactLegalFirstName, ' ' ,MP.CN_PriContactLegalMiddleName,' ',MP.CN_PriContactLastName) like '%" + oCommissionSearch.ContactName + "%' ) ";


                if (oCommissionSearch.City != null && oCommissionSearch.City != "" && oCommissionSearch.City != " ")
                    strSearchCondition += " AND City like '%" + oCommissionSearch.City + "%'";

                if (oCommissionSearch.State != null && oCommissionSearch.State != "" && oCommissionSearch.State != " ")
                    strSearchCondition += " AND FK_ProvinceOrState = " + oCommissionSearch.State + " ";

                if (oCommissionSearch.Country != null && oCommissionSearch.Country != "" && oCommissionSearch.Country != " ")
                    strSearchCondition += " AND FK_Country = " + oCommissionSearch.Country + " ";

                if (oCommissionSearch.Title != null && oCommissionSearch.Title != "" && oCommissionSearch.Title != " ")
                    strSearchCondition += " AND MP.CN_Title like '%" + oCommissionSearch.Title + "%'";

                if (oCommissionSearch.PrimaryNumber != null && oCommissionSearch.PrimaryNumber != "" && oCommissionSearch.PrimaryNumber != " ")
                    strSearchCondition += " AND MP.CN_Phone like '%" + oCommissionSearch.PrimaryNumber + "%'";
                //if (oCommissionSearch.webaddress != null && oCommissionSearch.webaddress != "" && oCommissionSearch.webaddress != " ")
                //    strSearchCondition += " AND  MP.CN_webaddress like '%" + oCommissionSearch.webaddress + "%'";

                string SearchResult = oDB.SearchData(oCommissionSearch.IsSuperadmin, 6, strSearchCondition);



                if (SearchResult != null)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = SearchResult;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }

        [HttpPost]
        [Route("api/Common/GetSanctioningBodySearchResult")]
        public IHttpActionResult GetSanctioningBodySearchResult(SactionBodySearch oSanctioningBodySearch)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                DBAccess oDB = new DBAccess();

                String strSearchCondition = null;


                if (oSanctioningBodySearch.ContactName != null && oSanctioningBodySearch.ContactName != "" && oSanctioningBodySearch.ContactName != " ")
                    strSearchCondition += " AND (MP.SB_PriContactLegalFirstName like '%" + oSanctioningBodySearch.ContactName + "%' OR " + "MP.SB_PriContactLegalMiddleName like '%" + oSanctioningBodySearch.ContactName + "%' OR " + "MP.SB_PriContactLastName like '%" + oSanctioningBodySearch.ContactName + "%' OR " + "concat(MP.SB_PriContactLegalFirstName, ' ' , MP.SB_PriContactLastName) like '%" + oSanctioningBodySearch.ContactName + "%'OR " + "concat(MP.SB_PriContactLegalFirstName, ' ' ,MP.SB_PriContactLegalMiddleName,' ',MP.SB_PriContactLastName) like '%" + oSanctioningBodySearch.ContactName + "%' ) ";

                if (oSanctioningBodySearch.City != null && oSanctioningBodySearch.City != "" && oSanctioningBodySearch.City != " ")
                    strSearchCondition += " AND City like '%" + oSanctioningBodySearch.City + "%'";

                if (oSanctioningBodySearch.State != null && oSanctioningBodySearch.State != "" && oSanctioningBodySearch.State != " ")
                    strSearchCondition += " AND FK_ProvinceOrState = " + oSanctioningBodySearch.State + " ";

                if (oSanctioningBodySearch.Country != null && oSanctioningBodySearch.Country != "" && oSanctioningBodySearch.Country != " ")
                    strSearchCondition += " AND FK_Country = " + oSanctioningBodySearch.Country + " ";

                if (oSanctioningBodySearch.Title != null && oSanctioningBodySearch.Title != "" && oSanctioningBodySearch.Title != " ")
                    strSearchCondition += " AND MP.SB_Title like '%" + oSanctioningBodySearch.Title + "%'";

                //if (oSanctioningBodySearch.WebAddress != null && oSanctioningBodySearch.WebAddress != "" && oSanctioningBodySearch.WebAddress != " ")
                //    strSearchCondition += " AND SB_WebAddress like '%" + oSanctioningBodySearch.WebAddress + "%'";

                //if (oSanctioningBodySearch.Style != null && oSanctioningBodySearch.Style != "" && oSanctioningBodySearch.Style != " ")
                //    strSearchCondition += " AND SB_Style like '%" + oSanctioningBodySearch.Style + "%'";

                if (oSanctioningBodySearch.PrimaryNumber != null && oSanctioningBodySearch.PrimaryNumber != "" && oSanctioningBodySearch.PrimaryNumber != " ")
                    strSearchCondition += " AND MU.Phone like '%" + oSanctioningBodySearch.PrimaryNumber + "%'";



                string SearchResult = oDB.SearchData(oSanctioningBodySearch.IsSuperadmin, 5, strSearchCondition);



                if (SearchResult != null)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = SearchResult;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }

        [HttpPost]
        [Route("api/Common/ChangePassword")]
        public IHttpActionResult ChangePassword(ChangePassword ChangeUserPassword)
        {

            ReturnResponse oResponse = new ReturnResponse();

            try
            {
                if (ChangeUserPassword == null)
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data available";
                    oResponse.result = null;
                    return Ok(oResponse);
                }

                ChangeUserPassword.CurrentPassword = FRICrypto.EncryptString(ChangeUserPassword.CurrentPassword, "FRI@AOT");
                ChangeUserPassword.NewPassword = FRICrypto.EncryptString(ChangeUserPassword.NewPassword, "FRI@AOT");

                Master_User MasterUser = (from user in dbContext.Master_User
                                          where user.PK_UserID == ChangeUserPassword.PK_UserID && user.Password == ChangeUserPassword.CurrentPassword
                                          select user).FirstOrDefault();
                if (MasterUser == null)
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data available";
                    oResponse.result = null;
                    return Ok(oResponse);
                }

                MasterUser.Password = ChangeUserPassword.NewPassword;
                dbContext.SaveChanges();





                oResponse.status = "200";
                oResponse.status = "Success";
                oResponse.result = ChangeUserPassword.PK_UserID;

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);

        }
        [HttpPost]
        [Route("api/Common/ResetPassword")]
        public IHttpActionResult ResetPassword(ChangePassword ChangeUserPassword)
        {

            ReturnResponse oResponse = new ReturnResponse();

            try
            {
            bool Isrequested= ocommon.IsrequestedForForgotpasswd(ChangeUserPassword.PK_UserID);
                if (Isrequested)
                {
                    ChangeUserPassword.NewPassword = FRICrypto.EncryptString(ChangeUserPassword.NewPassword, "FRI@AOT");

                    Master_User MasterUser = (from user in dbContext.Master_User
                                              where user.PK_UserID == ChangeUserPassword.PK_UserID
                                              select user).FirstOrDefault();
                    if (MasterUser == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "No data available";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }

                    MasterUser.Password = ChangeUserPassword.NewPassword;
                    ocommon.UpdateForgotpasswordstatus(MasterUser.PK_UserID, false);
                    dbContext.SaveChanges();
                    oResponse.status = "200";
                    oResponse.status = "Success";
                    oResponse.result = ChangeUserPassword.PK_UserID;
                }
                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "Authorization has been denied for this request.";
                }

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);

        }
        [HttpPost]
        [Route("api/Common/ForgotPassword")]
        public IHttpActionResult ForgotPassword(ForgotPassword oForgotPassword)
        {
            ReturnResponse oResponse = new ReturnResponse();

            try
            {
                Master_User MasterUser = null;

                if (oForgotPassword.CheckQuestion == true)
                {
                    MasterUser = (from user in dbContext.Master_User
                                  where user.SecurityQuestion == oForgotPassword.SecurityQuestion && user.SecurityAnswer == oForgotPassword.QuestionAnswer && user.EmailAddress == oForgotPassword.EmailAddress
                                  &&user.FK_Status==Constants.Active
                                  select user).FirstOrDefault();

                    if (MasterUser == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "Security Question/Answer/Email is not correct.";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }
                    else
                    {
                        omail.SelectMailTemplate(MasterUser.PK_UserID, Constants.forgotpasswordtemplte, Constants.forgotpasswd,MasterUser.LegalFirstName);
                        ocommon.UpdateForgotpasswordstatus(MasterUser.PK_UserID, true);
                        oResponse.status = "200";
                        oResponse.status = "Success";
                        oResponse.errors = "Email sent";
                        return Ok(oResponse);
                    }
                }
                else
                {
                    MasterUser = (from user in dbContext.Master_User
                                  where user.EmailAddress == oForgotPassword.EmailAddress
                                  select user).FirstOrDefault();

                    if (MasterUser == null)
                    {
                        oResponse.status = "Failed";
                        oResponse.errors = "Email Address not found";
                        oResponse.result = null;
                        return Ok(oResponse);
                    }
                    else
                    {
                        oResponse.status = "200";
                        oResponse.status = "Success";
                        oResponse.errors = "Email sent";
                        return Ok(oResponse);
                    }
                }

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);

        }

        [HttpGet]
        [Route("api/Common/GetCheerDetails/{id}")]
        public IHttpActionResult GetCheerDetails(int id)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                List<CheeredUsers> oMasterCheer = new List<CheeredUsers>();

                oMasterCheer = (from Cheer in dbContext.Master_Cheer
                                where Cheer.DelFlag == false && Cheer.FK_CheerToID == id
                                select new CheeredUsers
                                {
                                    PK_MessageID = Cheer.FK_MessageID,
                                    FK_RecipientID = Cheer.FK_CheerToID,
                                    FK_SenderID = Cheer.FK_CheerFromID,
                                    Message = Cheer.Master_Messages.Message,
                                    SenderName = Cheer.Master_Messages.SenderName,
                                    RecipientName = Cheer.Master_Messages.RecipientName,
                                    Subject = Cheer.Master_Messages.Subject,
                                    ProfilePath = Cheer.Master_User.ProfilePhotoPath,
                                    MessageStatus = Cheer.Master_Messages.MessageStatus

                                }).ToList();

                if (oMasterCheer.Count >= 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = oMasterCheer;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }



        private string FormatDateTime(DateTime Date)
        {
            if (Date == null)
                return string.Empty;
            else
            {
                string time = string.Format("{0:hh:mm tt}", Date);
                string DateString = Date.ToString("dd MMMM yyyy");
                return DateString = time + ", " + DateString;
            }

        }
        private string ChallengeboardFormatDateTime(DateTime Date)
        {
            if (Date == null)
                return string.Empty;
            else
            {
                //string time = string.Format("{0:hh:mm tt}", Date);
                string DateString = Date.ToString("dd MMMM yyyy");
                return DateString =  DateString;
            }

        }
        [HttpGet]
        [Route("api/Common/GetChallengeBoard/{oFullTextSearch}/{id}")]
        public IHttpActionResult GetChallengeBoard(string oFullTextSearch, int id)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                List<ChallengeBoard> oChallenges = new List<ChallengeBoard>();

                var oChallengeBoard = (from ChallengeBoard in dbContext.Master_Challenge
                                       where ChallengeBoard.DelFlag == false && ChallengeBoard.FK_ChallengeStatusID != 2 orderby ChallengeBoard.ChallengeTime descending /* Rejected */
                                       select new ChallengeBoard
                                       {
                                           PK_MessageID = ChallengeBoard.FK_MessageID,
                                           FK_RecipientID = ChallengeBoard.FK_ChallengerToID,
                                           FK_SenderID = ChallengeBoard.FK_ChallengeFromID,
                                           MessageType = "challenge",

                                           SenderName = ChallengeBoard.Master_Messages.SenderName,
                                           RecipientName = ChallengeBoard.Master_Messages.RecipientName,
                                           ProfilePhotoSender = ChallengeBoard.Master_User.ProfilePhotoPath,
                                           ProfilePhotoRecipient = ChallengeBoard.Master_User1.ProfilePhotoPath,
                                           SendTime = ChallengeBoard.CreatedDate,
                                           AcceptedTime = ChallengeBoard.AcceptedTime

                                       }).ToList();



                var oSparRequest = (from SparRequest in dbContext.Master_Spar
                                    where SparRequest.DelFlag == false && SparRequest.FK_SparStatusID != 2 
                                    select new ChallengeBoard
                                    {
                                        PK_MessageID = SparRequest.FK_MessageID,
                                        FK_RecipientID = SparRequest.FK_SparToID,
                                        FK_SenderID = SparRequest.FK_SparFromID,
                                        MessageType = "spar",
                                        SenderName = SparRequest.Master_Messages.SenderName,
                                        RecipientName = SparRequest.Master_Messages.RecipientName,
                                        ProfilePhotoSender = SparRequest.Master_User.ProfilePhotoPath,
                                        ProfilePhotoRecipient = SparRequest.Master_User1.ProfilePhotoPath,
                                        SendTime = SparRequest.CreatedDate,
                                        AcceptedTime = SparRequest.AcceptedTime

                                    }).ToList();

                oChallenges = oChallengeBoard.Union(oSparRequest).ToList();

                foreach (ChallengeBoard oChallenge in oChallenges)
                {
                    oChallenge.SendTimeString = "Challenge date: " + ChallengeboardFormatDateTime(oChallenge.SendTime.Value);
                    oChallenge.AcceptedTimeString = oChallenge.AcceptedTime == null ? "" : " Accepted date: " + ChallengeboardFormatDateTime(oChallenge.AcceptedTime.Value);
                    if (oChallenge.MessageType == "challenge")
                    {


                        if (oChallenge.AcceptedTime == null)
                            oChallenge.Message = oChallenge.SenderName + " challenged " + oChallenge.RecipientName + " to a match and awaiting respose.";
                        else
                            oChallenge.Message = oChallenge.RecipientName + " accepted the challenge from " + oChallenge.SenderName + ".";



                        if (oChallenge.FK_SenderID == id)
                        {
                            if (oChallenge.AcceptedTime == null)
                                oChallenge.Message = "You challenged " + oChallenge.RecipientName + " to a match and awaiting respose.";
                            else
                                oChallenge.Message = oChallenge.RecipientName + " accepted your challenge.";

                        }
                        if (oChallenge.FK_RecipientID == id)
                        {
                            if (oChallenge.AcceptedTime == null)
                                oChallenge.Message = oChallenge.SenderName + " challenged you to a match and he is awaiting respose.";
                            else
                                oChallenge.Message = "You accepted the challenge from " + oChallenge.SenderName;

                        }
                    }

                    if (oChallenge.MessageType == "spar")
                    {
                        if (oChallenge.AcceptedTime == null)
                            oChallenge.Message = oChallenge.SenderName + " spared " + oChallenge.RecipientName + " to a match and awaiting respose.";
                        else
                            oChallenge.Message = oChallenge.RecipientName + " accepted the spar from " + oChallenge.SenderName + ".";

                        if (oChallenge.FK_SenderID == id)
                        {
                            if (oChallenge.AcceptedTime == null)
                                oChallenge.Message = "You spared " + oChallenge.RecipientName + " to a match and awaiting respose.";
                            else
                                oChallenge.Message = oChallenge.RecipientName + " accepted your spar.";

                        }
                        if (oChallenge.FK_RecipientID == id)
                        {
                            if (oChallenge.AcceptedTime == null)
                                oChallenge.Message = oChallenge.SenderName + " spared you to a match and he is awaiting respose.";
                            else
                                oChallenge.Message = "You accepted the spar from " + oChallenge.SenderName;

                        }
                    }

                }
                if(oFullTextSearch!=null&&oFullTextSearch!="all"&&oFullTextSearch!="undefined")
                {

                    oChallenges = oChallenges.Where(a => a.Message.ToUpper().Contains(oFullTextSearch.ToUpper())).ToList();
                }
                if (oChallengeBoard.Count >= 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = oChallenges;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }


        [HttpPost]
        [Route("api/Common/AddForum")]
        public IHttpActionResult AddForum(Master_Forum oForum)
        {
            ReturnResponse oResponse = new ReturnResponse();


            try
            {

                oForum.CreatedDate = DateTime.Now;
                oForum.Name = GetUserName(oForum.FK_UserID);
                oForum.FK_ForumTypeID = oForum.FK_ForumTypeID;
                dbContext.Master_Forum.Add(oForum);


                dbContext.SaveChanges();

                oResponse.status = "200";
                oResponse.status = "Success";
                oResponse.result = oForum.PK_ForumID;

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;


            }




            return Ok(oResponse);
        }
        [HttpPost]
        [Route("api/Common/UpdateForum")]
        public IHttpActionResult UpdateForum(Master_Forum oForum)
        {
            ReturnResponse oResponse = new ReturnResponse();


            try
            {

                var MasterForum = (from Forum in dbContext.Master_Forum
                                   where Forum.PK_ForumID == oForum.PK_ForumID
                                   select Forum).FirstOrDefault();

                if (MasterForum != null)
                {

                    MasterForum.ForumTopic = oForum.ForumTopic;
                    MasterForum.Forum = oForum.Forum;

                    dbContext.SaveChanges();

                    oResponse.status = "200";
                    oResponse.status = "Success";
                    oResponse.result = oForum.PK_ForumID;
                }
                else
                {
                    oResponse.errors = "No data found";
                    oResponse.status = "Error";
                }

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;


            }




            return Ok(oResponse);
        }
        [HttpPost]
        [Route("api/Common/DeleteForum")]
        public IHttpActionResult DeleteForum(Master_Forum oForum)
        {
            ReturnResponse oResponse = new ReturnResponse();


            try
            {


                var MasterForum = (from Forum in dbContext.Master_Forum
                                   where Forum.PK_ForumID == oForum.PK_ForumID
                                   select Forum).FirstOrDefault();

                if (MasterForum != null)
                {
                    MasterForum.Delflag = true;


                    dbContext.SaveChanges();
                }


                oResponse.status = "200";
                oResponse.status = "Success";
                oResponse.result = MasterForum.PK_ForumID;

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;


            }



            return Ok(oResponse);
        }
        [HttpGet]
        [Route("api/Common/GetForumList/{id}")]
        public IHttpActionResult GetForumList(int id)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                List<UserForum> oForum = new List<UserForum>();

                oForum = (from Forum in dbContext.Master_Forum
                          where Forum.Delflag == false && Forum.FK_ForumTypeID == id
                          select new UserForum
                          {
                              PK_ForumID = Forum.PK_ForumID,
                              FK_UserID = Forum.FK_UserID,
                              Name = Forum.Name,
                              Forum = Forum.Forum,
                              ForumTopic = Forum.ForumTopic,
                              CreatedDate = Forum.CreatedDate.Value,
                              FK_ForumTypeID = Forum.FK_ForumTypeID,
                              ProfilePhotoPath = Forum.Master_User.ProfilePhotoPath

                          }).OrderByDescending(p => p.PK_ForumID).ToList();


                foreach (UserForum Forum in oForum)
                {
                    Forum.CreatedDateString = FormatDateTime(Forum.CreatedDate);

                    List<ForumComments> oForumComments = new List<ForumComments>();

                    oForumComments = (from ForumComments in dbContext.Master_ForumComments
                                      where ForumComments.DelFlag == false && ForumComments.FK_ForumD == Forum.PK_ForumID
                                      select new ForumComments
                                      {
                                          PK_ForumCommentID = ForumComments.PK_ForumCommentID,
                                          FK_UserID = ForumComments.FK_UserID,
                                          Name = ForumComments.Name,
                                          Comment = ForumComments.Comment,
                                          CreatedDate = ForumComments.CreatedDate,
                                          ProfilePhotoPath = ForumComments.Master_User.ProfilePhotoPath

                                      }).OrderByDescending(p => p.PK_ForumCommentID).ToList();
                    Forum.ForumComments = oForumComments;

                }

                if (oForum.Count >= 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = oForum;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }
        [HttpPost]
        [Route("api/Common/AddForumComments")]
        public IHttpActionResult AddForumComments(Master_ForumComments oForumComments)
        {
            ReturnResponse oResponse = new ReturnResponse();


            try
            {

                oForumComments.CreatedDate = DateTime.Now;
                oForumComments.Name = GetUserName(oForumComments.FK_UserID.Value);
                dbContext.Master_ForumComments.Add(oForumComments);

                dbContext.SaveChanges();


                oResponse.status = "200";
                oResponse.status = "Success";
                oResponse.result = oForumComments.PK_ForumCommentID;

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;


            }




            return Ok(oResponse);
        }
        [HttpPost]
        [Route("api/Common/UploadForumImage")]
        public ReturnResponse UploadForumImage()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count > 0)
                {

                    var postedFile = httpRequest.Files[0];
                    string ext = System.IO.Path.GetExtension(postedFile.FileName);
                    string csNewFileName = Guid.NewGuid().ToString() + ext;
                    var filePath = HttpContext.Current.Server.MapPath("~/Uploads/Forum/" + csNewFileName);
                    postedFile.SaveAs(filePath);

                    dbContext.SaveChanges();
                    oResponse.status = "200";
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    string Imageurl = WebConfigurationManager.AppSettings["Imageurl"];
                    Imageurl = Imageurl + "Forum/";
                    oResponse.result = Imageurl+ csNewFileName;
                    oResponse.link = Imageurl + csNewFileName;
                }

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }
            return oResponse;
        }
        [HttpPost]
        [Route("api/Common/UpdateForumComments")]
        public IHttpActionResult UpdateForumComment(Master_ForumComments oForumComments)
        {
            ReturnResponse oResponse = new ReturnResponse();


            try
            {

                var MasterForumComments = (from ForumComments in dbContext.Master_ForumComments
                                           where ForumComments.PK_ForumCommentID == oForumComments.PK_ForumCommentID
                                           select ForumComments).FirstOrDefault();

                if (MasterForumComments != null)
                {

                    MasterForumComments.Comment = oForumComments.Comment;

                    dbContext.SaveChanges();

                    oResponse.status = "200";
                    oResponse.status = "Success";
                    oResponse.result = oForumComments.PK_ForumCommentID;
                }
                else
                {
                    oResponse.errors = "No data found";
                    oResponse.status = "Error";
                }

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;


            }




            return Ok(oResponse);
        }
        [HttpPost]
        [Route("api/Common/DeleteForumComments")]
        public IHttpActionResult DeleteForumComments(Master_ForumComments oForumComments)
        {
            ReturnResponse oResponse = new ReturnResponse();


            try
            {


                var MasterForumComments = (from ForumComments in dbContext.Master_ForumComments
                                           where ForumComments.PK_ForumCommentID == oForumComments.PK_ForumCommentID
                                           select ForumComments).FirstOrDefault();

                if (MasterForumComments != null)
                {
                    MasterForumComments.DelFlag = true;


                    dbContext.SaveChanges();
                }


                oResponse.status = "200";
                oResponse.status = "Success";
                oResponse.result = MasterForumComments.PK_ForumCommentID;

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;


            }



            return Ok(oResponse);
        }


        [HttpPost]
        [Route("api/Common/AddBlog")]
        public IHttpActionResult AddBlog(Master_Blog oBlog)
        {
            ReturnResponse oResponse = new ReturnResponse();


            try
            {

                oBlog.CreatedDate = DateTime.Now;
                oBlog.Name = GetUserName(oBlog.FK_UserID);

                dbContext.Master_Blog.Add(oBlog);


                dbContext.SaveChanges();
                UserMessage oUserMessage = new UserMessage();
                MessageController omsg = new MessageController();
                oUserMessage.FK_SenderID = oBlog.FK_UserID;
                omsg.AddMsgToTrackedUser(oUserMessage,Constants.newBlog);
                oResponse.status = "200";
                oResponse.status = "Success";
                oResponse.result = oBlog.PK_Blog;

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;


            }




            return Ok(oResponse);
        }


        [HttpPost]
        [Route("api/Common/UpdateBlog")]
        public IHttpActionResult UpdateBlog(Master_Blog oBlog)
        {
            ReturnResponse oResponse = new ReturnResponse();


            try
            {

                var MasterBlog = (from Blog in dbContext.Master_Blog
                                  where Blog.PK_Blog == oBlog.PK_Blog
                                  select Blog).FirstOrDefault();

                if (MasterBlog != null)
                {

                    MasterBlog.Blog = oBlog.Blog;
                    MasterBlog.BlogHeading = oBlog.BlogHeading;

                    dbContext.SaveChanges();

                    oResponse.status = "200";
                    oResponse.status = "Success";
                    oResponse.result = oBlog.PK_Blog;
                }
                else
                {
                    oResponse.errors = "No data found";
                    oResponse.status = "Error";
                }

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;


            }




            return Ok(oResponse);
        }



        [HttpPost]
        [Route("api/Common/DeleteBlog")]
        public IHttpActionResult DeleteBlog(Master_Blog oBlog)
        {
            ReturnResponse oResponse = new ReturnResponse();


            try
            {


                var MasterBlog = (from Blog in dbContext.Master_Blog
                                  where Blog.PK_Blog == oBlog.PK_Blog
                                  select Blog).FirstOrDefault();

                if (MasterBlog != null)
                {
                    MasterBlog.DelFlag = true;


                    dbContext.SaveChanges();
                }


                oResponse.status = "200";
                oResponse.status = "Success";
                oResponse.result = MasterBlog.PK_Blog;

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;


            }



            return Ok(oResponse);
        }


        private string GetUserName(int UserID)
        {
            var User = (from user in dbContext.Master_User
                        where user.PK_UserID == UserID && user.DelFlag == false
                        select new { user.LegalFirstName, user.LegalMiddleName, user.LastName }).FirstOrDefault();

            if (User == null)
                return null;

            if (User.LegalMiddleName == null || User.LegalMiddleName == "" || User.LegalMiddleName == " ")
                return User.LegalFirstName + " " + User.LastName;
            else
                return User.LegalFirstName + " " + User.LegalMiddleName + " " + User.LastName;


        }


        [HttpPost]
        [Route("api/Common/AddBlogComments")]
        public IHttpActionResult AddBlogComments(Master_BlogComments oBlogComments)
        {
            ReturnResponse oResponse = new ReturnResponse();


            try
            {

                oBlogComments.CreatedDate = DateTime.Now;
                oBlogComments.Name = GetUserName(oBlogComments.FK_UserID.Value);
                dbContext.Master_BlogComments.Add(oBlogComments);


                dbContext.SaveChanges();

                oResponse.status = "200";
                oResponse.status = "Success";
                oResponse.result = oBlogComments.PK_BlogCommentID;

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;


            }




            return Ok(oResponse);
        }

        [HttpGet]
        [Route("api/Common/GetUserBlog/{id}")]
        public IHttpActionResult GetUserBlog(int id)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                List<UserBlog> oBlog = new List<UserBlog>();

                oBlog = (from Blog in dbContext.Master_Blog
                         where Blog.DelFlag == false && Blog.FK_UserID == id
                         select new UserBlog
                         {
                             PK_Blog = Blog.PK_Blog,
                             FK_UserID = Blog.FK_UserID,
                             Name = Blog.Name,
                             Blog = Blog.Blog,
                             BlogHeading = Blog.BlogHeading,
                             CreatedDate = Blog.CreatedDate

                         }).OrderByDescending(p => p.PK_Blog).ToList();


                foreach (UserBlog Blog in oBlog)
                {
                    Blog.CreatedDateString = FormatDateTime(Blog.CreatedDate);

                }

                if (oBlog.Count >= 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = oBlog;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }


        [HttpGet]
        [Route("api/Common/GetBlogComments/{id}")]
        public IHttpActionResult GetBlogComments(int id)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                List<BlogComments> oBlogComments = new List<BlogComments>();

                oBlogComments = (from BlogComments in dbContext.Master_BlogComments
                                 where BlogComments.DelFlag == false && BlogComments.FK_BlogID == id
                                 select new BlogComments
                                 {
                                     PK_BlogCommentID = BlogComments.PK_BlogCommentID,
                                     FK_BlogID = BlogComments.FK_BlogID,
                                     FK_UserID = BlogComments.FK_UserID,
                                     Name = BlogComments.Name,
                                     Comment = BlogComments.Comment,
                                     CreatedDate = BlogComments.CreatedDate
                                 }
                                ).OrderByDescending(p => p.PK_BlogCommentID).ToList();


                foreach (BlogComments BlogComments in oBlogComments)
                {
                    BlogComments.CreatedDateString = FormatDateTime(BlogComments.CreatedDate);

                }

                if (oBlogComments.Count >= 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = oBlogComments;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }


        [HttpPost]
        [Route("api/Common/UploadBlogImage")]
        public ReturnResponse UploadBlogImage()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count > 0)
                {

                    var postedFile = httpRequest.Files[0];
                    string ext = System.IO.Path.GetExtension(postedFile.FileName);
                    string csNewFileName = Guid.NewGuid().ToString() + ext;
                    var filePath = HttpContext.Current.Server.MapPath("~/Uploads/Blog/" + csNewFileName);
                    postedFile.SaveAs(filePath);

                    dbContext.SaveChanges();
                    oResponse.status = "200";
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    string Imageurl = WebConfigurationManager.AppSettings["Imageurl"];
                    Imageurl = Imageurl + "Blog/";
                    oResponse.result = Imageurl + csNewFileName;
                    oResponse.link = Imageurl + csNewFileName;
                }

            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }
            return oResponse;
        }

        [HttpPost]
        [Route("api/Common/ChallengeboardFullTextSearch/{id}")]
        public IHttpActionResult ChallengeboardFullTextSearch(FullTextSearch oFullTextSearch,int id)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {




                var partString = String.Format(@"""{0}*""", oFullTextSearch.searchString);
                var stringArray = oFullTextSearch.searchString.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                if (stringArray.Length > 1)
                {
                    for (int index = 0; index < stringArray.Length; index++)
                    {
                        partString = String.Format(@"{0} or ""{1}*""", partString, stringArray[index]);
                    }
                }

                var oDb = new DBAccess();

                string searchResult = oDb.ChallengeboardFullTextSearch(partString,id);

                if (searchResult != null)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = searchResult;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }
        [HttpPost]
        [Route("api/Common/FullTextSearch")]
        public IHttpActionResult FullTextSearch(FullTextSearch oFullTextSearch)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var partString = String.Format(@"""{0}*""", oFullTextSearch.searchString);
                var stringArray = oFullTextSearch.searchString.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                if (stringArray.Length > 1)
                {
                    for (int index = 0; index < stringArray.Length; index++)
                    {
                        partString = String.Format(@"{0} or ""{1}*""", partString, stringArray[index]);
                    }
                }

                var oDb = new DBAccess();

                string searchResult = oDb.FullTextSearch(partString,oFullTextSearch.IsSuperadmin);

                if (searchResult != null)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = searchResult;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }
        [HttpGet]
        [Route("api/Common/DEFAULTTOKEN")]
        public IHttpActionResult DEFAULTTOKEN()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = null;
                

                
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }
        [HttpGet]
        [Route("api/Common/FullTextSearch/{searchString}")]
        public IHttpActionResult FullTextSearch(string searchString)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {




                var partString = String.Format(@"""{0}*""", searchString);
                var stringArray = searchString.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                if (stringArray.Length > 1)
                {
                    for (int index = 0; index < stringArray.Length; index++)
                    {
                        partString = String.Format(@"{0} or ""{1}*""", partString, stringArray[index]);
                    }
                }

                DBAccess oDB = new DBAccess();

                string SearchResult = oDB.FullTextSearch(partString,false);

                if (SearchResult != null)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = SearchResult;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);
        }



    }


}