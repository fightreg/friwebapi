﻿using FRIWebAPI.Database;
using FRIWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using static FRIWebAPI.ClassFiles.Common;

namespace FRIWebAPI.Controllers
{
    public class LookupController : ApiController
    {
        FRIEntities dbContext = null;
        public LookupController()
        {
            dbContext = new FRIEntities();
        }

        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }

        [HttpGet]
        [Route("api/Lookup/GetCountryLookup")] //   /api/Lookup/GetCountryLookup
        public IHttpActionResult GetCountryLookup()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var result = (from CountryList in dbContext.Lookup_Country
                              
                 select new { Id = CountryList.PK_CountryID, Description = CountryList.Description }).OrderBy(o => new { o.Id }).ToList().Distinct();
                
                if (result.Count() > 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = result;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);


        }


        [HttpGet]
        [Route("api/Lookup/GetStateLookup/{id}")] //   /api/Lookup//GetStateLookup?Id=43
        public IHttpActionResult GetStateLookup(int id)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var result = (from StateLookup in dbContext.Lookup_StateOrProvince

                              select new { Id = StateLookup.PK_StateOrProvinceID, Description = StateLookup.Description, CountryID = StateLookup.FK_CountryID }).OrderBy(o => new { o.Id }).Where( W=> W.CountryID == id).ToList().Distinct();

                if (result.Count() > 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = result;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);


        }

        [HttpGet]
        [Route("api/Lookup/GetSocialMediaLookup")] //   /api/Lookup/GetSocialMediaLookup
        public IHttpActionResult GetSocialMediaLookup()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var result = (from SocialMediaList in dbContext.Lookup_SocialMediaPlatform 

                              select new { Id = SocialMediaList.PK_SocialMediaID, Description = SocialMediaList.Description, IconPath = SocialMediaList.IconPath }).OrderBy(o => new { o.Id }).ToList().Distinct();

                if (result.Count() > 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = result;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);


        }

        [HttpGet]
        [Route("api/Lookup/GetCombatStyleLookup")] //   /api/Lookup/GetCombatStyleLookup
        public IHttpActionResult GetCombatStyleLookup()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var result = (from CombatStyle in dbContext.Lookup_CombatStyle

                              select new { Id = CombatStyle.PK_CombatStyleID, Description = CombatStyle.Description }).OrderBy(o => new { o.Id }).ToList().Distinct();

                if (result.Count() > 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = result;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);


        }


        [HttpGet]
        [Route("api/Lookup/GetDisciplineLookup")] //   /api/Lookup/GetDisciplineLookup
        public IHttpActionResult GetDisciplineLookup()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var result = (from DisciplineList in dbContext.Lookup_Discipline

                              select new { Id = DisciplineList.PK_DisciplineID, Description = DisciplineList.Description }).OrderBy(o => new { o.Id }).ToList().Distinct();

                if (result.Count() > 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = result;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);


        }


        [HttpGet]
        [Route("api/Lookup/GetEyeColourLookup")] //   /api/Lookup/GetEyeColourLookup
        public IHttpActionResult GetEyeColourLookup()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var result = (from EyeColourList in dbContext.Lookup_EyeColour

                              select new { Id = EyeColourList.PK_EyeColourID, Description = EyeColourList.Description }).OrderBy(o => new { o.Id }).ToList().Distinct();

                if (result.Count() > 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = result;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);


        }     
       
       
        [HttpGet]
        [Route("api/Lookup/GetFightDecisionLookup")] //   /api/Lookup/GetFightDecisionLookup
        public IHttpActionResult GetFightDecisionLookup()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var result = (from FightDecisionList in dbContext.Lookup_FightDecision

                              select new { Id = FightDecisionList.PK_FightDecisionID, Description = FightDecisionList.Description }).OrderBy(o => new { o.Id }).ToList().Distinct();

                if (result.Count() > 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = result;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);


        }


        [HttpGet]
        [Route("api/Lookup/GetFightResultLookup")] //   /api/Lookup/GetFightResultLookup
        public IHttpActionResult GetFightResultLookup()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var result = (from FightResultList in dbContext.Lookup_FightResult

                              select new { Id = FightResultList.PK_FightResultID, Description = FightResultList.Description }).OrderBy(o => new { o.Id }).ToList().Distinct();

                if (result.Count() > 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = result;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);


        }


        [HttpGet]
        [Route("api/Lookup/GetHairColourLookup")] //   /api/Lookup/GetHairColourLookup
        public IHttpActionResult GetHairColourLookup()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var result = (from HairColourList in dbContext.Lookup_HairColour

                              select new { Id = HairColourList.PK_HairColourID, Description = HairColourList.Description }).OrderBy(o => new { o.Id }).ToList().Distinct();

                if (result.Count() > 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = result;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);


        }

        [HttpGet]
        [Route("api/Lookup/GetSkillLookup")] //   /api/Lookup/GetSkillLookup
        public IHttpActionResult GetSkillLookup()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var result = (from SkillList in dbContext.Lookup_Skill

                              select new { Id = SkillList.PK_SkillID, Description = SkillList.Description }).OrderBy(o => new { o.Id }).ToList().Distinct();

                if (result.Count() > 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = result;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);


        }


        [HttpGet]
        [Route("api/Lookup/GetUnitLookup")] //   /api/Lookup/GetUnitLookup
        public IHttpActionResult GetUnitLookup(String csItem)
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var result = (from UnitList in dbContext.Lookup_Unit 
                              select new { Id = UnitList.PK_UnitID, Description = UnitList.Description, UnitType = UnitList.UnitType }).Where(W => W.UnitType == csItem).OrderBy(o => new { o.Id }).ToList().Distinct();

                if (result.Count() > 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = result;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);


        }



       

        [HttpGet]
        [Route("api/Lookup/GetWeightCategoryLookup")] //   /api/Lookup/GetWeightCategoryLookup
        public IHttpActionResult GetWeightCategoryLookup()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var result = (from WeightCategoryList in dbContext.Lookup_WeightCategory

                              select new { Id = WeightCategoryList.PK_WeightCategoryID, Description = WeightCategoryList.Description }).OrderBy(o => new { o.Id }).ToList().Distinct();

                if (result.Count() > 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = result;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);


        }
        [HttpGet]
        [Route("api/Lookup/GetOfficialLevelLookup")] //   /api/Lookup/GetOfficialLevelLookup
        public IHttpActionResult GetOfficialLevelLookup()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var result = (from OfficialLevelList in dbContext.Lookup_OfficialLevel

                              select new { Id = OfficialLevelList.PK_OfficialLevelID, Description = OfficialLevelList.Description }).OrderBy(o => new { o.Id }).ToList().Distinct();

                if (result.Count() > 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = result;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);


        }

        [HttpGet]
        [Route("api/Lookup/GetOfficialTypeLookup")] //   /api/Lookup/GetOfficialTypeLookup
        public IHttpActionResult GetOfficialTypeLookup()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var result = (from OfficialTypeList in dbContext.Lookup_OfficialType

                              select new { Id = OfficialTypeList.PK_OfficialTypeID, Description = OfficialTypeList.Description }).OrderBy(o => new { o.Id }).ToList().Distinct();

                if (result.Count() > 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = result;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);


        }
        [HttpGet]
        [Route("api/Lookup/GetCitizenshipLookup")] //   /api/Lookup/GetCitizenshipLookup
        public IHttpActionResult GetCitizenshipLookup()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var result = (from CitizenshipList in dbContext.Lookup_Citizenship

                              select new { Id = CitizenshipList.PK_CitizenshipID, Description = CitizenshipList.Description }).OrderBy(o => new { o.Id }).ToList().Distinct();

                if (result.Count() > 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = result;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);


        }


        [HttpGet]
        [Route("api/Lookup/GetEventTypeLookup")] //   /api/Lookup/GetEventTypeLookup
        public IHttpActionResult GetEventTypeLookup()
        {
            ReturnResponse oResponse = new ReturnResponse();
            try
            {
                var result = (from EventTypeList in dbContext.Lookup_EventType

                              select new { Id = EventTypeList.PK_EventTypeID, Description = EventTypeList.Description }).OrderBy(o => new { o.Id }).ToList().Distinct();

                if (result.Count() > 0)
                {
                    oResponse.status = "Success";
                    oResponse.errors = null;
                    oResponse.result = result;
                }

                else
                {
                    oResponse.status = "Failed";
                    oResponse.errors = "No data availabe.";
                    oResponse.result = null;
                }
            }
            catch (Exception ex)
            {
                oResponse.errors = "Service Error";
                oResponse.status = "Exception";
                oResponse.result = ex.Message;

            }

            return Ok(oResponse);


        }
    }
}