﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using FRIWebAPI.Models;

namespace FRIWebAPI.App_Start
{




    [DataContract]
    public class GetDatas
    {
        [DataMember]
        public string strSessionID { get; set; }
        [DataMember]
        public string strCondition { get; set; }
        [DataMember]
        public string strFieldNames { get; set; }

    }


    public class DBAccess
    {

        public bool FormatJsonOutput { get; set; }
        public SqlConnection oDbConnection = new SqlConnection();
        public SqlCommand oDbCommand = new SqlCommand();
        public DataSet oDataSet = new DataSet();
        ArrayList arrSqlParam = new ArrayList();
        //string strSingleResult="";

        public static LogWriter oLogFile = new LogWriter();

        public string WriteLog(string Message, LogType LogWriteType)
        {
            oLogFile.WriteLog(Message, LogWriteType);
            return "Success";
        }

        public void WriteReportLog(string strLog)
        {
            try
            {
                /*   string csLogFile = HttpContext.Current.Server.MapPath(@"~\Report\Log.txt");
                   FileStream fs = new FileStream(csLogFile, FileMode.Append);
                   StreamWriter sw = new StreamWriter(fs);
                   sw.WriteLine(DateTime.Now.ToString() + ":" +strLog);
                   sw.Flush();
                   sw.Close();
                   fs.Close();*/
            }
            catch (Exception)
            {
                // Show( "Error in WriteLog funtion");
            }
        }

        public DBAccess()
        {

            try
            {

                string strConnectionString = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
                oDbConnection.ConnectionString = strConnectionString;
                oDbCommand.Connection = oDbConnection;
                oDbCommand.CommandTimeout = 120;

            }
            catch (SqlException ClsDBException)
            {
                string eMess = ClsDBException.Message.ToString();
                WriteLog("DBAccess:DBAccess: " + eMess, LogType.EXCEPTION);
                return;
            }
            catch (OutOfMemoryException ClsMemException)
            {
                string eMess = ClsMemException.Message.ToString();
                WriteLog("DBAccess:DBAccess: " + eMess, LogType.EXCEPTION);
                return;
            }
            catch
            {

                return;
            }
            return;
        }
        public void InitilizeArray()
        {
            arrSqlParam.Clear();
            //strSingleResult = "";
        }
        public void CloseDB()
        {
            oDataSet.Tables.Clear();
            if (oDbConnection.State == ConnectionState.Open)
                oDbConnection.Close();
        }
        public Boolean ExecuteStoredProcedure(string strProcedure)
        {

            try
            {


                if (oDbConnection.State != ConnectionState.Open)
                    oDbConnection.Open();
                oDbCommand.CommandType = CommandType.StoredProcedure;
                oDbCommand.CommandText = strProcedure;
                oDbCommand.Parameters.Clear();
                for (int nIndex = 0; nIndex < arrSqlParam.Count; nIndex++)
                {
                    oDbCommand.Parameters.Add(arrSqlParam[nIndex]);
                }
                oDbCommand.ExecuteNonQuery();
                //     oDbConnection.Close();

            }
            catch (SqlException ClsDBException)
            {
                string eMess = ClsDBException.Message.ToString();
                WriteLog("DBAccess:ExecuteStoredProcedure: " + strProcedure + "," + eMess, LogType.EXCEPTION);
                return false;
            }
            catch (OutOfMemoryException ClsMemException)
            {
                string eMess = ClsMemException.Message.ToString();
                WriteLog("DBAccess:ExecuteStoredProcedure: " + strProcedure + "," + eMess, LogType.EXCEPTION);
                return false;
            }
            catch (Exception ex)
            {
                string eMess = ex.Message.ToString();
                WriteLog("DBAccess:ExecuteStoredProcedureEx: " + strProcedure + "," + eMess, LogType.EXCEPTION);
                return false;
            }

            return true;
        }


        public Boolean ExecuteStoredProcedureEx(string strProcedure)
        {
            try
            {
                oDataSet.Tables.Clear();
                if (oDbConnection.State != ConnectionState.Open)
                    oDbConnection.Open();
                oDbCommand.CommandText = strProcedure;
                oDbCommand.Parameters.Clear();
                for (int nIndex = 0; nIndex < arrSqlParam.Count; nIndex++)
                {
                    oDbCommand.Parameters.Add(arrSqlParam[nIndex]);
                }
                oDbCommand.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter oDataAdapter = new SqlDataAdapter();
                oDataAdapter.SelectCommand = oDbCommand;
                oDataAdapter.Fill(oDataSet);
                //   oDbConnection.Close();
                return true;
            }
            catch (SqlException ClsDBException)
            {
                string eMess = ClsDBException.Message.ToString();
                WriteLog("DBAccess:ExecuteStoredProcedureEx: " + strProcedure + "," + eMess, LogType.EXCEPTION);
                return false;
            }
            catch (OutOfMemoryException ClsMemException)
            {
                string eMess = ClsMemException.Message.ToString();
                WriteLog("DBAccess:ExecuteStoredProcedureEx: " + strProcedure + "," + eMess, LogType.EXCEPTION);
                return false;
            }

            catch (Exception ex)
            {
                string eMess = ex.Message.ToString();
                WriteLog("DBAccess:ExecuteStoredProcedureEx: " + strProcedure + "," + eMess, LogType.EXCEPTION);
                return false;
            }

        }

        public Boolean AddParam(string strParam, int nValue)
        {

            try
            {
                arrSqlParam.Add(new SqlParameter(strParam, nValue));
            }
            catch
            {
                return false;
            }
            return true;
        }
        public Boolean AddParam(string strParam, DateTime dtValue)
        {
            try
            {
                arrSqlParam.Add(new SqlParameter(strParam, dtValue));
            }
            catch
            {
                return false;
            }
            return true;
        }
        public Boolean AddParam(string strParam, float fValue)
        {
            try
            {
                arrSqlParam.Add(new SqlParameter(strParam, fValue));
            }
            catch
            {
                return false;
            }
            return true;
        }
        public Boolean AddParam(string strParam, Boolean bValue)
        {
            try
            {
                arrSqlParam.Add(new SqlParameter(strParam, bValue));
            }
            catch
            {
                return false;
            }
            return true;
        }
        public Boolean AddParam(string strParam, string strValue)
        {
            try
            {

                arrSqlParam.Add(new SqlParameter(strParam, strValue));
            }
            catch
            {
                return false;
            }
            return true;
        }
        public Boolean AddParam(string strParam, long lValue)
        {
            try
            {
                arrSqlParam.Add(new SqlParameter(strParam, lValue));
            }
            catch
            {
                return false;
            }
            return true;
        }

        public string Serialize(object value)
        {
            Type type = value.GetType();

            Newtonsoft.Json.JsonSerializer json = new Newtonsoft.Json.JsonSerializer();

            json.NullValueHandling = NullValueHandling.Ignore;

            json.ObjectCreationHandling = Newtonsoft.Json.ObjectCreationHandling.Replace;
            json.MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore;
            json.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            if (type == typeof(DataTable))
                json.Converters.Add(new DataTableConverter());
            else if (type == typeof(DataSet))
                json.Converters.Add(new DataSetConverter());

            StringWriter sw = new StringWriter();
            Newtonsoft.Json.JsonTextWriter writer = new JsonTextWriter(sw);
            if (this.FormatJsonOutput)
                writer.Formatting = Newtonsoft.Json.Formatting.Indented;
            else
                writer.Formatting = Newtonsoft.Json.Formatting.None;

            writer.QuoteChar = '"';
            json.Serialize(writer, value);

            string output = sw.ToString();

            writer.Close();
            sw.Close();

            return output;
        }
        public string SelectData(GetDatas objGetData, string strTable)
        {


            string strRetData = "";
            string StrLogToWrite = "";
            try
            {

                oDataSet.Tables.Clear();
                if (oDbConnection.State != ConnectionState.Open)
                    oDbConnection.Open();

                oDbCommand.CommandText = "PROC_SELECTDATA";
                oDbCommand.Parameters.Clear();
                oDbCommand.Parameters.AddWithValue("@strFieldNames", objGetData.strFieldNames);
                oDbCommand.Parameters.AddWithValue("@strTableName", strTable);
                oDbCommand.Parameters.AddWithValue("@strCondition", objGetData.strCondition);
                oDbCommand.CommandType = CommandType.StoredProcedure;

                StrLogToWrite = "exec PROC_SELECTDATA '" + strTable + "','" + objGetData.strFieldNames + "','" + objGetData.strCondition + "'";
                oLogFile.WriteToFile(StrLogToWrite);

                SqlDataAdapter oDataAdapter = new SqlDataAdapter();
                oDataAdapter.SelectCommand = oDbCommand;
                oDataAdapter.Fill(oDataSet);
                DataTable myDataTable = oDataSet.Tables[0];
                strRetData = Serialize(myDataTable);


            }
            catch (SqlException ClsDBException)
            {
                string eMess = ClsDBException.Message.ToString();
                WriteLog("DBAccess:SelectData: " + StrLogToWrite + "," + eMess, LogType.EXCEPTION);

            }
            catch (OutOfMemoryException ClsMemException)
            {
                string eMess = ClsMemException.Message.ToString();
                WriteLog("DBAccess:SelectData: " + StrLogToWrite + "," + eMess, LogType.EXCEPTION);

            }

            catch (Exception ex)
            {
                string eMess = ex.Message.ToString();
                WriteLog("DBAccess:SelectData: " + StrLogToWrite + "," + eMess, LogType.EXCEPTION);

            }
            finally
            {

                if (oDbConnection != null)
                {
                    if (oDbConnection.State == System.Data.ConnectionState.Open)
                        oDbConnection.Close();
                }

            }
            return strRetData;

        }


        public string SearchData(bool IsAdmin, int RoleID, string strCondition)
        {



            string StrLogToWrite = "";
            try
            {

                oDataSet.Tables.Clear();
                if (oDbConnection.State != ConnectionState.Open)
                    oDbConnection.Open();

                oDbCommand.CommandText = "PROC_COMMONSEARCH";
                oDbCommand.Parameters.Clear();

                oDbCommand.Parameters.AddWithValue("@strIsAdmin", IsAdmin);
                oDbCommand.Parameters.AddWithValue("@strRole", RoleID);
                oDbCommand.Parameters.AddWithValue("@strCondition", strCondition);
                oDbCommand.CommandType = CommandType.StoredProcedure;

                StrLogToWrite = "exec PROC_COMMONSEARCH " + RoleID.ToString() + "'" + strCondition + "'";
                oLogFile.WriteToFile(StrLogToWrite);

                SqlDataAdapter oDataAdapter = new SqlDataAdapter();
                oDataAdapter.SelectCommand = oDbCommand;
                oDataAdapter.Fill(oDataSet);
                DataTable myDataTable = oDataSet.Tables[0];

                //string strRetData = Serialize(myDataTable);
                // return strRetData;

                //    List<DataRow> rows = oDataSet.Tables[0].AsEnumerable().ToList();


                //   List<DataRow> rows = oDataSet.Tables[0].Select().ToList();


                ///   return rows;


                string JSONString = string.Empty;
                JSONString = JsonConvert.SerializeObject(oDataSet.Tables[0]);

                return JSONString;


            }
            catch (SqlException ClsDBException)
            {
                string eMess = ClsDBException.Message.ToString();
                WriteLog("DBAccess:SelectData: " + StrLogToWrite + "," + eMess, LogType.EXCEPTION);

            }
            catch (OutOfMemoryException ClsMemException)
            {
                string eMess = ClsMemException.Message.ToString();
                WriteLog("DBAccess:SelectData: " + StrLogToWrite + "," + eMess, LogType.EXCEPTION);

            }

            catch (Exception ex)
            {
                string eMess = ex.Message.ToString();
                WriteLog("DBAccess:SelectData: " + StrLogToWrite + "," + eMess, LogType.EXCEPTION);

            }
            finally
            {

                if (oDbConnection != null)
                {
                    if (oDbConnection.State == System.Data.ConnectionState.Open)
                        oDbConnection.Close();
                }

            }
            return null;

        }



        public string ChallengeboardFullTextSearch(string strString, int id)
        {

            try
            {
                oDataSet.Tables.Clear();
                if (oDbConnection.State != ConnectionState.Open)
                    oDbConnection.Open();

                var query = (@"SELECT TMP1.FK_MessageID as MessageId, TMP1.Fk_spartoid as RecipientId, TMP1.Fk_sparfromid as SenderId, MM.SenderName, MM.RecipientName, TMP1.MessageType,
                            TMP1.CreatedTime, TMP1.AcceptedTime,MU1.[ProfilePhotoPath] as ProfilePhotoSender, MU2.[ProfilePhotoPath] as ProfilePhotoRecipient,CreatedTime, 
                            AcceptedTime from (select TMP.PK_UserID,  MS.FK_MessageID, MS.Fk_spartoid, MS.Fk_sparfromid, 'spar' as MessageType, MS.CreatedDate as CreatedTime,
                            MS.AcceptedTime from master_spar MS INNER JOIN (select distinct PK_UserID from [FRIFullTextSearcNewView] MU INNER JOIN CONTAINSTABLE([FRIFullTextSearcNewView],*, '{0}') SR
                            ON SR.[Key] = MU.Id) TMP ON TMP.PK_UserID = MS.Fk_sparfromid OR  TMP.PK_UserID = MS.Fk_spartoid) TMP1 INNER JOIN Master_user MU1 ON MU1.PK_UserID = TMP1.Fk_spartoid
                            INNER JOIN Master_user MU2 ON MU2.PK_UserID = TMP1.Fk_sparfromid INNER JOIN Master_Messages MM ON TMP1.FK_MessageID = MM.[PK_MessageID]
                            UNION
                            SELECT TMP1.FK_MessageID as MessageId, TMP1.FK_ChallengerToID as RecipientId, TMP1.Fk_Challengefromid as SenderId, MM.SenderName, MM.RecipientName, TMP1.MessageType,
                            TMP1.CreatedTime, TMP1.AcceptedTime,MU1.[ProfilePhotoPath] as ProfilePhotoSender, MU2.[ProfilePhotoPath] as ProfilePhotoRecipient, CreatedTime, AcceptedTime from
                            (select TMP.PK_UserID, MS.FK_MessageID, MS.FK_ChallengerToID, MS.Fk_Challengefromid, 'Challenge' as MessageType, MS.CreatedDate as CreatedTime, MS.AcceptedTime
                             from Master_Challenge MS INNER JOIN (select distinct PK_UserID from[FRIFullTextSearcNewView] MU INNER JOIN CONTAINSTABLE([FRIFullTextSearcNewView],*, '{0}') SR
                                ON SR.[Key] = MU.Id) TMP ON TMP.PK_UserID = MS.Fk_Challengefromid OR  TMP.PK_UserID = MS.FK_ChallengerToID) TMP1 INNER JOIN Master_user MU1
                                ON MU1.PK_UserID = TMP1.FK_ChallengerToID INNER JOIN Master_user MU2 ON MU2.PK_UserID = TMP1.Fk_Challengefromid INNER JOIN Master_Messages MM
                                ON TMP1.FK_MessageID = MM.[PK_MessageID]");


                oDbCommand.CommandText = String.Format(query, strString);
                oDbCommand.Parameters.Clear();
                oDbCommand.CommandType = CommandType.Text;

                SqlDataAdapter oDataAdapter = new SqlDataAdapter();
                oDataAdapter.SelectCommand = oDbCommand;
                oDataAdapter.Fill(oDataSet);
                DataTable myDataTable = oDataSet.Tables[0];

                List<ChallengeBoard> challengeBoards = new List<ChallengeBoard>();

                foreach (DataRow dataRow in myDataTable.Rows)
                {
                    var oChallenge = new ChallengeBoard()
                    {
                        PK_MessageID = dataRow.Field<Int64>("MessageId"),
                        FK_RecipientID = dataRow.Field<int>("RecipientId"),
                        FK_SenderID = dataRow.Field<int>("SenderId"),
                        MessageType = dataRow.Field<string>("MessageType"),
                        SenderName = dataRow.Field<string>("SenderName"),
                        RecipientName = dataRow.Field<string>("RecipientName"),
                        ProfilePhotoSender = dataRow.Field<string>("ProfilePhotoSender"),
                        ProfilePhotoRecipient = dataRow.Field<string>("ProfilePhotoRecipient"),
                        SendTime = dataRow.Field<DateTime>("CreatedTime"),
                        AcceptedTime = dataRow.Field<DateTime?>("AcceptedTime"),
                    };


                    oChallenge.SendTimeString = "Challenge date: " + oChallenge.SendTime.Value.ToString("dd MMMM yyyy");
                    oChallenge.AcceptedTimeString = oChallenge.AcceptedTime == null ? "" : " Accepted date: " + oChallenge.AcceptedTime.Value.ToString("dd MMMM yyyy");
                    if (oChallenge.MessageType == "challenge")
                    {
                        if (oChallenge.AcceptedTime == null)
                            oChallenge.Message = oChallenge.SenderName + " challenged " + oChallenge.RecipientName + " to a match and awaiting respose.";
                        else
                            oChallenge.Message = oChallenge.RecipientName + " accepted the challenge from " + oChallenge.SenderName + ".";



                        if (oChallenge.FK_SenderID == id)
                        {
                            if (oChallenge.AcceptedTime == null)
                                oChallenge.Message = "You challenged " + oChallenge.RecipientName + " to a match and awaiting respose.";
                            else
                                oChallenge.Message = oChallenge.RecipientName + " accepted your challenge.";

                        }
                        if (oChallenge.FK_RecipientID == id)
                        {
                            if (oChallenge.AcceptedTime == null)
                                oChallenge.Message = oChallenge.SenderName + " challenged you to a match and he is awaiting respose.";
                            else
                                oChallenge.Message = "You accepted the challenge from " + oChallenge.SenderName;

                        }
                    }

                    if (oChallenge.MessageType == "spar")
                    {
                        if (oChallenge.AcceptedTime == null)
                            oChallenge.Message = oChallenge.SenderName + " spared " + oChallenge.RecipientName + " to a match and awaiting respose.";
                        else
                            oChallenge.Message = oChallenge.RecipientName + " accepted the spar from " + oChallenge.SenderName + ".";

                        if (oChallenge.FK_SenderID == id)
                        {
                            if (oChallenge.AcceptedTime == null)
                                oChallenge.Message = "You spared " + oChallenge.RecipientName + " to a match and awaiting respose.";
                            else
                                oChallenge.Message = oChallenge.RecipientName + " accepted your spar.";

                        }
                        if (oChallenge.FK_RecipientID == id)
                        {
                            if (oChallenge.AcceptedTime == null)
                                oChallenge.Message = oChallenge.SenderName + " spared you to a match and he is awaiting respose.";
                            else
                                oChallenge.Message = "You accepted the spar from " + oChallenge.SenderName;

                        }
                    }

                    challengeBoards.Add(oChallenge);
                }


                var jsonString = JsonConvert.SerializeObject(challengeBoards);
                return jsonString;
            }
            catch (SqlException clsDbException)
            {
                string eMess = clsDbException.Message.ToString();
                WriteLog(eMess, LogType.EXCEPTION);

            }
            catch (OutOfMemoryException ClsMemException)
            {
                string eMess = ClsMemException.Message.ToString();
                WriteLog(eMess, LogType.EXCEPTION);

            }

            catch (Exception ex)
            {
                string eMess = ex.Message.ToString();
                WriteLog(eMess, LogType.EXCEPTION);

            }
            finally
            {

                if (oDbConnection != null)
                {
                    if (oDbConnection.State == System.Data.ConnectionState.Open)
                        oDbConnection.Close();
                }

            }
            return null;

        }
        public string FullTextSearch(string strString,bool IsSuperadmin)
        {

            try
            {
                oDataSet.Tables.Clear();
                if (oDbConnection.State != ConnectionState.Open)
                    oDbConnection.Open();
                var query = string.Empty;
                if (IsSuperadmin)

                 query = (@"select PK_UserID, concat(LegalFirstName, ' ', LegalMiddleName, ' ', LastName) as username, ProfilePhotoPath, sr.[Rank], MU.Roledescription,
                                case when City = null or City = '' then StateName else concat(City, ', ', StateName) end as UserLocation from [FRIFullTextSearcNewView] MU INNER JOIN
                                CONTAINSTABLE([FRIFullTextSearcNewView],*, '{0}') SR
                                ON SR.[Key] = MU.Id order by sr.[rank] desc ");//modified by nisha,Phone and EmailAddress can not be displayed,search result should be include Roll and Country/City
                else

                    query = (@"select PK_UserID, concat(LegalFirstName, ' ', LegalMiddleName, ' ', LastName) as username, ProfilePhotoPath, sr.[Rank], MU.Roledescription,
                                case when City = null or City = '' then StateName else concat(City, ', ', StateName) end as UserLocation from [FRIFullTextSearcNewView] MU INNER JOIN
                                CONTAINSTABLE([FRIFullTextSearcNewView],*, '{0}') SR
                                ON SR.[Key] = MU.Id  where MU.FK_Status=1 order by sr.[rank] desc ");
                             //order by username
                oDbCommand.CommandText = String.Format(query, strString);
                oDbCommand.Parameters.Clear();
                oDbCommand.CommandType = CommandType.Text;

                SqlDataAdapter oDataAdapter = new SqlDataAdapter();
                oDataAdapter.SelectCommand = oDbCommand;
                oDataAdapter.Fill(oDataSet);
                DataTable myDataTable = oDataSet.Tables[0];

                Dictionary<int, SearchResult> results = new Dictionary<int, SearchResult>();
                var columns = myDataTable.Columns;
                foreach (DataRow dataRow in myDataTable.Rows)
                {
                    var userId = dataRow.Field<int>("PK_UserID");
                    var userRole = dataRow.Field<string>("Roledescription");

                    if (results.ContainsKey(userId))
                    {
                        results[userId].Roles.Add(userRole);
                        continue;
                    }

                    var searchResult = new SearchResult
                    {
                        PK_userid = userId,
                        Profilephotopath = dataRow.Field<string>("ProfilePhotoPath"),
                        Userlocation = dataRow.Field<string>("UserLocation"),
                        Username = dataRow.Field<string>("username")
                    };

                    searchResult.Roles.Add(userRole);
                    results.Add(userId, searchResult);
                }

                var finalResult = new List<SearchResult>();

                foreach (var resultsValue in results.Values)
                {
                    if (resultsValue.Roles.Count == 1 && resultsValue.Roles.Contains("Fan"))
                        continue;

                    finalResult.Add(resultsValue);
                }

                var jsonString = JsonConvert.SerializeObject(finalResult);
                return jsonString;
            }
            catch (SqlException clsDbException)
            {
                string eMess = clsDbException.Message.ToString();
                WriteLog(eMess, LogType.EXCEPTION);

            }
            catch (OutOfMemoryException ClsMemException)
            {
                string eMess = ClsMemException.Message.ToString();
                WriteLog(eMess, LogType.EXCEPTION);

            }

            catch (Exception ex)
            {
                string eMess = ex.Message.ToString();
                WriteLog(eMess, LogType.EXCEPTION);

            }
            finally
            {

                if (oDbConnection != null)
                {
                    if (oDbConnection.State == System.Data.ConnectionState.Open)
                        oDbConnection.Close();
                }

            }
            return null;

        }


        //      DBAccess objDBAccess = new DBAccess();
        //      objDBAccess.oDbConnection.Open();
        //objDBAccess.InitilizeArray();
        //                  objDBAccess.AddParam("@aram", id);
        //                  if (!objDBAccess.ExecuteStoredProcedureEx(""))
        //                      throw new CustomException("Error While filling individual profile");
        //      objDBAccess.CloseDB();


        //public string InsertData(string strSessionID, string strJsonData, string strTable)
        //{
        //    string strRetData = "";
        //    SqlConnection sqlCon = new SqlConnection();
        //    SqlCommand sqlCmd = new SqlCommand();
        //    try
        //    {

        //        sqlCon = GetConnectionString(strSessionID);
        //        if (sqlCon == null)
        //            return " You are not authorized to use the server";
        //        SqlCommand myCommand = new SqlCommand("PROC_INSERTDATA");
        //        myCommand.CommandType = CommandType.StoredProcedure;
        //        myCommand.Connection = sqlCon;



        //        // To convert JSON text contained in string json into an XML node


        //        XmlDocument doc = (XmlDocument)JsonConvert.DeserializeXmlNode(strJsonData, "Record");


        //        string strXMLData = doc.InnerXml;

        //        myCommand.Parameters.AddWithValue("@strXMLData", strXMLData);
        //        myCommand.Parameters.AddWithValue("@strTableName", strTable);

        //        string StrLogToWrite = "exec PROC_INSERTDATA '" + strXMLData + "','" + strTable + "'";
        //        oLogFile.WriteToFile(StrLogToWrite);

        //        SqlDataAdapter da = new SqlDataAdapter(myCommand);
        //        DataSet data = new DataSet();
        //        da.Fill(data);
        //        DataTable myDataTable = data.Tables[0];
        //        strRetData = Serialize(myDataTable);
        //        da.Dispose();
        //        sqlCon.Close();
        //    }
        //    catch (Exception ex)
        //    {

        //        oLogFile.WriteToFile(ex.ToString());
        //        return ex.ToString();

        //    }
        //    finally
        //    {
        //        if (sqlCon != null)
        //        {
        //            if (sqlCon.State == System.Data.ConnectionState.Open)
        //                sqlCon.Close();
        //        }
        //    }
        //    return strRetData;
        //}

        //public string UpdateData(string strSessionID, string strCondition, string strJsonData, string strTable)
        //{
        //    SqlConnection sqlCon = new SqlConnection();
        //    SqlCommand sqlCmd = new SqlCommand();
        //    try
        //    {

        //        sqlCon = GetConnectionString(strSessionID);
        //        if (sqlCon == null)
        //            return " You are not authorized to use the server";
        //        SqlCommand myCommand = new SqlCommand("PROC_UPDATEDATA");
        //        myCommand.CommandType = CommandType.StoredProcedure;
        //        myCommand.Connection = sqlCon;
        //        XmlDocument doc = (XmlDocument)JsonConvert.DeserializeXmlNode(strJsonData, "Record");
        //        string strXMLData = doc.InnerXml;
        //        myCommand.Parameters.AddWithValue("@strXMLData", strXMLData);
        //        myCommand.Parameters.AddWithValue("@strTableName", strTable);
        //        myCommand.Parameters.AddWithValue("@strCondition", strCondition);
        //        SqlDataAdapter da = new SqlDataAdapter(myCommand);
        //        DataSet data = new DataSet();
        //        da.Fill(data);
        //        da.Dispose();
        //        sqlCon.Close();
        //    }
        //    catch (Exception ex)
        //    {


        //        return ex.ToString();

        //    }
        //    finally
        //    {
        //        if (sqlCon != null)
        //        {
        //            if (sqlCon.State == System.Data.ConnectionState.Open)
        //                sqlCon.Close();
        //        }
        //    }
        //    return "Success";
        //}


        //public string DeleteData(string strSessionID, string strCondition, string strTable)
        //{
        //    SqlConnection sqlCon = new SqlConnection();
        //    SqlCommand sqlCmd = new SqlCommand();
        //    try
        //    {

        //        sqlCon = GetConnectionString(strSessionID);
        //        if (sqlCon == null)
        //            return " You are not authorized to use the server";
        //        SqlCommand myCommand = new SqlCommand("PROC_DELETEDATA");
        //        myCommand.CommandType = CommandType.StoredProcedure;
        //        myCommand.Connection = sqlCon;
        //        myCommand.Parameters.AddWithValue("@strTableName", strTable);
        //        myCommand.Parameters.AddWithValue("@strCondition", strCondition);
        //        SqlDataAdapter da = new SqlDataAdapter(myCommand);
        //        DataSet data = new DataSet();
        //        da.Fill(data);
        //        da.Dispose();
        //        sqlCon.Close();
        //    }
        //    catch (Exception ex)
        //    {


        //        return ex.ToString();

        //    }
        //    finally
        //    {
        //        if (sqlCon != null)
        //        {
        //            if (sqlCon.State == System.Data.ConnectionState.Open)
        //                sqlCon.Close();
        //        }
        //    }
        //    return "Success";
        //}


    }
}