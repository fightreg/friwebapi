﻿using System;

namespace FRIWebAPI.App_Start
{

    public enum LogWriteLevel
    {
        ALL = 1,
        LIMITED = 2,
        NO_LOG = 3
    }
    public enum LogType
    {
        WARING = 1,
        ERROR = 2,
        EXCEPTION = 3,
        LOGIN_LOGOUT = 4,
        MESSAGE = 5,
        TRACE = 6,
        OTHER = 7

    }

    public class LogWriter
    {
        internal void WriteLog(string message, LogType logWriteType)
        {
            return;
        }

        internal void WriteToFile(string strLogToWrite)
        {
            return;
        }
    }
}