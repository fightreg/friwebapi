//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FRIWebAPI.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class Master_Messages
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Master_Messages()
        {
            this.Details_Follower = new HashSet<Details_Follower>();
            this.Master_Challenge = new HashSet<Master_Challenge>();
            this.Master_Cheer = new HashSet<Master_Cheer>();
            this.Master_Dispute = new HashSet<Master_Dispute>();
            this.Master_Spar = new HashSet<Master_Spar>();
            this.Master_Sponsership = new HashSet<Master_Sponsership>();
        }
    
        public long PK_MessageID { get; set; }
        public int FK_SenderID { get; set; }
        public string SenderName { get; set; }
        public int FK_RecipientID { get; set; }
        public string RecipientName { get; set; }
        public bool DelFlag { get; set; }
        public Nullable<byte> FK_MessageTypeID { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public Nullable<long> ParentMessageID { get; set; }
        public Nullable<int> FK_DeletedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> FK_CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> FK_ModifiedBy { get; set; }
        public Nullable<System.DateTime> DeletedDate { get; set; }
        public Nullable<bool> ReadFlag { get; set; }
        public string IconPath { get; set; }
        public string MessageStatus { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Details_Follower> Details_Follower { get; set; }
        public virtual Lookup_MessageType Lookup_MessageType { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Master_Challenge> Master_Challenge { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Master_Cheer> Master_Cheer { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Master_Dispute> Master_Dispute { get; set; }
        public virtual Master_User Master_User { get; set; }
        public virtual Master_User Master_User1 { get; set; }
        public virtual Master_User Master_User2 { get; set; }
        public virtual Master_User Master_User3 { get; set; }
        public virtual Master_User Master_User4 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Master_Spar> Master_Spar { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Master_Sponsership> Master_Sponsership { get; set; }
    }
}
