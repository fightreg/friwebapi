﻿using FRIWebAPI.Database;
using Newtonsoft.Json.Converters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;


enum ROLES : byte
{
    SuperAdmin = 1,
    Fighter,
    Official,
    Promoter,
    SanctioningBody,
    MemberCommission,
    Ambassador,
    Sponsor,
    GymOwner,
    Fan
};

enum PROFILES : byte
{
    Fan = 1,
    Fighter,
    Promoter,
    Commission,
    SanctioningBody,
    Sponsor,
    GymOwner,
    Official
};

enum MESSAGETYPE : byte
{
    SparRequest = 1,
    RequestDocumentation,
    ChallengeRequest,
    Message,
    Follower,
    Cheer,
    UpdateProfile,
    Sponsor,
    Dispute,
    Reply
}

namespace FRIWebAPI.ClassFiles
{
    public class Common
    {
        public static ArrayList _tokenList { get; set; }
        public bool CheckTokenValidity(string token)
        {
            FRIEntities DbContext = new FRIEntities();
            try
            {
                var isTokenExists = DbContext.TokenManagements.Any(a => a.TokenId == token&&a.ExpiredTime>=DateTime.UtcNow);
                if (isTokenExists)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool IsPublicToken(string token)
        {
            FRIEntities DbContext = new FRIEntities();
            try
            {
                var isTokenExists = DbContext.TokenManagements.Any(a => a.TokenId == token && a.ExpiredTime >= DateTime.UtcNow&&a.UserId==null);
                if (isTokenExists)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public String SaveToken(TokenData TokenData)
        {
            FRIEntities DbContext = new FRIEntities();
            string result = string.Empty;
            TokenManagement tokenManager = new TokenManagement();
            tokenManager.TokenId = TokenData.Guid;
            tokenManager.GeneratedTime = DateTime.UtcNow;
            tokenManager.ExpiredTime = DateTime.UtcNow.AddDays(30);
            DbContext.TokenManagements.Add(tokenManager);
            DbContext.SaveChanges();
                result = "Success";
                return result;
        }
        public String RemoveToken(int Id)
        {
            FRIEntities DbContext = new FRIEntities();
            string result = string.Empty;
            TokenManagement tokenManager = new TokenManagement();
            var isTokenExists = DbContext.TokenManagements.Any(a => a.UserId == Id);
            if (isTokenExists)
            {
                var doc = DbContext.TokenManagements.FirstOrDefault(a => a.UserId == Id);
               
                doc.TokenId = null;
                DbContext.SaveChanges();
                result = "Success";
            }
            if (result != null)
                return result;
            else
                return "Failed";
        }
        public String UpdateToken(TokenData TokenData,string oldtoken)
        {
            FRIEntities DbContext = new FRIEntities();
            string result = string.Empty;
            TokenManagement tokenManager = new TokenManagement();



            var isTokenExists = DbContext.TokenManagements.Any(a => a.TokenId == oldtoken);
            if (isTokenExists)
            {
                var doc = DbContext.TokenManagements.FirstOrDefault(a => a.TokenId == oldtoken);
               doc.UserId = TokenData.Userid;
                doc.TokenId = TokenData.Guid;
                doc.GeneratedTime = DateTime.UtcNow;
                tokenManager.ExpiredTime = DateTime.UtcNow.AddDays(1);
                DbContext.SaveChanges();
                result = "Success";
            }
            if (result != null)
                return result;
            else
                return "Failed";
        }
        public bool IsSuperAdmin(string token)
        {
            FRIEntities DbContext = new FRIEntities();
            bool result = false;
            TokenManagement tokenManager = new TokenManagement();
            result = (from tokn in DbContext.TokenManagements
                      join user in DbContext.Master_User
                      on tokn.UserId equals user.PK_UserID
                      where tokn.TokenId == token
                      select user.IsSuperAdmin).FirstOrDefault();
            return result;
        }
        public class ReturnResponse
        {
            public string status { get; set; }
            public string errors { get; set; }

            public string link { get; set; }

            public object result { get; set; }
        }
        public class PhofilePhoto
        {
            public string Imageurl { get; set; }
            public string Base64image { get; set; }


        }
        public class TokenData
        {
            public string Guid { get; set; }
            public int Userid { get; set; }
            public DateTime date { get; set; }
        }
        public class Constants
        {
            public const int Pendingstaus = -1;
            public const int InActive = 0;
            public const int Active = 1;
            public const int Rejected = 2;

            public const int commisionapproval = 1;
            public const int fightertemplate = 2;
            public const int commontemplate = 3;
            public const int santionbodyapproval = 4;
            public const int forgotpasswordtemplte = 5;

            public const int accepted = 1;
            public const int rejected = 2;
            public const int profileon = 3;
            public const int profileoff = 4;
            public const int featuredfighteron = 5;
            public const int featuredfighteroff = 6;
            public const int newcommission = 7;
            public const int newsantionbody = 8;
            public const int newfighter = 9;
            public const int forgotpasswd = 10;

            public const int SparAccepted = 11;
            public const int SparRejected = 12;
            public const int RequestAccepted = 15;
            public const int RequestRejected = 16;
            public const int RequestReceived = 22;
            public const int SparSent = 23;
            public const int SparReceived = 24;
            public const int newBlog = 9;
            public const int Follower = 5;

        }

        public void UpdateForgotpasswordstatus(int userid, bool status)
        {
            FRIEntities DbContext = new FRIEntities();
            Master_User tokenManager = new Master_User();
            var User = DbContext.Master_User.Any(a => a.PK_UserID == userid);
            if (User)
            {
                var ouser = DbContext.Master_User.FirstOrDefault(a => a.PK_UserID == userid);
                ouser.EnableResetPassword = status;
                DbContext.SaveChanges();
            }

        }

        public bool IsrequestedForForgotpasswd(int userid)
        {
            bool Isrequestd = false;
            FRIEntities DbContext = new FRIEntities();
            Master_User tokenManager = new Master_User();
            var User = DbContext.Master_User.Any(a => a.PK_UserID == userid&&a.EnableResetPassword==true);
            if (User)
                Isrequestd = true;
            return Isrequestd;
        }
    }
    public class MyDateFormatConverter : DateTimeConverterBase
    {
        public override object ReadJson(Newtonsoft.Json.JsonReader reader, Type objectType, object existingValue, Newtonsoft.Json.JsonSerializer serializer)
        {
            return DateTime.Parse(reader.Value.ToString());
        }

        public override void WriteJson(Newtonsoft.Json.JsonWriter writer, object value, Newtonsoft.Json.JsonSerializer serializer)
        {
            //writer.WriteValue(((DateTime)value).ToString("yyyy-MM-ddTHH:mm:ssZ"));
            writer.WriteValue(((DateTime)value).ToString("dd-MMM-yyyy"));

        }
    }


    public static class FRICrypto
    {
        public static string EncryptString(string message, string passphrase)
        {
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();

            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(passphrase));

            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();

            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;

            byte[] DataToEncrypt = UTF8.GetBytes(message);

            try
            {
                ICryptoTransform Encryptor = TDESAlgorithm.CreateEncryptor();
                Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }
            return Convert.ToBase64String(Results);
        }
         public static string decryption(string id)
        {
            string base64Encoded = id;
            string base64Decoded;
            byte[] data = System.Convert.FromBase64String(base64Encoded);
            base64Decoded = System.Text.ASCIIEncoding.ASCII.GetString(data);

            string keyString = ConfigurationManager.AppSettings["keyString"];
            string ivString = ConfigurationManager.AppSettings["ivString"];

            byte[] key = Encoding.ASCII.GetBytes(keyString);
            byte[] iv = Encoding.ASCII.GetBytes(ivString);

            using (var rijndaelManaged =
                    new RijndaelManaged
                    {
                        Key = key,
                        IV = iv,
                        Mode = CipherMode.CBC
                    })
            {
                rijndaelManaged.BlockSize = 128;
                rijndaelManaged.KeySize = 256;
                using (var memoryStream =
                       new MemoryStream(Convert.FromBase64String(base64Decoded)))
                using (var cryptoStream =
                       new CryptoStream(memoryStream,
                           rijndaelManaged.CreateDecryptor(key, iv),
                           CryptoStreamMode.Read))
                {
                    return new StreamReader(cryptoStream).ReadToEnd();
                }
            }
        }
        public static string Decrypt(string cipherString)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(cipherString);
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes("FRI@AOT"));
              
                hashmd5.Clear();
            
            
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;

            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(
                                 toEncryptArray, 0, toEncryptArray.Length);                
            tdes.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
        public static string DecryptString(string message, string passphrase)
        {
            string ip;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();


            var password = Convert.FromBase64String(message);

            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(passphrase));

            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();

            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;

            try
            {
                ICryptoTransform decryptor = TDESAlgorithm.CreateDecryptor();
                // Results = Decryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);

                byte[] original = decryptor.TransformFinalBlock(password, 0, password.Length);
                ip = System.Text.Encoding.Default.GetString(original);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }

            return ip;
        }
    }
  
    //public class PreflightRequestsHandler : DelegatingHandler
    //{
    //    protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
    //    {


    //        string[] allowedOrigin = new string[] { "http://localhost:59661", "http://localhost:4200", "https://fighterregistryserver.azurewebsites.net", "http://fri-fighterregistry.azurewebsites.net", "http://fighterregistryserver.azurewebsites.net", "http://fri-fighterregistry.azurewebsites.net/", "https://fri-fighterregistry.azurewebsites.net/", "https://fri-fighterregistry.azurewebsites.net/", "chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop" };

    //        if (request.Headers.Contains("Origin") && request.Method.Method.Equals("OPTIONS"))
    //        {

    //            var origin = HttpContext.Current.Request.Headers["Origin"];
    //            if (allowedOrigin.Contains(origin))
    //            {
    //                var response = new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
    //                // Define and add values to variables: origins, headers, methods (can be global)              
    //                response.Headers.Add("Access-Control-Allow-Headers", "accept, content-type");

    //                response.Headers.Add("Access-Control-Allow-Origin", origin);
    //                response.Headers.Add("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");

    //                var tsc = new TaskCompletionSource<HttpResponseMessage>();
    //                tsc.SetResult(response);
    //                return tsc.Task;
    //            }
    //            else
    //            {
    //                var response = new HttpResponseMessage { StatusCode = HttpStatusCode.ExpectationFailed };
    //                var tsc = new TaskCompletionSource<HttpResponseMessage>();
    //                tsc.SetResult(response);
    //                request.Dispose();
    //                return tsc.Task;

    //            }

    //        }


    //        return base.SendAsync(request, cancellationToken);
    //    }

    //}
}