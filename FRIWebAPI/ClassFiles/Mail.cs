﻿using FRIWebAPI.Database;
using FRIWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Reflection;
using System.Web;
using System.Xml.Linq;
using static FRIWebAPI.ClassFiles.Common;

namespace FRIWebAPI.ClassFiles
{
    public class Mail
    {
        FRIEntities dbContext = new FRIEntities();
        public void SelectMailTemplate(int userId,int iXMLType,int iMessageType,string username)
        {
            string content = string.Empty;
            string recipientEmail = string.Empty;
            string Email = string.Empty;
            string recipientName = string.Empty;
            string phone = string.Empty;
            string OrganizationName = string.Empty;
            string DistrictofAuthorization = string.Empty;
            string AuthorizingGovernmentorAgency = string.Empty;
            string CommissionName = string.Empty;
            string forgotpasswordlink = string.Empty;
            try
            {
                forgotpasswordlink = ConfigurationManager.AppSettings["Forgotpasswordlink"];
                switch (iMessageType)
                {
                    case Constants.accepted:
                        content = "Your request for registration with FightReg has been approved by Administrator.You can now login with your credentials and view your Profile.You can now  manage and update your profile details anytime,request fighters to update fight history and records,access all fighter information and receive PDF of fighter details,history and fight records.";
                        break;
                    case Constants.rejected:
                        content = "We are sorry that your request for registration with FightReg as a Commision has been rejected by FightReg; as we were not able to verify your details.";
                        break;
                    case Constants.featuredfighteroff:
                        content = "We are sorry that you won't be a Featured Fighter with FightReg.";
                        break;
                    case Constants.featuredfighteron:
                        content = "Congratulations!!! You become a Featured Fighter on FightReg.";
                        break;
                    case Constants.profileon:
                        content = "Your profile on FightReg as a Fighter has been enabled by Administrator.You can now login with your credentials and view your Profile. You can now centralize all your fight-related social media in your FightReg blog, update blogs with training and fight activities,challenge other fighters and request sparring opportunities with other fighters.Your fans will receive notifications of all your blog updates";
                        break;
                    case Constants.profileoff:
                        content = "We are sorry that your profile has been disabled by FightReg Administrator.";
                        break;
                    case Constants.newcommission:
                        content = "A new request has been generated from Fight Reg Portal.";
                        break;
                    case Constants.newsantionbody:
                        content = "A new request has been generated from Fight Reg Portal.";
                        break;
                    case Constants.newfighter:
                        content = "A new Fghter has requested to register with FightReg. Please verify the details, you activate or deactivate the profile.";
                        break;
                    case Constants.forgotpasswd:
                        //content = "Please click below link to reset your password. " + " </br>"+"<a href=" +"http://fri-fighterregistry.azurewebsites.net/#/password-reset/"+ userId+"</a>"+ " Click here";
                        content = "You recently requested to reset password for your FRI account. Please click the link to continue with Password reset.";
                        break;
                }
               
                XElement XTemp = XElement.Load(HttpContext.Current.Server.MapPath("~/bin/EmailTemplate.xml"));
                var queryCDATAXML = from element in XTemp.DescendantNodes()
                                    where element.NodeType == System.Xml.XmlNodeType.CDATA
                                    select element.Parent.Value.Trim();
                string BodyHtml = queryCDATAXML.ToList<string>()[iXMLType].ToString();
                
                    if (BodyHtml.Contains("{forgotpasswordlink}"))
                    BodyHtml = BodyHtml.Replace("{forgotpasswordlink}", forgotpasswordlink.ToString());
                if (BodyHtml.Contains("{UserId}"))
                    BodyHtml = BodyHtml.Replace("{UserId}",userId.ToString());
                if (BodyHtml.Contains("{Username}"))
                    BodyHtml = BodyHtml.Replace("{Username}", username);
                if (BodyHtml.Contains("{Subject}"))
                {
                    switch (iMessageType)
                    {
                        case Constants.forgotpasswd:
                            BodyHtml = BodyHtml.Replace("{Subject}", "Forgot Password");
                            break;
                        case Constants.newcommission:
                            BodyHtml = BodyHtml.Replace("{Subject}", "New Registration");
                            break;
                        case Constants.newsantionbody:
                            BodyHtml = BodyHtml.Replace("{Subject}", "New Registration");
                            break;
                        default:
                            BodyHtml = BodyHtml.Replace("{Subject}", "Fighter Registry Information");
                            break;

                    }
                }
                    
                if (BodyHtml.Contains("{Content}"))
                    BodyHtml = BodyHtml.Replace("{Content}", content);
                string Subject = "Fighter Registry Information";

                if (iXMLType == Constants.commontemplate|| iXMLType == Constants.forgotpasswordtemplte)
                {
                    UserData oUserData = (from user in dbContext.Master_User
                                             where user.PK_UserID==userId
                                             select new UserData
                                             {
                                                 Email=user.EmailAddress,
                                             }).FirstOrDefault();
                    if (oUserData != null)
                        recipientEmail = oUserData.Email;
                }
                else
                {
                     recipientEmail = ConfigurationManager.AppSettings["RecipientEmail"];
                     recipientName = ConfigurationManager.AppSettings["RecipientName"];
                }

                if (iXMLType == Constants.santionbodyapproval)
                {
                    UserData oUserData = (from user in dbContext.Master_User
                                          where user.PK_UserID == userId
                                          select new UserData
                                          {
                                              Email = user.EmailAddress,
                                              OrganizationName = user.Master_Profile.FirstOrDefault().SB_Title,
                                              phone = user.Phone,
                                          }).FirstOrDefault();
                    if (oUserData != null)
                    {
                        Email = oUserData.Email;
                        phone = oUserData.phone;
                        OrganizationName = oUserData.OrganizationName;
                    }
                    if (BodyHtml.Contains("{organizationname}"))
                        BodyHtml = BodyHtml.Replace("{organizationname}", OrganizationName);
                    if (BodyHtml.Contains("{Email}"))
                        BodyHtml = BodyHtml.Replace("{Email}", Email);
                    if (BodyHtml.Contains("{Phone}"))
                        BodyHtml = BodyHtml.Replace("{Phone}", phone);
                }
                else if(iXMLType==Constants.commisionapproval)

                {
                    UserData oUserData = (from user in dbContext.Master_User
                                          where user.PK_UserID == userId
                                          select new UserData
                                          {
                                              Email = user.EmailAddress,
                                              DistrictofAuthorization = user.Master_Profile.FirstOrDefault().CN_DistrictOfAuthorization,
                                              CommissionName=user.Master_Profile.FirstOrDefault().CN_Title,
                                              AuthorizingGovernmentorAgency=user.Master_Profile.FirstOrDefault().CN_AuthorizingBody,
                                              phone = user.Phone,
                                          }).FirstOrDefault();
                    if (oUserData != null)
                    {
                        Email = oUserData.Email;
                        phone = oUserData.phone;
                        DistrictofAuthorization = oUserData.DistrictofAuthorization;
                        CommissionName = oUserData.CommissionName;
                        AuthorizingGovernmentorAgency = oUserData.AuthorizingGovernmentorAgency;
                    }
                    if (BodyHtml.Contains("{DistrictofAuthorization}"))
                        BodyHtml = BodyHtml.Replace("{DistrictofAuthorization}", DistrictofAuthorization);

                    if (BodyHtml.Contains("{CommissionName}"))
                        BodyHtml = BodyHtml.Replace("{CommissionName}", CommissionName);

                    if (BodyHtml.Contains("{AuthorizingGovernmentorAgency}"))
                        BodyHtml = BodyHtml.Replace("{AuthorizingGovernmentorAgency}", AuthorizingGovernmentorAgency);

                    if (BodyHtml.Contains("{Email}"))
                        BodyHtml = BodyHtml.Replace("{Email}", Email);

                    if (BodyHtml.Contains("{Phone}"))
                        BodyHtml = BodyHtml.Replace("{Phone}", phone);
                }
                
                string Result = SendMail(recipientName, recipientEmail, Subject, BodyHtml);
            }
            catch(Exception ex)
            {

            }

        }
        public static string SendMail(string Name, string Receiver, string Subject, string Body)
        {
            string result = string.Empty;
            try
            {
                if (Receiver == null)
                    return "mail address cannot be empty";

                var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
                string username = smtpSection.Network.UserName;
                string password = smtpSection.Network.Password;
                string host = smtpSection.Network.Host;
                int port = smtpSection.Network.Port;
                bool enableSSL = smtpSection.Network.EnableSsl;

                var mClient = new SmtpClient();
                mClient.Host = host;
                mClient.Port = port;
                mClient.Credentials = new NetworkCredential(username, password);
                mClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                mClient.Timeout = 100000;
                mClient.EnableSsl = enableSSL;

                var msg = new MailMessage(username, Receiver);
                msg.IsBodyHtml = true;
                msg.Body = Body;
                msg.Subject = Subject;
                msg.From = new MailAddress("info@fightreg.com", "FightReg");
                mClient.Send(msg);
                msg.Dispose();

            }
            catch (SmtpFailedRecipientException ex)
            {
                result = ex.Message;
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }
        public string GenerateMailResponse(int iXMLType, int iMessageType)
        {
            string BodyHtml = string.Empty;
            string Response = string.Empty;
            switch (iMessageType)
            {
                case Constants.Active:
                    Response = "Activated new request on Figter Registry portal.";
                    break;
                case Constants.rejected:
                    Response = "Rejected new request on Figter Registry portal.";
                    break;
                case Constants.featuredfighteroff:
                    Response = "Activated  featured fighter on Figter Registry portal.";
                    break;
                case Constants.featuredfighteron:
                    Response = "Deactivated  featured fighter on Figter Registry portal.";
                    break;
                case Constants.profileon:
                    Response = "Activated fighter account on Figter Registry portal.";
                    break;
                case Constants.profileoff:
                    Response = "Deactivated fighter account on Figter Registry portal.";
                    break;
            }
            XElement XTemp = XElement.Load(HttpContext.Current.Server.MapPath("~/bin/EmailTemplate.xml"));
            var queryCDATAXML = from element in XTemp.DescendantNodes()
                                where element.NodeType == System.Xml.XmlNodeType.CDATA
                                select element.Parent.Value.Trim();
            BodyHtml = queryCDATAXML.ToList<string>()[iXMLType].ToString();
            if (BodyHtml.Contains("{Subject}"))
                BodyHtml = BodyHtml.Replace("{Subject}", "Fighter Registry Information");
            if (BodyHtml.Contains("{Content}"))
                BodyHtml = BodyHtml.Replace("{Content}", Response);
            return BodyHtml;
        }
    }
}