﻿using FRIWebAPI.ClassFiles;
using FRIWebAPI.Controllers;
using FRIWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Script.Serialization;
using static FRIWebAPI.ClassFiles.Common;

namespace FRIWebAPI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalConfiguration.Configuration.MessageHandlers.Add(new MessageHandlerTokenVerification());
            
        }
        public class MessageHandlerTokenVerification : DelegatingHandler
        {
            protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {

                // unauthorized response
                var Unauthorizedresponse = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                {
                    Content = new StringContent("Unauthorised response")
                };
                if (request.Method == HttpMethod.Get || request.Method == HttpMethod.Post || request.Method == HttpMethod.Options)
                {
                    string Email = string.Empty;
                    string Password = string.Empty;
                    string token = string.Empty;
                    //identify Api 
                    string sAPIpath = request.RequestUri.AbsolutePath.ToString();
                    var sAPIAction = sAPIpath.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                    if (sAPIAction.Length > 1 && (sAPIAction[1].ToUpper() == "COMMON"|| sAPIAction[1].ToUpper() == "USER" || sAPIAction[1].ToUpper() == "PROFILE" || sAPIAction[1].ToUpper() == "EVENTS") &&
                        (sAPIAction[2].ToUpper() == "DEFAULTTOKEN"|| sAPIAction[2].ToUpper() == "UPDATEPROFILESTATUS"
                        || sAPIAction[2].ToUpper() == "UPDATEFEATUREDFIGHTERSTATUS"|| sAPIAction[2].ToUpper() == "UPDATESTATUS"
                        || sAPIAction[2].ToUpper() == "UPLOADFORUMIMAGE"|| sAPIAction[2].ToUpper() == "UPLOADBLOGIMAGE"|| sAPIAction[2].ToUpper() == "UPLOADEVENTIMAGE"
                        ||sAPIAction[2].ToUpper() == "CREATEPROFILEPHOTO"|| sAPIAction[2].ToUpper() == "UPLOADPROFILEPHOTO"|| sAPIAction[2].ToUpper() == "UPLOADPHOTO"
                        || sAPIAction[2].ToUpper() == "UPLOADVIDEO" || sAPIAction[2].ToUpper() == "RESETPASSWORD"|| ((sAPIAction[1].ToUpper() == "PROFILE") && ((sAPIAction[2].ToUpper() == "GETOFFICIALPROFILE") || sAPIAction[2].ToUpper() == "GETPROMOTORPROFILE"||sAPIAction[2].ToUpper() == "GETSANCTIONINGBODYDETAILS"|| sAPIAction[2].ToUpper() == "GETFETUREDFIGHTERPROFILE"))))
                    {
                        var response = await base.SendAsync(request, cancellationToken);
                        if (sAPIAction[1].ToUpper() == "COMMON")
                        {
                            TokenData tkndata = new TokenData();
                            Common oCommon = new Common();

                            string guid = Guid.NewGuid().ToString();
                            if (_tokenList == null)
                                _tokenList = new System.Collections.ArrayList();

                            _tokenList.Add(guid);

                            response.Headers.Add("accessToken", guid);
                            tkndata.Guid = guid;
                            string SaveToken = oCommon.SaveToken(tkndata);
                        }
                        return response;

                    }
                    else
                    { 
                    if (request.Headers.Contains("Token"))
                    {
                        token = request.Headers.GetValues("Token").First();
                        if (new Common().CheckTokenValidity(token))
                        {
                                HttpResponseMessage response = new HttpResponseMessage();
                                if (new Common().IsPublicToken(token))
                                {
                                    if(sAPIAction.Length > 1 && (sAPIAction[1].ToUpper() == "LOOKUP"|| sAPIAction[1].ToUpper() == "PROFILE"||( sAPIAction[1].ToUpper() == "COMMON"&& sAPIAction[2].ToUpper() == "FORGOTPASSWORD")))
                                     {
                                        response = await base.SendAsync(request, cancellationToken);
                                    }
                                   else if (sAPIAction.Length > 1 && (sAPIAction[1].ToUpper() == "USER" || sAPIAction[1].ToUpper() == "EVENTS" ||
                                        sAPIAction[1].ToUpper() == "FIGHTERHOME" || sAPIAction[1].ToUpper() == "COMMON"|| sAPIAction[1].ToUpper() == "PROFILE")
                                        && (sAPIAction[2].ToUpper() == "USERSIGININ" || sAPIAction[2].ToUpper() == "GETUSEREVENTS" || sAPIAction[2].ToUpper() == "GETPROFILEDETAILS"
                                        || sAPIAction[2].ToUpper() == "GETUPCOMMINGEVENTS"|| sAPIAction[2].ToUpper() == "FULLTEXTSEARCH"|| 
                                        sAPIAction[2].ToUpper() == "GETFETUREDFIGHTERPROFILE"|| sAPIAction[2].ToUpper() == "GETEVENTDETAILS"
                                        || sAPIAction[2].ToUpper() == "GETUSERSELECTEDPROFILES"|| sAPIAction[2].ToUpper() == "GETPROMOTORPROFILE"||
                                        sAPIAction[2].ToUpper() == "EMAILDUPLICATIONCHECK"|| sAPIAction[2].ToUpper() == "CREATEFAN"|| sAPIAction[2].ToUpper() == "CREATECOMMISSION"))//add apis here
                                    {
                                        response = await base.SendAsync(request, cancellationToken);
                                    }
                                    else
                                    {
                                        ReturnResponse oReturnResponse = new ReturnResponse();
                                        response = new HttpResponseMessage();
                                        oReturnResponse.status = "Failed";
                                        oReturnResponse.errors = "Authorization has been denied for this request.";
                                        string tempResponse = new JavaScriptSerializer().Serialize(oReturnResponse).Replace(@"\", "");
                                        response.Content = new StringContent(tempResponse, Encoding.UTF8, "application/json");
                                        return response;
                                    }
                                }
                                else
                                {
                                    response = await base.SendAsync(request, cancellationToken);
                                }

                                TokenData tkndata = new TokenData();
                            Common oCommon = new Common();
                                if (sAPIAction.Length > 1 && sAPIAction[1].ToUpper() == "USER" && (sAPIAction[2].ToUpper() == "USERSIGININ" || sAPIAction[2].ToUpper() == "USERSIGNOUT")
                                    && (((ReturnResponse)((ObjectContent)response.Content).Value).result != null))
                                 {  
                                        string guid = Guid.NewGuid().ToString();
                                        if (_tokenList == null)
                                            _tokenList = new System.Collections.ArrayList();

                                        _tokenList.Add(guid);

                                        response.Headers.Add("accessToken", guid);
                                        tkndata.Guid = guid;
                                        if (sAPIAction[2].ToUpper() == "USERSIGININ")
                                        {
                                            tkndata.Userid = ((UserSignIn)((ReturnResponse)((ObjectContent)response.Content).Value).result).PK_UserID;
                                            string SaveToken = oCommon.UpdateToken(tkndata, token);
                                        }
                                        else if (sAPIAction[2].ToUpper() == "USERSIGNOUT")
                                        {
                                            string SaveToken = oCommon.SaveToken(tkndata);
                                        }
                                    
                                 }
                            return response;
                        }
                        else
                        {

                            ReturnResponse oReturnResponse = new ReturnResponse();
                            HttpResponseMessage response = new HttpResponseMessage();
                            oReturnResponse.status = "Failed";
                            oReturnResponse.errors = "Token Expired";
                            string tempResponse = new JavaScriptSerializer().Serialize(oReturnResponse).Replace(@"\", "");
                            response.Content = new StringContent(tempResponse, Encoding.UTF8, "application/json");
                            return response;
                        }
                    }
                    else
                    {

                        ReturnResponse oReturnResponse = new ReturnResponse();
                        HttpResponseMessage response = new HttpResponseMessage();
                        oReturnResponse.status = "Failed";
                        oReturnResponse.errors = "Authorization has been denied for this request.";
                        string tempResponse = new JavaScriptSerializer().Serialize(oReturnResponse).Replace(@"\", "");
                        response.Content = new StringContent(tempResponse, Encoding.UTF8, "application/json");
                        return response;

                    }
                }
                    
                }
                return null;
            }

           
        }
        protected void Application_BeginRequest()
        {
            //string[] allowedOrigin = new string[] { "http://localhost:59661", "http://localhost:4200", "https://fighterregistryserver.azurewebsites.net", "http://fri-fighterregistry.azurewebsites.net", "http://fighterregistryserver.azurewebsites.net", "http://fri-fighterregistry.azurewebsites.net/", "https://fri-fighterregistry.azurewebsites.net/", "https://fri-fighterregistry.azurewebsites.net/", "https://fightregproduction.azurewebsites.net", "chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop" };

            string[] allowedOrigin = new string[] { "http://localhost:59661", "http://localhost:4200", "https://fighterregistryserver.azurewebsites.net", "http://fighterregistryserver.azurewebsites.net", "https://fri-fighterregistry.azurewebsites.net", "http://fri-fighterregistry.azurewebsites.net", "https://fighterregistryuat-api.azurewebsites.net", "http://fighterregistryuat-api.azurewebsites.net", "https://fighterregistryuat.azurewebsites.net", "http://fighterregistryuat.azurewebsites.net", "https://fightregproductionapi.azurewebsites.net", "http://fightregproductionapi.azurewebsites.net", "https://fightreg.com", "http://fightreg.com", "https://www.fightreg.com", "http://www.fightreg.com", "http://uat.ladaee.com", "https://uat.ladaee.com", "https://wwww.uat.ladaee.com", "http://wwww.uat.ladaee.com", "www.fightreg.com", "chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop" };

            var origin = HttpContext.Current.Request.Headers["Origin"];
            var Accept = HttpContext.Current.Request.Headers["Accept"]; //application/json
            var Authorization = HttpContext.Current.Request.Headers["Authorization"];//friclientmodule
            var ContentType = HttpContext.Current.Request.Headers["Content-Type"];//application/json


            if (origin != null)
            {
                // if(Accept == "application/json" && Authorization == "friclientmodule" && ContentType== "application/json")
                // {
                if (allowedOrigin.Contains(origin))
                {
                    HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", origin);

                }
                else
                    Response.Close();
                // }
                // else
                //    Response.Close();
            }

        }
    }
}
