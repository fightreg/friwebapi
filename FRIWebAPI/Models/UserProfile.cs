﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using FRIWebAPI.Database;

namespace FRIWebAPI.Models
{

    public class UserEvents
    {
        public int PK_EventID { get; set; }       
        public int FK_CreatedBy { get; set; }
        public string EventTitle { get; set; }       
        public string EventType { get; set; }
        public System.DateTime Date { get; set; }
        public string Location { get; set; }
        public string LocationDetails { get; set; }
        public string Description { get; set; }
        public string Creator { get; set; }
        public string Banner { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public Nullable<int> FK_Country { get; set; }
        public Nullable<int> FK_ProvinceOrState { get; set; }
        public string Street { get; set; }
        public string PostalOrZipCode { get; set; }

        public System.DateTime CreatedDate { get; set; }

        public string DateString { get; set; }
    }
    public class FanInfo
    {
        

        public int PK_UserID { get; set; }
        public int PK_UserProfileID { get; set; }

        public string LastName { get; set; }
        public string LegalFirstName { get; set; }
        public string LegalMiddleName { get; set; }
        public string Phone { get; set; }

        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string SecurityQuestion { get; set; }
        public string SecurityAnswer { get; set; }

        public Nullable<int> FK_Country { get; set; }
        public Nullable<int> FK_ProvinceOrState { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostalOrZipCode { get; set; }
        public int? profilestatus { get; set; }



        public bool DelFlag { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> FK_CreatedBy { get; set; }
      


    }


    public class DemoGraphicInfo
    {
        

        //Demographic Information
        public int PK_UserID { get; set; }
        public string LastName { get; set; }
        public string LegalFirstName { get; set; }
        public string LegalMiddleName { get; set; }
        public string Gender { get; set; }
       // public System.DateTime DOB { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string PersonalWebsite { get; set; }
        public Nullable<int> FK_Country { get; set; }
        public Nullable<int> FK_ProvinceOrState { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostalOrZipCode { get; set; }
        public string Phone { get; set; }
        public List<SocialMediaRecord> SocialMediaRecords { get; set; }
        public bool DelFlag { get; set; }
        public string ProfilePhotoPath { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> FK_CreatedBy { get; set; }
        public bool IsAmbassador { get; set; }
        public string SecurityQuestion { get; set; }
        public string SecurityAnswer { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyContactNumber { get; set; }
        public int? profilestatus { get; set; }
        public string Base64Image { get; set; }

    }

    public class CommissionInfo
    {
        internal int? profilestatus;

        public int PK_UserID { get; set; }
        public int PK_UserProfileID { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string PriContactLastName { get; set; }
        public string PriContactLegalFirstName { get; set; }
        public string PriContactLegalMiddleName { get; set; }
        public string SecContactLastName { get; set; }
        public string SecContactLegalFirstName { get; set; }
        public string SecContactLegalMiddleName { get; set; }
        public string DistrictOfAuthorization { get; set; }
        public string SecContactEmail { get; set; }
        public string AuthorizingBody { get; set; }
        public string PriContactEmail { get; set; }
        public string ProfilePhotoPath { get; set; }
        public string Title { get; set; }
        public string Website { get; set; }
        public bool termsCondtnCheck { get; set; }
        public bool codeOfconduct { get; set; }
        public string Description { get; set; }
        public Nullable<int> FK_Country { get; set; }
        public Nullable<int> FK_ProvinceOrState { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostalOrZipCode { get; set; }

       
        public string SecurityQuestion { get; set; }
        public string SecurityAnswer { get; set; }

        public List<SocialMediaRecord> SocialMediaRecords { get; set; }
        public bool DelFlag { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> FK_CreatedBy { get; set; }

        public string Phone { get; set; }


    }

    public class SanctioningBody
    {


        //Commission/SanctioningBody Information
        public int PK_UserID { get; set; }
        public int PK_UserProfileID { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string PriContactLastName { get; set; }
        public string PriContactLegalFirstName { get; set; }
        public string PriContactLegalMiddleName { get; set; }
        public string SecContactLastName { get; set; }
        public string SecContactLegalFirstName { get; set; }
        public string SecContactLegalMiddleName { get; set; }
        public string ProfilePhotoPath { get; set; }
        public string SecContactEmail { get; set; }
        public string WebAddres { get; set; }
        public string Website { get; set; }
        public string PriContactEmail { get; set; }
        public string Location { get; set; }
        public string FullName { get; set; }
        public string Title { get; set; }
        public bool termsCondtnCheck { get; set; }
        public bool codeOfconduct { get; set; }
        public string Description { get; set; }
        public Nullable<int> FK_Country { get; set; }
        public Nullable<int> FK_ProvinceOrState { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostalOrZipCode { get; set; }
        public string Address { get; set; }

        public string SecurityQuestion { get; set; }
        public string SecurityAnswer { get; set; }

        public List<SocialMediaRecord> SocialMediaRecords { get; set; }
        public bool DelFlag { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> FK_CreatedBy { get; set; }

        public string Phone { get; set; }
        public int? profilestatus { get; set; }

    }


    public class Fan
    {

        //Fan
        public int PK_UserID { get; set; }
        public int PK_UserProfileID { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }      
        public Nullable<int> FK_Country { get; set; }
        public Nullable<int> FK_ProvinceOrState { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostalOrZipCode { get; set; }
        public string SecurityQuestion { get; set; }
        public string SecurityAnswer { get; set; }      
        public bool DelFlag { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> FK_CreatedBy { get; set; }
        public string Phone { get; set; }

    }

    public class SocialMediaRecord
    {
        public int PK_UserSocialMediaID { get; set; }
        public byte FK_SocialMediaID { get; set; }
        public int FK_UserID { get; set; }
        public string SocialMedia { get; set; }
        public string URL { get; set; }
        public bool DelFlag { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int FK_CreatedBy { get; set; }
        public string IconPath { get; set; }

    }

    public class UserProfile
    {
        public int PK_ProfileMapID { get; set; }
        //  public int FK_UserID { get; set; }
        public byte FK_ProfileID { get; set; }
        public string ProfileDescription { get; set; }
        public bool DelFlag { get; set; }

    }

    public class UserRole
    {
        public int PK_RoleMapID { get; set; }
        //  public int FK_UserID { get; set; }
        public byte FK_RoleID { get; set; }
        public string RoleDescription { get; set; }
        public bool DelFlag { get; set; }

    }

    public class UserDetails
    {
        public int PK_UserID { get; set; }
        public int PK_UserProfileID { get; set; }
        public List<UserProfile> UserProfiles { get; set; }
        public DemoGraphicInfo DemoGraphicInfo { get; set; }
        public FighterProfile FighterProfile { get; set; }
        public PromotorProfile PromotorProfile { get; set; }
        public SanctioningBodyProfile SanctioningBodyProfile { get; set; }
        public CommissionProfile CommissionProfile { get; set; }
        public OfficialProfile OfficialProfile { get; set; }
    }



    public class CreateUserDetails
    {
        public int PK_UserID { get; set; }
        public int PK_UserProfileID { get; set; }
      
    }

    public class FeturedFighterProfile
    {
       

        public int PK_UserID { get; set; }
        public int PK_UserProfileID { get; set; }

        public string Name { get; set; }

        public string FighterName { get; set; }
        public DateTime? DOB { get; set; }
        
        public string Location { get; set; }

        public int CheerCount { get; set; }

        public string FR_Bio { get; set; }
        public string FR_PrimaryDiscipline { get; set; }
        public string FR_SecondaryDiscipline { get; set; }
        public string FR_TertiaryDiscipline { get; set; }
        public string FR_PrimarySkillLevel { get; set; }
        public string EmailAddress { get; set; }

        public int Wins { get; set; }
        public int Losses { get; set; }
        public int Draws { get; set; }
        public string Height { get; set; }
        public string Reach { get; set; }
        public string FR_CurrentWeight { get; set; }
        public int Age { get; set; }

        public string LegReach { get; set; }

        public string ProfilePhotoPath { get; set; }

        public bool IsCheered { get; set; }
        public bool IsTracked { get; set; }

        public List<SocialMediaRecord> SocialMediaRecords { get; set; }
        public List<FighterPhoto> FighterPhotoDetails { get; set; }
        public List<FighterVideo> FighterVideoDetails { get; set; }
        public bool IsFeaturedFighter { get; internal set; }
        public int? profilestatus { get; set; }
    }
 
    public class OfficialDetailsView
    {
        

        public int PK_UserID { get; set; }
        public int PK_UserProfileID { get; set; }
        public string Name { get; set; }
        public string FighterName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string WebSite { get; set; }
        public string Country { get; set; }
        public string State { get; set; }   
        public string Location { get; set; }
        public string Address { get; set; }
        public string DistrictOfAuthorization { get; set; }
        public int? profilestatus { get; set; }
        public string ProfilePhotoPath { get; set; }
        public List<OfficialDetail> OfficialDetails { get; set; }
        public List<SocialMediaRecord> SocialMediaRecords { get; set; }
        public string commisionarname { get; set; }
    }

    public class PromotorDetailsView
    {
        

        public int PK_UserID { get; set; }
        public int PK_UserProfileID { get; set; }
        public string Name { get; set; }
        public string FighterName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string WebSite { get; set; }
        public string Title { get; set; }
        public string EventNameOrSeries { get;set;}
        public string Location { get; set; }
        public string ProfilePhotoPath { get; set; }
        public List<SocialMediaRecord> SocialMediaRecords { get; set; }
        public int? profilestatus { get; set; }
    }

    public class UserDetailsView
    {
        public int PK_UserID { get; set; }
        public int PK_UserProfileID { get; set; }
        public string Name { get; set; }
        public string FighterName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string WebSite { get; set; }
        public string Location { get; set; }
        public string ProfilePhotoPath { get; set; }

        public List<SocialMediaRecord> SocialMediaRecords { get; set; }
    }


    public class CommissionDetailsView
    {
        

        public int PK_UserID { get; set; }
        public int PK_UserProfileID { get; set; }
        public string Name { get; set; }
        public string FighterName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string WebSite { get; set; }
        public string Location { get; set; }
        public string ProfilePhotoPath { get; set; }
        public string RepresentCommission { get; set; }
        public string DistrictofAuthorization { get; set; }
        public int? profilestatus { get; set; }

        public List<SocialMediaRecord> SocialMediaRecords { get; set; }
    }


    public class SanctioningBodyView
    {
        public int PK_UserID { get; set; }
        public int PK_UserProfileID { get; set; }
        public string Name { get; set; }
        public string FighterName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string WebSite { get; set; }
        public string Location { get; set; }
        public string ProfilePhotoPath { get; set; }


        public string Title { get; set; }

        public string Style { get; set; }

        public List<SocialMediaRecord> SocialMediaRecords { get; set; }
    }

    public class UserProfileNotUsed
    {
        public int PK_UserProfileID { get; set; }
        public int FK_UserID { get; set; }

        //Other Details
        public string Description { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> FK_ModifiedBy { get; set; }
        public Nullable<System.DateTime> DeletedDate { get; set; }
        public Nullable<int> FK_DeletedBy { get; set; }
        public bool IsAcceptedTerms { get; set; }
        public bool AcceptChallenge { get; set; }
        public bool IsSubscribeToEvents { get; set; }
    }

    public class FighterHistoryRecord
    {
        public int PK_HistoryID { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Event { get; set; }
        public string OpponentName { get; set; }
        public string OpponentGym { get; set; }
        public string WeightCategory { get; set; }
        public string Result { get; set; }
        public string Decision { get; set; }
        public string TitleWon { get; set; }
        public string Venue { get; set; }
        public Nullable<int> FK_DeletedBy { get; set; }
        public bool DelFlag { get; set; }
        public string StyleOfFight { get; set; }
        public Nullable<int> ScheduledRounds { get; set; }
        public Nullable<int> StoppageRound { get; set; }
        public Nullable<bool> AmateurOrProfessional { get; set; }
        public string TournamentStyle { get; set; }


    }

    public class FighterProfile
    {
        //Fighter Details
        public bool IsFighter { get; set; }
        public string FighterName { get; set; }
        public bool IsFeaturedfighter { get; set; }
        public string FR_PrimaryDiscipline { get; set; }
        public string FR_SecondaryDiscipline { get; set; }
        public string FR_TertiaryDiscipline { get; set; }
        public string FR_TertiarySkillLevel { get; set; }
        public string FR_PrimarySkillLevel { get; set; }
        public string FR_SecondarySkillLevel { get; set; }
        public Nullable<bool> FR_IsProfessional { get; set; }
        public Nullable<bool> FR_IsAvailableMatch { get; set; }
        public Nullable<bool> FR_IsAvailableSparing { get; set; }
        public string FR_Height { get; set; }
        public string FR_HeightUnit { get; set; }
        public string FR_ReachUnit { get; set; }

        public string FR_LegReachUnit { get; set; }
        public string FR_LegReach { get; set; }
        public string FR_Reach { get; set; }
        public string FR_EyeColor { get; set; }
        public string FR_FighterStatus { get; set; }
        public string FR_CurrentWeight { get; set; }
        public string FR_CurrentWeightUnit { get; set; }
        public string FR_MaxWeight { get; set; }
        public string FR_MaxWeightUnit { get; set; }
        public string FR_MinWeightUnit { get; set; }
        public string FR_HairColor { get; set; }
        public string FR_MinWeight { get; set; }
        public string FR_Bio { get; set; }
        public DateTime FR_DOB { get; set; }
        public Nullable<int> FR_Citizenship { get; set; }
        public string FR_EmergencyContactName { get; set; }
        public string FR_EmergencyContactPhone { get; set; }

        public int Wins { get; set; }
        public int Losses { get; set; }
        public int Draws { get; set; }
        public int TotalFights { get; set; }
        public int KO { get; set; }
        public int TKO { get; set; }
        public int CheerCount { get; set; }

        public bool FR_IsOwner { get; set; }
        public string GO_TrainingFacilityName { get; set; }
        public string GO_TrainerName { get; set; }
        public string GO_Phone { get; set; }
        public Nullable<int> GO_FK_Country { get; set; }
        public Nullable<int> GO_FK_ProvinceOrState { get; set; }
        public string GO_Street { get; set; }
        public string GO_Website { get; set; }
        
        public string GO_City { get; set; }
        public string GO_PostalOrZipCode { get; set; }
        public string GO_Facebook { get; set; }

        public int FR_HeightInFt { get; set; }
        public int FR_HeightInInch { get; set; }
        public decimal FR_HeightCM { get; set; }

        public List<FighterHistoryRecord> FighterHistoryRecords { get; set; }
        public List<FighterPhoto> FighterPhotoDetails { get; set; }

        public List<FighterVideo> FighterVideoDetails { get; set; }
    }

    public class PromotorProfile
    {

        public string PR_Title { get; set; }
        public string PR_Description { get; set; }
        public string PR_Address { get; set; }
        public string PR_Phone { get; set; }
        public string PR_Webaddress { get; set; }
        public string PR_EventNameOrSeries { get; set; }
        public string PR_PriContactLastName { get; set; }
        public string PR_PriContactLegalFirstName { get; set; }
        public string PR_PriContactLegalMiddleName { get; set; }
        public string PR_SecContactLastName { get; set; }
        public string PR_SecContactLegalFirstName { get; set; }
        public string PR_SecContactLegalMiddleName { get; set; }
    }

    public class SanctioningBodyProfile
    {

        public Nullable<int> SB_FK_SanctioningBodyID { get; set; }
        public string SB_SanctioningBody { get; set; }
        public string SB_Title { get; set; }
        public string SB_Style { get; set; }
        public string SB_WebAddress { get; set; }
        public string SB_Description { get; set; }
        public string SB_address { get; set; }
        public string SB_Phone { get; set; }
        public string SB_PrimaryContact { get; set; }
        public string SB_PrimaryEmail { get; set; }
        public string SB_SecondaryContact { get; set; }
        public string SB_SecondaryEmail { get; set; }
        public string SB_LogoPath { get; set; }
        public string SB_OptionalPhone { get; set; }
        public string SB_PriContactLastName { get; set; }
        public string SB_PriContactLegalFirstName { get; set; }
        public string SB_PriContactLegalMiddleName { get; set; }
        public string SB_SecContactLastName { get; set; }
        public string SB_SecContactLegalFirstName { get; set; }
        public string SB_SecContactLegalMiddleName { get; set; }
    }

    public class CommissionProfile
    {
        public Nullable<int> CN_FK_CommissionID { get; set; }
        public string CN_Commission { get; set; }
        public string CN_Title { get; set; }
        public string CN_Description { get; set; }
        public string CN_Address { get; set; }
        public string CN_phone { get; set; }
        public string CN_webaddress { get; set; }
        public string CN_SecondaryEmail { get; set; }
        public string CN_PrimaryContact { get; set; }
        public string CN_PrimaryEmail { get; set; }
        public string CN_SecondaryContact { get; set; }
        public string CN_LogoPath { get; set; }

        public string CN_DistrictOfAuthorization { get; set; }
        public string CN_AuthorizingBody { get; set; }
        public string CN_PrimaryPhone { get; set; }
        public string CN_OptionalPhone { get; set; }
        public string CN_PriContactLastName { get; set; }
        public string CN_PriContactLegalFirstName { get; set; }
        public string CN_PriContactLegalMiddleName { get; set; }
        public string CN_SecContactLastName { get; set; }
        public string CN_SecContactLegalFirstName { get; set; }
        public string CN_SecContactLegalMiddleName { get; set; }
    }

    public class OfficialProfile
    {
        public int PK_UserProfileID { get; set; }
        public List<OfficialDetail> OfficialDetails { get; set; }
    }

    public class OfficialDetail
    {
        public int PK_OfficialProfileID { get; set; }
        public string Description { get; set; }
        public string OfficialLevel { get; set; }
        public string CertID { get; set; }
        public string Licence { get; set; }
        public Nullable<int> NumberofFights { get; set; }
        public Nullable<int> Citizenship { get; set; }
        public string EmergencyContactName { get; set; }
        public string TypeOfOfficial { get; set; }
        public string Discipline { get; set; }
        public string LicensedBy { get; set; }

        public bool DelFlag { get; set; }
        public int FK_ProfileID { get; set; }

    }

    public class FighterPhoto
    {
        public int PK_FighterPhotoID { get; set; }
        public string PhotoPath { get; set; }
        public Nullable<int> FK_DeletedBy { get; set; }
        public bool DelFlag { get; set; }

    }

    public class FighterVideo
    {
        public int PK_FighterVideoID { get; set; }
        public string VideoLink { get; set; }
        public string VideoDescription { get; set; }
        public Nullable<int> FK_DeletedBy { get; set; }
        public bool DelFlag { get; set; }

    }
 
     public class FullTextSearch
    {
        public string searchString { get; set; }
        public bool IsSuperadmin { get; set; }
    }
        public class FighterSearch
    {
        public FighterSearch()
        {
            Gender = null;
            AcceptsSparingPartner = -1;
            AcceptMatch = -1;
            HeightFrom = -1;
            HeightTo = -1;
            HeightUnit = null;
            WeightFrom = -1;
            WeightTo = -1;
            WeightUnit = null;
            Discipline = null;
            SkillLevel = null;
        }
        public string FighterName { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Gender { get; set; }
        public int AcceptsSparingPartner { get; set; }
        public int AcceptMatch { get; set; }
        public int HeightFrom { get; set; }
        public int HeightTo { get; set; }
        public string HeightUnit { get; set; }
        public int WeightFrom { get; set; }
        public int WeightTo { get; set; }
        public string WeightUnit { get; set; }
        public string Discipline { get; set; }
        public string SkillLevel { get; set; }
        public bool IsSuperadmin { get; set; }
    }

    public class PromoterSearch
    {
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Title { get; set; }
        public string EventName { get; set; }
        public string ContactName { get; set; }
        public bool IsSuperadmin { get; set; }
    }
    public class OfficialSearch
    {
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string OfficialLevel { get; set; }
        public string TypeOfOfficial { get; set; }
        public string Discipline { get; set; }
        public bool IsSuperadmin { get; set; }

    }

    public class AmbassadorSearch
    {
        public string City { get; set; }
        public string Name { get; set; }
        public string State { get; set; }
        public string Gender { get; set; }
        public string Country { get; set; }
        public bool IsSuperadmin { get; set; }

    }

    public class CommissionSearch
    {
        public string City { get; set; }
        public string ContactName { get; set; }
        public string State { get; set; }
        public string Title { get; set; }
        public string PrimaryNumber { get; set; }
        public string Country { get; set; }
        public string webaddress { get; set; }
        public bool IsSuperadmin { get; set; }
    }
    public class SactionBodySearch
    {
        public string City { get; set; }
        public string ContactName { get; set; }
        public string State { get; set; }
        public string Title { get; set; }
        public string PrimaryNumber { get; set; }
        public string Country { get; set; }
        public string Style { get; set; }
        public string WebAddress { get; set; }
        public bool IsSuperadmin { get; set; }
    }

    public class FanSearch
    {
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public bool IsSuperadmin { get; set; }

    }


    public class ChangePassword
    {
        public int PK_UserID { get; set; }
        public string NewPassword { get; set; }
        public string CurrentPassword { get; set; }

    }


    public class ForgotPassword
    {
        public string SecurityQuestion { get; set; }
        public string QuestionAnswer { get; set; }
        public string EmailAddress { get; set; }
        public bool CheckQuestion { get; set; }
    }

    public class UserBlog
    {
        public int PK_Blog { get; set; }
        public int FK_UserID { get; set; }
        public string Name { get; set; }
        public string Blog { get; set; }
        public string BlogHeading { get; set; }
        public System.DateTime CreatedDate { get; set; }

        public string CreatedDateString { get; set; }
    }
    public class BlogComments
    {

        public int PK_BlogCommentID { get; set; }
    public Nullable<int> FK_BlogID { get; set; }
    public Nullable<int> FK_UserID { get; set; }
    public string Name { get; set; }
    public bool DelFlag { get; set; }
    public string Comment { get; set; }
    public System.DateTime CreatedDate { get; set; }

        public string CreatedDateString { get; set; }
    }

    public class UserForum
    {

        public int PK_ForumID { get; set; }
        public byte FK_ForumTypeID { get; set; }
        public int FK_UserID { get; set; }
        public string Forum { get; set; }
        public string ForumTopic { get; set; }
        public string Name { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedDateString { get; set; }
        public List<ForumComments> ForumComments { get; set; }
        public string ProfilePhotoPath { get; set; }
    }

    public class ForumComments
    {

        public int PK_ForumCommentID { get; set; }
        public Nullable<int> FK_ForumD { get; set; }
        public Nullable<int> FK_UserID { get; set; }
        public string Name { get; set; }
        public bool DelFlag { get; set; }
        public string Comment { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedDateString { get; set; }
        public string ProfilePhotoPath { get; set; }


    }
}