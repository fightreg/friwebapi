﻿using System.Collections;
using System.Collections.Generic;

namespace FRIWebAPI.Models
{
    public class SearchResult
    {
        public int PK_userid { get; set; }

        public string Username { get; set; }

        public string Profilephotopath { get; set; }

        public List<string> Roles { get; set; }

        public string Userlocation { get; set; }

        public SearchResult()
        {
            Roles = new List<string>();
        }
    }
}