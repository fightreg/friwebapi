﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FRIWebAPI.Models
{
    public class UserSignIn
    {
        public int PK_UserID { get; set; }
        public string LastName { get; set; }
        public string LegalFirstName { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string ProfilePhotoPath { get; set; }
        public bool IsSuperadmin { get; internal set; }
        public int? ProfileStatus { get; internal set; }
        
    }
    public class LoginInfo
    {
        public string EmailAddress { get; set; }
        public string Password { get; set; }

    }
    
    public class UserData
    {
        public string Email { get; set; }
        public string OrganizationName { get; set; }
        public string phone { get; set; }
        public string Name { get; set; }
        public string DistrictofAuthorization { get; internal set; }
        public string CommissionName { get; internal set; }
        public string AuthorizingGovernmentorAgency { get; internal set; }
    }

    public class FilterMessage
    {
        public int UserID { get; set; }
        public bool AcceptedMessage { get; set; }
        public bool RejectedMessage { get; set; }
        public bool ReadMessage { get; set; }
        public bool UnreadMessage { get; set; }
        public bool AllMessage { get; set; }

    }



    public class UserMessage
    {
       
        public UserMessage()
        {
           
        }

        public Nullable<System.DateTime> SendDate { get; set; }
        public Nullable<byte> FK_MessageTypeID { get; set; }
        public long PK_MessageID { get; set; }
        public int FK_SenderID { get; set; }
        public string SenderName { get; set; }
        public int FK_RecipientID { get; set; }
        public string RecipientName { get; set; }      
       
        public string Subject { get; set; }
        public string Message { get; set; }
        public Nullable<long> ParentMessageID { get; set; }

        public Nullable<bool> ReadFlag { get; set; }
        public string IconPath { get; set; }

        public string MessageStatus { get; set; }


    }



    public class ChallengeBoard
    {

        public long PK_MessageID { get; set; }
        public int FK_SenderID { get; set; }
        public string SenderName { get; set; }
        public int FK_RecipientID { get; set; }
        public string RecipientName { get; set; }

        public string ProfilePhotoSender { get; set; }
        public string ProfilePhotoRecipient { get; set; }

        public Nullable<System.DateTime> SendTime { get; set; }

        public Nullable<System.DateTime> AcceptedTime { get; set; }


        public string SendTimeString { get; set; }

        public string AcceptedTimeString { get; set; }

        public string Message { get; set; }

        public string MessageType { get; set; }

    }
   

    public class CheeredUsers
    {

        public long PK_MessageID { get; set; }
        public int FK_SenderID { get; set; }
        public string SenderName { get; set; }
        public int FK_RecipientID { get; set; }
        public string RecipientName { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public Nullable<long> ParentMessageID { get; set; }

        public string ProfilePath { get; set; }

        public string MessageStatus { get; set; }



    }
}