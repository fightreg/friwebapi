﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FRIWebAPI.Models
{
    public class FighterHomeModel
    {
        public int UserID { get; set; }
        public string FullName { get; set; }
        public string FighterName { get; set; }       
        public bool IsFeaturedfighter { get; set; }       
        public string ProfilePhoto{ get; set; }
       
    }

   
}