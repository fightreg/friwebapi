select distinct
    object_name(fic.[object_id])as Master_User,
    [name]
from
    sys.fulltext_index_columns fic
    inner join sys.columns c
        on c.[object_id] = fic.[object_id]
        and c.[column_id] = fic.[column_id]



DROP VIEW [FRIFullTextSearch]

CREATE VIEW [dbo].[FRIFullTextSearch] WITH SCHEMABINDING   AS
SELECT  PK_UserID, FR_CurrentWeight, FR_CurrentWeightUnit, FR_TertiaryDiscipline, FR_TertiarySkillLevel, [Description], EmailAddress, EmergencyContactName, EmergencyContactNumber, 
                         FighterName, Gender, LastName, LegalFirstName, LegalMiddleName, PersonalWebsite, Phone, PostalOrZipCode, ProfilePhotoPath, Street, FR_PrimaryDiscipline, 
                         FR_SecondaryDiscipline, FR_PrimarySkillLevel, FR_SecondarySkillLevel, FR_HairColor, PR_Title, PR_EventNameOrSeries, SB_SanctioningBody, SB_Title, CN_Commission, CN_Title, 
                         cast(Wins as varchar(20)) as Wins, cast(Losses as varchar(20)) as Losses, cast(Draws as varchar(20)) as Draws, cast(TotalFights as varchar(20)) as TotalFights, cast(KO as varchar(20)) as KO, cast(TKO as varchar(20)) as TKO, CountryName, StateName,City
 from dbo.Master_User MU
    INNER JOIN dbo.Master_Profile MP ON MU.PK_UserID= MP.FK_UserID 
GO

create unique clustered index IDX_FRIFullTextSearch on dbo.FRIFullTextSearch (PK_UserID)

if not exists(select null from sys.fulltext_catalogs where [name] = 'FullText_catalog')

create fulltext catalog FullText_catalog as default

create fulltext index on dbo.FRIFullTextSearch (FR_CurrentWeight, FR_CurrentWeightUnit, FR_TertiaryDiscipline, FR_TertiarySkillLevel, [Description], EmailAddress, EmergencyContactName, EmergencyContactNumber, 
                         FighterName, Gender, LastName, LegalFirstName, LegalMiddleName, PersonalWebsite, Phone, PostalOrZipCode, ProfilePhotoPath, Street, FR_PrimaryDiscipline, 
                         FR_SecondaryDiscipline, FR_PrimarySkillLevel, FR_SecondarySkillLevel, FR_HairColor, PR_Title, PR_EventNameOrSeries, SB_SanctioningBody, SB_Title, CN_Commission, CN_Title, 
                         Wins, Losses, Draws, TotalFights, KO, TKO, CountryName, StateName,City) key index IDX_FRIFullTextSearch




select PK_UserID,concat(LegalFirstName,' ',LegalMiddleName,' ',LastName) as username, ProfilePhotoPath,Phone,
case when City = null or City = '' then StateName else concat(City,', ',StateName) end as UserLocation from [FRIFullTextSearch] MU INNER JOIN 
                                CONTAINSTABLE([FRIFullTextSearch],*, '"test*"') SR
                                ON SR.[Key] = MU.PK_UserID 



Steps

How can create full text search in a table

To create an Index, follow the steps:

1.Create a Full-Text Catalog
2.Create a Full-Text Index
3.Populate the Index


1) Create a Full-Text Catalog
select storage folder from database

Select "Full Text Catalog"---> "Storage"---->New Full-Text Catalog

Then Display New Full-Text Catalog-FRI_UAT
(FRI_UAT=Table Name)
Type FullText Catalog name-->ok
Full � Text Catalog created


2) Create a Full-Text Index
Select and Right click on the table (,which table you want to indexed) 
select Full-Text Index--->Define Full-Text Index...
Display Full-Text indexing Wizard-->Next--->Select Unique index--->Next--->Full-Text Indexing Wizard display-->Next--->Selcet Automaticaly----->Next---->Select Full-Text catalog---->Next---->
Next---->Finish

3) Populate the Index

Select indexed table--->right click on the table--->select Full text index---->start population

display full text index population--->close



